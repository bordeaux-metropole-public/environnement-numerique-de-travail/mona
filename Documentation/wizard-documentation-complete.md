# __Objectif__

D'un point de vue Global, le script PowerShell crée un assistant graphique (wizard) pour finaliser la configuration de la solution Mona.

# __Principales fonctionnalités__

## A. Interface utilisateur

1. Utilise WPF (Windows Presentation Foundation) pour créer une interface graphique.
2. Implémente un système d'étapes (wizard) avec des boutons "Précédent" et "Suivant".
3. Affiche une barre de progression pour les opérations finales.


## B. Configuration des comptes nécessaires au paramétrage de Mona

1. Permet de définir ou modifier les mots de passe des comptes administrateurs existants sur le système.
2. Crée un nouveau compte utilisateur nommé "Configurer" avec des permissions spécifiques.


## C. Paramétrage du délai d'inactivité

1. Permet de définir un délai d'inactivité avant la fermeture automatique de session.
2. Enregistre ce délai dans le registre Windows.


## D. Actions de configuration système

1. Installation de WebView2.
2. Configuration de WinLogon pour exécuter un script au démarrage.
3. Exécution de scripts supplémentaires pour la configuration du système :
   - Keys.ps1
   - WMI_Script.ps1


## E. Gestion de la sécurité

1. Utilise des PasswordBox pour la saisie sécurisée des mots de passe.
2. Offre une option pour afficher/masquer les mots de passe.


## F. Validation et gestion des erreurs

1. Vérifie la correspondance et la longueur minimale des mots de passe.
2. Affiche des messages d'erreur en cas de problème lors de la configuration.


## G. Exécution asynchrone

1. Utilise des runspaces pour exécuter les tâches finales de manière asynchrone.
2. Met à jour l'interface utilisateur en temps réel pendant l'exécution des tâches.


# __Particularités techniques__

1. Utilise des hashtables synchronisées pour partager des données entre les threads.
2. Emploie des runspaces pour l'exécution asynchrone des tâches finales.
3. Interagit directement avec le registre Windows et les processus système.
4. Utilise des techniques avancées de PowerShell comme les sessions initiales et les pools de runspaces.


#########################################################################################################
#########################################################################################################

# Documentation technique détaillée: Wizard.ps1

## 1. Objectif et fonctionnalités principales
Ce script PowerShell crée un assistant graphique pour la configuration initiale de l'environnement MONA (Médiathèque Ouverte Nouvelle Aquitaine) pour Bordeaux Métropole. Il guide l'administrateur à travers plusieurs étapes essentielles de configuration.

## 2. Structure des paramètres et variables

### 2.1 Paramètres d'entrée
```powershell
param(
    $InstallDIR = 'C:\ProgramData\BMETRO\ORONNACEUR',
    $DataDIR = 'C:\ProgramData\BordeauxMetropole\Mona'
)
```

### 2.2 Variables globales essentielles
```powershell
$global:step = 1                    # Étape courante du wizard
$global:adminIndex = 0              # Index du compte admin en cours
$global:idleTimeout = 5             # Délai d'inactivité par défaut
$global:passwordSet = $false        # État de configuration du mot de passe
$global:isTextVisible = $false      # État de visibilité du mot de passe
```

## 3. Configuration initiale

### 3.1 Préparation de l'environnement
```powershell
Set-ExecutionPolicy Bypass -Force
Start-Transcript -Path "$InstallDIR\script_log.txt" -Append
```

### 3.2 Installation des modules requis
Le script vérifie et installe les composants suivants :
- Fournisseur NuGet
- PackageManagement
- PowerShellGet

## 4. Interface utilisateur

### 4.1 Fenêtre principale
```xml
<Window xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' 
        xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'
        Title='Assistant de configuration Mona' Height='500' Width='600'>
    <Grid Name='MainGrid'>
        <Grid.RowDefinitions>
            <RowDefinition Height='Auto'/>
            <RowDefinition Height='*'/>
            <RowDefinition Height='Auto'/>
            <RowDefinition Height='Auto'/>
        </Grid.RowDefinitions>
        <!-- Composants détaillés ci-dessous -->
    </Grid>
</Window>
```

## 5. Étapes du wizard

### 5.1 Étape d'accueil (Étape 1)
```powershell
function UpdateWizard {
    switch ($global:step) {
        1 {
            $MainGrid.Children.Clear()
            # Configuration de l'écran d'accueil
            $lblTitle = New-Object System.Windows.Controls.TextBlock
            $lblTitle.Text = "Bienvenue dans l'Assistant de configuration de MONA..."
            # ... configuration détaillée
        }
        # ... autres étapes
    }
}
```

### 5.2 Configuration des comptes administrateurs (Étape 2)
```powershell
function ShowPasswordStep {
    $passwordXaml = @"
    <Grid xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'>
        <!-- Interface de mot de passe -->
    </Grid>
"@
    # ... implémentation détaillée
}
```

### 5.3 Création du compte Configurer (Étape 3)
```powershell
function ShowConfigStep {
    # Configuration de l'interface pour le compte Configurer
    # Validation et création du compte
}
```

### 5.4 Configuration du délai d'inactivité (Étape 4)
```powershell
function ShowIdleTimeoutStep {
    # Interface de configuration du délai
    # Validation et enregistrement dans le registre
}
```

### 5.5 Étape finale (Étape 5)
```powershell
function ShowFinalStep {
    # Affichage du résumé
    # Exécution des actions finales
}
```

## 6. Actions finales

### 6.1 Exécution des scripts complémentaires
```powershell
$actions = @(        
    @{ 
        Name = "Vérification de la présence du dossier d'installation"
        Action = { # ... }
    },
    @{ 
        Name = "Installation de WebView2"
        Action = { # ... }
    },
    @{ 
        Name = "Configuration de WinLogon"
        Action = { # ... }
    },
    @{ 
        Name = "Exécution de Keys.ps1"
        Action = { # ... }
    },
    @{ 
        Name = "Exécution de WMI_Script.ps1"
        Action = { # ... }
    }
)
```

### 6.2 Gestion asynchrone
```powershell
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, 1, $InitialSessionState, $Host)
$RunspacePool.ApartmentState = 'STA'
$RunspacePool.ThreadOptions = 'ReuseThread'
$RunspacePool.Open()
```

## 7. Fonctions auxiliaires

### 7.1 Gestion des mots de passe
```powershell
function ValidatePasswords {
    # Validation des mots de passe
    # Mise à jour des comptes
}

function TogglePasswordVisibility {
    # Basculement de l'affichage des mots de passe
}
```

### 7.2 Gestion du registre
```powershell
function SaveIdleTimeout {
    # Enregistrement du délai d'inactivité dans le registre
}
```

## 8. Dépendances externes

### 8.1 Fichiers requis
- MicrosoftEdgeWebView2RuntimeInstallerX64.exe
- Keys.ps1
- WMI_Script.ps1
- Launch-EULA_2.ps1

### 8.2 Modules PowerShell
- PackageManagement
- PowerShellGet

### 9.1 Prérequis
- Droits administratifs requis
- PowerShell 5.1 ou supérieur
- Windows 10/11

### 9.2 Structure des dossiers
```
C:\Programmes\BordeauxMetropole\Mona\ = Copie des fichiers sources necessaire uniquement à l'instalaltion/désinstallation
C:\ProgramData\BordeauxMetropole\Mona\ = Emplacement des fichers sources necessaire à l'utlisation et modifiable en fonction du profil


C:\Programmes\BordeauxMetropole\Mona\
 \Gallery
    \NuGet
    \PackageManagement
    \PowerShellGet
 \EULA
 \SET_VALEURS_SCRIPTBASE
 \SCRIPT_WMI
 ...

 C:\ProgramData\BordeauxMetropole\Mona\
 \admin
    \EULA
    \Pictures
 \Configurer
    \HTML
 \Wifi
    \Wificonfifgs
```




