# Documentation du script Keys.ps1

## Paramètres initiaux
- Installation Directory (InstallDIR) : 'C:\ProgramData\BMETRO\ORONNACEUR'
- Data Directory (DataDIR) : 'C:\ProgramData\BordeauxMetropole\Mona'

## Fonctionnalités principales

### 1. Gestion des groupes et utilisateurs
#### Création des groupes
- Crée un groupe local "Gestionnaire"
- Crée un groupe local "Admins_Mona"

#### Gestion des membres
- Récupère les membres du groupe Administrateurs via SID (S-1-5-32-544)
- Ajoute tous les membres du groupe Administrateurs aux groupes :
  - "Admins_Mona"
  - "Gestionnaire"

#### Création du compte USAGERS
- Crée un compte "USAGERS" avec les paramètres suivants :
  - Nom complet : "Acceder au PC"
  - Pas de mot de passe requis
  - Pas de changement de mot de passe possible
  - Pas d'expiration du compte
  - Mot de passe n'expire jamais

#### Gestion des comptes utilisateurs supplémentaires
- Utilise un fichier de configuration XML (Keys2.xml)
- Ajoute l'utilisateur "Configurer" au groupe "Gestionnaire"
- Crée des profils utilisateurs spécifiés dans le XML

### 2. Gestion des profils utilisateurs
- Création du profil pour l'utilisateur "Configurer"
- Copie du profil par défaut vers C:\ProgramData\BordeauxMetropole\DefaultProfile
- Configuration des droits d'accès sur le profil par défaut :
  - Groupe Administrateurs comme propriétaire
  - Droits "Modify" pour l'utilisateur "Configurer" sur le bureau

### 3. Gestion des répertoires et fichiers
- Création des répertoires selon la configuration XML
- Copie des fichiers et répertoires spécifiés dans le XML
- Création de raccourcis :
  - "Se déconnecter" sur le bureau public
  - "Personnalisation Mona" sur le bureau de Configurer avec sous-raccourcis :
    - "Bureau AccesLibre"
    - "Personnalisation page d'accueil"
  - "Accès aux CGU" sur le bureau des membres "Admins_Mona"

### 4. Configuration de l'alimentation
#### Paramètres généraux
- Activation du mode "Performances élevées"
- Désactivation de l'extinction de l'affichage
- Configuration des délais :
  - Mise en veille prolongée : désactivée
  - Arrêt du disque dur : désactivé
  - Extinction de l'écran : désactivée
  - Mise en veille : désactivée

#### Configuration des boutons
- Bouton Marche/Arrêt : configuré sur "Arrêter"
- Bouton de Mise en veille : configuré sur "Arrêter"
- Action de fermeture du capot : "Ne rien faire" (secteur et batterie)

### 5. Configuration du registre
- Application des clés de registre définies dans Keys2.xml
- Chaque clé contient :
  - Chemin
  - Nom de la valeur
  - Données
  - Type

### 6. Configuration de la sécurité
#### Exclusions Windows Defender
- Ajout du chemin "C:\Program Files\BordeauxMetropole\Mona" aux exclusions

#### Tâches planifiées
1. "Reset Profil Usagers"
   - Déclencheur : à l'ouverture de session de "usagers"
   - Action : exécution du script reset_profil_usagers.ps1

2. "Disable Autologon"
   - Action : désactivation de l'autologon via registre
   - Pas de déclencheur automatique

3. "Force Autologon"
   - Déclencheur : à l'ouverture de session
   - Action : activation de l'autologon via registre

4. "Importer Profil WiFi a la deconnexion"
   - Déclencheur : à l'ouverture de session
   - Action : exécution du script script2.ps1

### 7. Configuration des ACL (Access Control Lists)
- Application des ACL selon la configuration dans ACL.xml
- Gestion des permissions :
  - Suppression des règles existantes spécifiées
  - Ajout de nouvelles règles d'accès
  - Configuration du propriétaire
  - Protection des règles d'accès

### 8. Configuration du verrouillage Windows
- Désactivation du verrouillage pour :
  - Compte "Configurer"
  - Compte d'installation actuel
- Configuration via la clé de registre DisableLockWorkstation

### 9. Gestion des mises à jour Edge
- Suppression du déclencheur "À l'ouverture de session" de la tâche MicrosoftEdgeUpdateTaskMachineCore

## Journalisation
- Création d'un fichier de log : registre_log.txt dans le répertoire d'installation