# Documentation du script Launch-IDLE.ps1

## Configuration initiale

### Paramètres du script
- `$Configure` : Switch pour activer le mode configuration

### Détermination du chemin du script
Utilise plusieurs méthodes en cascade pour déterminer le chemin :
1. Extension PSCommandPath (.ps1)
2. Host Invocation
3. Script Invocation
4. Process courant

## Fonctions principales

### Set-InactivityTime
- Paramètre : `$Minutes` (entier)
- Modifie le temps d'inactivité maximal dans le script
- Utilise une expression régulière pour remplacer la valeur existante

### start-configure
Crée une interface de configuration avec :
- Fenêtre XAML pour la sélection du temps d'inactivité
- ComboBox avec options : 2, 3, 5, 8, 10 minutes
- Bouton de validation

### start-idle
Fonction principale de surveillance d'inactivité

#### Variables de configuration
- `$MaxInactivityTime` : Temps maximum d'inactivité (lu depuis le registre)
- `$WarningLevel` : Seuil d'avertissement (2.5 minutes)
- `$AlerteLevel` : Seuil d'alerte (1 minute)
- `$CountDownStep` : Pas du décompte (5 secondes)

## Classes et types personnalisés

### Win32Helper
- Constantes : `WS_EX_TOOLWINDOW`, `GWL_EXSTYLE`
- Fonctions importées :
  - `SetWindowLongPtr`
  - `FindWindow`
  - `ShowWindow`

### DPI
- Fonctions importées : `GetDC`, `ReleaseDC`, `GetDeviceCaps`
- Méthode `scaling()` pour calcul du ratio d'échelle

### Taskbar
- Constantes : `SW_HIDE`, `SW_SHOW`
- Méthodes : `Show()`, `Hide()`

### Util.Creds
- Gestion des identifiants Windows
- Constantes pour tailles maximales (nom utilisateur, mot de passe, domaine)
- Énumérations pour les codes retour et flags
- Structure CREDUI_INFO
- Méthode `PromptForCredentials`

### Helper.IdleTimeDetector
- Structure `LASTINPUTINFO`
- Méthodes :
  - `SendBackWindow`
  - `SendFrontWindow`
  - `GetIdleTimeInfo`

## Interface graphique (XAML)

### Styles personnalisés
1. SliderCheckBox
   - Style personnalisé pour cases à cocher avec animation
   - Transitions de couleur vert/rouge

2. WindowStyleRounded
   - Fenêtre sans bordure
   - Coins arrondis
   - Ombre portée

3. Styles de labels
   - Style de base avec ombre
   - Style Warning (orange)
   - Style AnimatedLabel (avec animations)

### Éléments d'interface
1. Panel EULA
   - Message d'avertissement
   - Bouton "J'ai compris"

2. Panel Content
   - Affichage du décompte
   - Cases à cocher :
     - "Toujours visible"
     - "Suspendre" (masqué par défaut)
     - "Se Déconnecter" (masqué par défaut)

## Gestion des événements

### Fenêtre principale
1. Loaded
   - Configure la fenêtre comme outil
   - Positionne la fenêtre
   - Initialise le timer de surveillance

2. MouseLeftButtonDown
   - Gestion du déplacement de la fenêtre
   - Maintien dans les limites de l'écran

3. MouseDoubleClick
   - Combinaison Ctrl+Shift gauches pour fermeture
   - Shift gauche pour afficher/masquer options

### Timer de surveillance (InactivityDT)
- Intervalle : 500ms
- Vérifie le temps d'inactivité
- Gère les niveaux d'alerte :
  - Normal : Affichage simple
  - Warning : Texte orange
  - Alerte : Animation et agrandissement
- Force la déconnexion si temps maximum dépassé

### Contrôles spécifiques

#### chkSuspend (Suspendre)
- Demande authentification
- Vérifie appartenance groupe 'Gestionnaire'
- Arrête/Redémarre la surveillance

#### chkAutologonExit (Se Déconnecter)
- Demande authentification
- Vérifie appartenance groupe 'Gestionnaire'
- Exécute la tâche planifiée "Disable Autologon"
- Déconnecte l'utilisateur

## Sécurité et validation
- Validation des identifiants locaux
- Vérification appartenance groupe Gestionnaire
- Protection contre la fermeture non autorisée
- Maintien de la fenêtre au premier plan si nécessaire

## Registre et configuration
- Lecture du temps d'inactivité depuis la clé : "HKLM:\SOFTWARE\MonaAssistant"
- Valeur par défaut : 5 minutes
- Propriété : "IdleTimeout"