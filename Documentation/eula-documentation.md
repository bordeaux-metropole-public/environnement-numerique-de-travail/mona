# Documentation détaillée du script de gestion des CGU

## 1. Configuration initiale et variables
### Variables principales
- `$Debug` : Mode débogage (False par défaut)
- `$showEULAUsers` : Liste des utilisateurs devant voir les CGU (@('USAGERS'))
- `$MyDir` : Répertoire courant du script
- `$UserProfileLetter` : Lettre du lecteur virtuel ('Z:')
- `$KeyPath` : Chemin de la clé de registre ('HKLM:\SOFTWARE\NEO')
- `$UserDataFolder` : Dossier temporaire ("$UserProfileLetter\AppData\Local\Temp")
- `$Flag_Action` : Indicateur d'action utilisateur (False par défaut)

### Détermination du chemin du script ($Script:ScriptPath)
Utilise plusieurs méthodes pour déterminer le chemin :
1. Extension du fichier PSCommandPath
2. Invocation de l'hôte
3. Invocation du script
4. Process courant

### Chemins dérivés
- `$idleScript` : Chemin vers Launch-IDLE.ps1 basé sur $Script:ScriptPath

## 2. Vérification initiale WiFi
- Vérifie l'appartenance au groupe 'Gestionnaire'
- Si membre du groupe : Lance 'WiFiMonitor.ps1' en arrière-plan avec :
  - ExecutionPolicy : Bypass
  - WindowStyle : Hidden

## 3. Fonctions
### Start-EdgeSizedWindow
- Ajoute la classe Win32 avec l'import DLL user32.dll
- Charge System.Windows.Forms
- Lance msedge.exe
- Recherche la fenêtre principale d'Edge
- Redimensionne la fenêtre :
  - Position X : 300
  - Position Y : 5
  - Largeur : Écran - 300
  - Hauteur : Écran - 5

## 4. Contrôle d'accès et configuration du profil
### Vérification utilisateur
- Sort si utilisateur non dans showEULAUsers et pas en debug
- Lance IDLE.ps1 et sort si utilisateur est 'USAGERS'

### Configuration du profil (hors debug)
1. Création du lecteur virtuel Z: avec subst
2. Configuration des dossiers spéciaux via SHSetKnownFolderPath :
   - Bureau (2 GUID : FDD39AD0-238F-46AF-ADB4-6C85480369C7, B4BFCC3A-DB2C-424C-B029-7FE99A87C641)
   - Téléchargements (2 GUID : 374DE290-123F-4565-9164-39C4925E467B, 7D83EE9B-2244-4E70-B1F5-5393042AF1E4)
   - Images (2 GUID : 33E28130-4E1E-4676-835A-98395C3BC3BB, 0DDD015D-B06C-45D5-8C4C-F59713854639)
   - Musique (GUID : 4BD8D571-6D19-48D3-BE97-422220080E43)
   - Vidéos (GUID : 18989B1D-99B5-455B-841C-AB7C74E4DDFC)

## 5. Gestion des CGU
### Préparation de l'environnement
- Crée le dossier Charte dans UserDataFolder
- Copie index.html et le dossier assets

### Sélection de la charte
1. Lit le CodeCommune dans le registre
2. Sélection de la charte :
   - Si pas de code : utilise 'Model__CGU__ Defaut.html'
   - Si code existe : recherche fichier correspondant au code
   - Si fichier non trouvé : utilise charte par défaut

## 6. Configuration technique
### Importation DLL et types
1. Ajout des fonctions Win32 :
   - SetDllDirectory
   - SetSysColors
   - SystemParametersInfo

2. Chargement des bibliothèques :
   - System.Windows.Forms
   - PresentationFramework
   - System.Windows.Interop
   - Microsoft.Web.WebView2.Wpf
   - Microsoft.Web.WebView2.Core

### Classes personnalisées
1. Win32Helper :
   - Constantes : WS_EX_TOOLWINDOW, GWL_EXSTYLE
   - Fonctions : SetWindowLongPtr, FindWindow, ShowWindow

2. DPI :
   - GetDC, ReleaseDC, GetDeviceCaps
   - Calcul du scaling d'affichage

3. Taskbar :
   - Constantes : SW_HIDE, SW_SHOW
   - Méthodes : Show(), Hide()

## 7. Interface graphique (XAML)
### Configuration de la fenêtre principale
- WindowStartupLocation : CenterScreen
- WindowState : Maximized
- Topmost : True
- WindowStyle : None
- ResizeMode : NoResize
- ShowInTaskbar : False

### Éléments d'interface
1. WebView2 pour l'affichage des CGU
2. Panel de validation :
   - Checkbox d'acceptation avec style personnalisé
   - Labels pour le texte
   - Boutons "Continuer" et "Refuser"

## 8. Gestion des événements
### WebView2
- Configuration initiale :
  - Désactivation menus contextuels
  - Désactivation DevTools
  - Désactivation zoom
  - Désactivation auto-remplissage
  - Activation scripts

### Boutons
1. Validation (Continuer) :
   - Désabonne des événements ProcessStart
   - Lance Edge redimensionné
   - Nettoie Quick Access (conserve Téléchargements)
   - Lance IDLE.ps1

2. Refuser :
   - Active Flag_Action
   - Lance logoff.exe si non debug

### Événements fenêtre
1. Loaded :
   - Cache barre des tâches
   - Configure timer Escape (80ms)
   - Configure timer redimensionnement (5s)
   - Configure surveillance processus

2. Closed :
   - Arrête les timers
   - Restaure barre des tâches
   - Libère WebView2

### Surveillance des processus
- Liste des processus exclus :
  ```powershell
  @('msedgewebview2.exe', 'explorer.exe', 'RuntimeBroker.exe',
    'backgroundTaskHost.exe', 'StartMenuExperienceHost.exe',
    'SearchApp.exe', 'SettingSyncHost.exe', 'SearchProtocolHost.exe',
    'Microsoft.Photos.exe', 'taskhostw.exe', 'SearchProtocolHost.exe',
    'HxTsr.exe')
  ```
- Arrête tout autre processus lancé

## 9. Sécurité
### Protection contre les raccourcis
- Timer Escape pour bloquer Alt+F4/Alt+Tab
- Monitoring continu du redimensionnement
- Blocage des processus non autorisés
- Mode fenêtre exclusive (Topmost)