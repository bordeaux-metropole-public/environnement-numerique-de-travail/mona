﻿$NomTache = "Importer Profil WiFi a la deconnexion"

Start-Transcript -Path "C:\Temp\importer_profil_wifi.txt" -Append

# Chemin vers le script d'importation Wi-Fi
$ScriptPath = "C:\Program Files\BordeauxMetropole\Mona\Wifi\script2.ps1"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe" -Argument "-ExecutionPolicy Bypass -File `"$ScriptPath`""

# Déclencheur : à la connexion de tous les utilisateurs
$Trigger = New-ScheduledTaskTrigger -AtLogOn
$Trigger.Enabled = $true

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "Importe le profil Wi-Fi" -Settings $Settings

Stop-Transcript