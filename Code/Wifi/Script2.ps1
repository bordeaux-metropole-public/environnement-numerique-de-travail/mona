﻿### Import du profil Wifi a partir du XML ###
###############################################

# Demarrage du transcript pour enregistrer les logs
Start-Transcript -Path "C:\ProgramData\BordeauxMetropole\Mona\wifi_profile_import.txt"

# Script pour deconnecter tous les profils Wi-Fi, les supprimer, puis monter un nouveau profil depuis un fichier XML

# Deconnexion de tous les profils Wi-Fi
Write-Host "Deconnexion de tous les profils Wi-Fi..."
netsh wlan disconnect

# Obtention de la liste des profils Wi-Fi
$profils = netsh wlan show profiles | Select-String "Tous les profils utilisateurs" | ForEach-Object { $_.ToString().Split(':')[1].Trim() }

# Suppression de tous les profils Wi-Fi
Write-Host "Debut de la suppression des profils Wi-Fi..."
foreach ($profil in $profils) {
    Write-Host "Suppression du profil : $profil"
    $resultat = netsh wlan delete profile name="$profil"
    # Enregistrement du resultat de la suppression dans le transcript
    Write-Host "Resultat de la suppression : $resultat"
}
Write-Host "Fin de la suppression des profils Wi-Fi."

# Chemin vers le dossier contenant les fichiers XML des profils Wi-Fi
$dossierXML = "C:\ProgramData\BordeauxMetropole\Mona\Wifi\WifiConfigs"

# Recuperation des fichiers XML tries par date de modification decroissante
$fichiersXML = Get-ChildItem -Path $dossierXML -Filter "*.xml" | Sort-Object LastWriteTime -Descending

# Variable pour suivre si la connexion a reussi
$connexionReussie = $false

foreach ($fichierXML in $fichiersXML) {
    $cheminXML = $fichierXML.FullName
    
    # Ajout du profil Wi-Fi depuis le fichier XML
    Write-Host "Ajout du profil Wi-Fi depuis $cheminXML"
    $resultatAjout = netsh wlan add profile filename="$cheminXML" user=all
    # Enregistrement du resultat de l'ajout dans le transcript
    Write-Host "Resultat de l'ajout du profil : $resultatAjout"
    
    # Verification de la connexion
    Start-Sleep -Seconds 5 # Attendre 5 secondes pour laisser le temps a la connexion de s'etablir
    $etatConnexion = netsh wlan show interfaces
    # Recuperation du SSID actuel
    $connexionActuelle = netsh wlan show interfaces | Select-String "SSID\s+:\s(.+)" | ForEach-Object { $_.Matches.Groups[1].Value.Trim() }
    
    if ($connexionActuelle -eq $nomProfil) {
        Write-Host "Connexion reussie au profil $nomProfil"
        $connexionReussie = $true
        break
    } else {
        Write-Host "echec de la connexion au profil $nomProfil. Tentative avec le prochain profil..."
    }
}

if (-not $connexionReussie) {
    Write-Host "Impossible de se connecter a l'un des profils Wi-Fi disponibles."
}

# Arrêt du transcript
Stop-Transcript


