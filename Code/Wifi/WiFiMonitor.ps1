﻿# WiFiMonitorWPF.ps1

# Configurer la journalisation
$logPath = "C:\ProgramData\BordeauxMetropole\Mona"

if (!(Test-Path -Path $logPath)) {
    New-Item -ItemType Directory -Path $logPath -Force
}

$logFile = Join-Path $logPath "WiFiMonitor.log"
$global:LogFile = $logFile

Function Write-Log {
    param(
        [string]$Message,
        [string]$LogFile = $global:LogFile
    )

    $timestamp = (Get-Date).ToString("yyyy-MM-dd HH:mm:ss")
    $entry = "$timestamp $Message"
    Add-Content -Path $LogFile -Value $entry
}

# Charger les assemblies necessaires
Add-Type -AssemblyName PresentationFramework
Add-Type -AssemblyName WindowsBase
Add-Type -AssemblyName PresentationCore

# Demarrer la surveillance
Write-Log "Demarrage du script WiFiMonitorWPF.ps1"

# Pause de 10 secondes
Start-Sleep -Seconds 10

# Fonction pour afficher le pop-up WPF avec le nom du reseau Wi-Fi surligne en vert
function Show-WifiPopup {
    param(
        [string]$networkName
    )

    Write-Log "Debut de la fonction Show-WifiPopup pour le reseau $networkName"

    $XAML = @"
<Window xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        Title="Configuration Wi-Fi" Height="200" Width="400" WindowStartupLocation="CenterScreen">
    <Grid>
        <TextBlock TextWrapping="Wrap" Margin="10,10,10,50" FontSize="14">
        <Run Text="Souhaitez-vous paramétrer ou conserver le réseau Wi-Fi " />
        <Run Text="$networkName" Foreground="Green" FontWeight="Bold"/>
        <Run Text=" pour le compte Usager ?" />
        </TextBlock>
        <StackPanel Orientation="Horizontal" HorizontalAlignment="Center" VerticalAlignment="Bottom" Margin="0,0,0,10">
        <Button Name="YesButton" Content="Oui" Width="80" Margin="10,0"/>
        <Button Name="NoButton" Content="Non" Width="80" Margin="10,0"/>
        </StackPanel>
    </Grid>
</Window>
"@

    Write-Log "Chargement du XAML pour la fenêtre WPF."

    # Charger le XAML
    $StringReader = New-Object System.IO.StringReader($XAML)
    $XmlReader = [System.Xml.XmlReader]::Create($StringReader)
    $Form = [Windows.Markup.XamlReader]::Load($XmlReader)
    
    # Verifier que la fenêtre a ete chargee correctement
    if (-not $Form) {
        Write-Log "Erreur lors du chargement de la fenêtre WPF."
        return
    } else {
        Write-Log "Fenêtre WPF chargee avec succes."
    }

    # Recuperer les boutons
    $YesButton = $Form.FindName("YesButton")
    $NoButton = $Form.FindName("NoButton")

    # Variable pour stocker le choix de l'utilisateur
    $script:UserChoice = $null

    # evenement pour le bouton "Oui"
    $YesButton.Add_Click({
        $script:UserChoice = "Yes"
        Write-Log "Bouton Oui clique."
        $Form.Close()
    })

    # evenement pour le bouton "Non"
    $NoButton.Add_Click({
        $script:UserChoice = "No"
        Write-Log "Bouton Non clique."
        $Form.Close()
    })

    # Afficher la fenêtre
    try {
        Write-Log "Affichage de la fenêtre WPF."
        $Form.ShowDialog() | Out-Null
        Write-Log "Fenêtre WPF fermee par l'utilisateur."
    } catch {
        Write-Log "Exception lors de l'affichage de la fenêtre WPF : $_"
    }

    # Verifier si l'utilisateur a ferme la fenêtre sans faire de choix
    if (-not $script:UserChoice) {
        $script:UserChoice = "Closed"
        Write-Log "L'utilisateur a ferme la fenêtre sans faire de choix."
    }

    # Retourner le choix de l'utilisateur
    Write-Log "Choix de l'utilisateur pour le reseau $networkName : $script:UserChoice"
    return $script:UserChoice
}

# Fonction pour surveiller les connexions Wi-Fi
Function Monitor-WifiConnections {
    Write-Log "Debut de la fonction Monitor-WifiConnections"

    # Recuperer les interfaces Wi-Fi
    $wifiAdapters = Get-NetAdapter | Where-Object { $_.InterfaceDescription -match "Wireless" -or $_.InterfaceDescription -match "Wi-Fi" -or $_.InterfaceDescription -match "802.11" }

    if (-not $wifiAdapters) {
        Write-Log "Aucune interface Wi-Fi trouvee."
        return
    } else {
        $wifiInterfaceAliases = $wifiAdapters | Select-Object -ExpandProperty InterfaceAlias
        Write-Log "Interfaces Wi-Fi detectees : $($wifiInterfaceAliases -join ', ')"
    }

    # Obtenir la liste initiale des reseaux connectes
    $initialNetworks = Get-NetConnectionProfile | Where-Object { $wifiInterfaceAliases -contains $_.InterfaceAlias }

    if (-not $initialNetworks) {
        $initialNetworks = @()
        Write-Log "Aucun reseau initialement connecte."
    } else {
        Write-Log "Reseaux initialement connectes : $($initialNetworks.Name -join ', ')"
    }

    # Obtenir la liste initiale des profils Wi-Fi
    $initialWifiProfiles = netsh wlan show profiles | Where-Object { $_ -match ":\s" } | ForEach-Object {
        ($_.ToString().Split(":")[1]).Trim()
    }

    if (-not $initialWifiProfiles) {
        $initialWifiProfiles = @()
        Write-Log "Aucun profil Wi-Fi initialement trouve."
    } else {
        Write-Log "Profils Wi-Fi initialement trouves : $($initialWifiProfiles -join ', ')"
    }

    # Liste des reseaux en attente de connexion complete
    $pendingNetworks = @()

    while ($true) {
        #Write-Log "Boucle principale - verification des reseaux Wi-Fi"

        # Obtenir la liste actuelle des reseaux connectes
        $currentNetworks = Get-NetConnectionProfile | Where-Object { $wifiInterfaceAliases -contains $_.InterfaceAlias }
    
        if (-not $currentNetworks) {
            $currentNetworks = @()
            #Write-Log "Aucun reseau actuellement connecte."
        }

        # Obtenir la liste actuelle des profils Wi-Fi
        $currentWifiProfiles = netsh wlan show profiles | Where-Object { $_ -match ":\s" } | ForEach-Object {
            ($_.ToString().Split(":")[1]).Trim()
        }

        if (-not $currentWifiProfiles) {
            $currentWifiProfiles = @()
            #Write-Log "Aucun profil Wi-Fi actuellement trouve."
        } else {
            #Write-Log "Profils Wi-Fi actuels : $($currentWifiProfiles -join ', ')"
        }

        # Detecter les nouveaux profils Wi-Fi
        $newWifiProfiles = Compare-Object -ReferenceObject $initialWifiProfiles -DifferenceObject $currentWifiProfiles -PassThru | Where-Object { $_.SideIndicator -eq '=>' }

        if ($newWifiProfiles) {
            foreach ($profile in $newWifiProfiles) {
                Write-Log "Nouveau profil Wi-Fi detecte : $profile"
            }
        }

        # Detecter les profils Wi-Fi supprimes
        $removedWifiProfiles = Compare-Object -ReferenceObject $initialWifiProfiles -DifferenceObject $currentWifiProfiles -PassThru | Where-Object { $_.SideIndicator -eq '<=' }

        if ($removedWifiProfiles) {
            foreach ($profile in $removedWifiProfiles) {
                $profileName = $profile

                Write-Log "Profil Wi-Fi supprime : $profileName"

                # Chemin du fichier profil Wi-Fi a supprimer
                $exportPath = "C:\ProgramData\BordeauxMetropole\Mona\Wifi\WifiConfigs"
                $profileFilePattern = Join-Path $exportPath "Wi-Fi-$profileName*.xml"

                # Supprimer le fichier correspondant
                $filesToDelete = Get-ChildItem -Path $profileFilePattern -ErrorAction SilentlyContinue

                if ($filesToDelete) {
                    foreach ($file in $filesToDelete) {
                        try {
                            Remove-Item -Path $file.FullName -Force
                            Write-Log "Fichier de profil Wi-Fi '$($file.Name)' supprime avec succes."
                        } catch {
                            Write-Log "Erreur lors de la suppression du fichier '$($file.Name)': $_"
                        }
                    }
                } else {
                    Write-Log "Aucun fichier de profil Wi-Fi correspondant a '$profileName' trouve."
                }
            }
        }

        # Mettre a jour la liste initiale des profils Wi-Fi
        $initialWifiProfiles = $currentWifiProfiles

        # Detecter les nouveaux reseaux
        $newNetworks = Compare-Object -ReferenceObject $initialNetworks -DifferenceObject $currentNetworks -Property Name, InterfaceAlias -PassThru | Where-Object { $_.SideIndicator -eq '=>' }

        if ($newNetworks) {
            foreach ($network in $newNetworks) {
                $networkName = $network.Name

                Write-Log "Nouveau reseau detecte : $networkName"

                # Verifier si le nom du reseau est valide
                if ($networkName -and $networkName -ne "Identification..." -and $networkName -ne "unknown") {
                    # Ajouter le reseau a la liste des reseaux en attente s'il n'y est pas deja
                    if (-not ($pendingNetworks -contains $networkName)) {
                        $pendingNetworks += $networkName
                        Write-Log "Nouveau reseau Wi-Fi ajoute a la liste en attente : $networkName"
                    } else {
                        Write-Log "Le reseau $networkName est deja dans la liste en attente."
                    }
                } else {
                    Write-Log "Le nom du reseau $networkName n'est pas valide."
                }
            }
        }

        # Verifier les reseaux en attente
        $pendingNetworksCopy = @($pendingNetworks)

        foreach ($networkName in $pendingNetworksCopy) {
            Write-Log "Traitement du reseau en attente : $networkName"

            # Obtenir les details du reseau
            $networkDetails = Get-NetConnectionProfile | Where-Object {
                ($_.Name -eq $networkName) -and
                ($wifiInterfaceAliases -contains $_.InterfaceAlias)
            }

            if ($networkDetails) {
                Write-Log "Details du reseau $networkName obtenus."

                # Verifier si le reseau est entierement connecte
if (($networkDetails.IPv4Connectivity -eq 'Internet') -or ($networkDetails.IPv4Connectivity -eq 'LocalNetwork')) {
    Write-Log "Le reseau $networkName est entierement connecte."

    # Definir le chemin du fichier de profil Wi-Fi
    $exportPath = "C:\ProgramData\BordeauxMetropole\Mona\Wifi\WifiConfigs"
    $profileFilePattern = Join-Path $exportPath "Wi-Fi-$networkName*.xml"

    # Verifier si le fichier existe deja
    if (Test-Path -Path $profileFilePattern) {
        Write-Log "Le profil Wi-Fi pour '$networkName' existe deja dans le repertoire. Aucun pop-up ne sera affiche."
    } else {
        Write-Log "Aucun profil Wi-Fi existant pour '$networkName'. Affichage du pop-up."

        # Afficher le pop-up
        $userChoice = Show-WifiPopup -networkName $networkName

        if ($userChoice -eq "Yes") {
            # Code pour parametrer le Wi-Fi
            Write-Log "L'utilisateur a choisi Oui pour le reseau $networkName"

            # Creer le repertoire s'il n'existe pas
            if (!(Test-Path -Path $exportPath)) {
                                New-Item -ItemType Directory -Path $exportPath -Force
                                Write-Log "Creation du repertoire d'exportation : $exportPath"
                                    }

            # Exporter le profil Wi-Fi
            try {
                netsh wlan export profile name="$networkName" folder="$exportPath" key=clear | Out-Null
                Write-Log "Profil Wi-Fi '$networkName' exporte avec succes."
                        } catch {
                            Write-Log "Erreur lors de l'exportation du profil Wi-Fi '$networkName' : $_"
                        }
                    } else {
                        # L'utilisateur a choisi "Non" ou a ferme le pop-up
                        Write-Log "L'utilisateur a choisi Non ou a ferme le pop-up pour le reseau $networkName"
                        # Aucune action n'est entreprise
                    }
                }

    # Retirer le reseau de la liste des reseaux en attente
    $pendingNetworks = $pendingNetworks | Where-Object { $_ -ne $networkName }
    Write-Log "Le reseau $networkName a ete retire de la liste en attente."
} else {
                    Write-Log "Le reseau $networkName n'est pas encore entierement connecte."
                }
            } else {
                Write-Log "Le reseau $networkName n'est plus present. Retrait de la liste en attente."
                # Retirer le reseau de la liste des reseaux en attente s'il n'est plus present
                $pendingNetworks = $pendingNetworks | Where-Object { $_ -ne $networkName }
            }
        }

        # Mettre a jour la liste initiale des reseaux connectes
        $initialNetworks = $currentNetworks

        Start-Sleep -Seconds 5
    }
}

# Appeler la fonction pour surveiller les connexions Wi-Fi
Monitor-WifiConnections