﻿# Charger le fichier XML
$xmlPath = "C:\Tmp\Mona\SCRIPT_WMI\WMI_XML.xml"
$xml = [xml](Get-Content $xmlPath)

# Fonction pour vérifier si un namespace existe
function Test-NamespaceExists {
    param (
        [string]$Namespace
    )
    try {
        Get-CimNamespace -Namespace $Namespace -ErrorAction Stop | Out-Null
        return $true
    } catch {
        return $false
    }
}

# Parcourir chaque élément CIMInstance dans le XML
foreach ($instance in $xml.WMISettings.ChildNodes) {
    $namespace = $instance.Namespace
    $name = $instance.Name
    $instanceID = $instance.InstanceID

    # Vérifier si le namespace est valide
    if (-not [string]::IsNullOrEmpty($namespace)) {
        if (Test-NamespaceExists -Namespace $namespace) {
            try {
                # Tenter de supprimer l'instance WMI
                $wmiInstance = Get-CimInstance -Namespace $namespace -ClassName $instanceID -Filter "Name='$name'" -ErrorAction Stop
                if ($wmiInstance) {
                    Remove-CimInstance -InputObject $wmiInstance
                    Write-Host "Instance supprimée : $namespace, $name"
                } else {
                    Write-Host "Instance non trouvée : $namespace, $name"
                }
            } catch {
                Write-Host "Erreur lors de la suppression de l'instance : $namespace, $name"
                Write-Host $_.Exception.Message
            }
        } else {
            Write-Host "Namespace non trouvé : $namespace"
        }
    } else {
        Write-Host "Namespace invalide pour l'instance : $name"
    }
}

Write-Host "Opération terminée."
