﻿
<#
.SYNOPSIS
    Configure WMI settings based on XML input for Windows management.
.DESCRIPTION
    Reads from an XML file to configure WMI settings for Windows management, specifically for managing pinned folders in Windows 11 start menu.
.PARAMETER XmlConfigPath
    The path to the XML configuration file containing the WMI settings to apply.
.EXAMPLE
    .\script.ps1 -XmlConfigPath "C:\Path\To\configuration.xml"
#>

####################################################################################################
######################## Script to configure WMI settings based on XML input #######################
####################################################################################################




[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [string]$InstallDIR
)

# Démarrez la transcription (assurez-vous que le chemin d'accès est accessible en écriture)
Start-Transcript -Path "$InstallDIR\WMI_log.txt" -Append

function Get-Configuration {
    param (
        [string]$FilePath
    )
    [xml]$configXml = Get-Content $FilePath
    $configItems = @()
    foreach ($cimInstance in $configXml.WMISettings.ChildNodes) {
        $classNamespace = $cimInstance.Namespace.Trim('"')
        $name = $cimInstance.Name
        $value = $cimInstance.Value
        $instanceID = $cimInstance.InstanceID
        $configItems += [PSCustomObject]@{
            ClassName = $classNamespace
            PropertyName = $name
            PropertyValue = $value
            InstanceID = $instanceID
        }
    }
    return $configItems
}

function Update-WMISetting {
    param (
        [string]$className,
        [string]$name,
        [int]$value,
        [string]$instanceID
    )
    $namespacePath = 'root\cimv2\mdm\dmmap'

    try {
        $instanceFilter = "ParentID='./Vendor/MSFT/Policy/Config' and InstanceID='$instanceID'"
        $instances = Get-CimInstance -Namespace $namespacePath -ClassName $className -Filter $instanceFilter -ErrorAction SilentlyContinue

        if ($instances.Count -eq 0) {
            $instance = New-CimInstance -Namespace $namespacePath -ClassName $className -Property @{ParentID='./Vendor/MSFT/Policy/Config'; InstanceID=$instanceID} -ErrorAction Stop
        } else {
            $instance = $instances[0]
        }

        $instance = $instance | Set-CimInstance -Property @{$name = $value} -PassThru

        Write-Host "Successfully configured '$name' with value '$value' in class '$className' within namespace '$namespacePath' with InstanceID '$instanceID'." -ForegroundColor Green
    } catch {
        Write-Host "Failed to configure '$name' in class '$className' within namespace '$namespacePath' with InstanceID '$instanceID': $($_.Exception.Message)" -ForegroundColor Red
    }
}


$configItems = Get-Configuration -FilePath "$InstallDIR\SCRIPT_WMI\WMI_XML.xml"

foreach ($item in $configItems) {
    Update-WMISetting -className $item.ClassName -name $item.PropertyName -value $item.PropertyValue -instanceID $item.InstanceID
}

Stop-Transcript
