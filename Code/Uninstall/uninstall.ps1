﻿###################################
### Desinstallation de Mona V2 ####
###################################


### Script to uninstall WMI settings based on XML input ###
###########################################################


# Demarrez la transcription (assurez-vous que le chemin d'acces est accessible en ecriture)
Start-Transcript -Path "C:\Windows\Temp\desinstallation.txt" -Append

[System.Management.Automation.Runspaces.Runspace]::DefaultRunspace.ApartmentState
[System.Security.Principal.WindowsIdentity]::GetCurrent().Name

function Get-Configuration {
    param (
        [string]$FilePath
    )
    [xml]$configXml = Get-Content $FilePath
    $configItems = @()
    foreach ($cimInstance in $configXml.WMISettings.ChildNodes) {
        $classNamespace = $cimInstance.Namespace.Trim('"')
        $name = $cimInstance.Name
        $value = $cimInstance.Value
        $instanceID = $cimInstance.InstanceID
        $configItems += [PSCustomObject]@{
            ClassName = $classNamespace
            PropertyName = $name
            PropertyValue = $value
            InstanceID = $instanceID
        }
    }
    return $configItems
}

function Update-WMISetting {
    param (
        [string]$className,
        [string]$name,
        [int]$value,
        [string]$instanceID
    )
    $namespacePath = 'root\cimv2\mdm\dmmap'

    try {
        $instanceFilter = "ParentID='./Vendor/MSFT/Policy/Config' and InstanceID='$instanceID'"
        $instances = Get-CimInstance -Namespace $namespacePath -ClassName $className -Filter $instanceFilter -ErrorAction SilentlyContinue

        if ($instances.Count -eq 0) {
            $instance = New-CimInstance -Namespace $namespacePath -ClassName $className -Property @{ParentID='./Vendor/MSFT/Policy/Config'; InstanceID=$instanceID} -ErrorAction Stop
        } else {
            $instance = $instances[0]
        }

        $instance = $instance | Set-CimInstance -Property @{$name = $value} -PassThru

        Write-Host "Successfully configured '$name' with value '$value' in class '$className' within namespace '$namespacePath' with InstanceID '$instanceID'." -ForegroundColor Green
    } catch {
        Write-Host "Failed to configure '$name' in class '$className' within namespace '$namespacePath' with InstanceID '$instanceID': $($_.Exception.Message)" -ForegroundColor Red
    }
}


$configItems = Get-Configuration -FilePath "C:\Program Files\BordeauxMetropole\Mona\Uninstall\uninstall_XML.xml"

foreach ($item in $configItems) {
    Update-WMISetting -className $item.ClassName -name $item.PropertyName -value $item.PropertyValue -instanceID $item.InstanceID
}

### Desinstallation SyncML ###
##############################

#Import-Module LocalMDM

#function Unregister-LocalMDM
#{
#
#.SYNOPSIS
#    Unregisters the local MDM server.
 
#.DESCRIPTION
 #   Unregisters the local MDM server. This will in some cases revert any policies configured via the local MDM server back to their default values.
 
#.EXAMPLE
#    Unregister-LocalMDM
 
#.OUTPUTS
#    A message confirming the unregister operation.
 
#>

#[cmdletbinding()]  
#Param(
#)
#    PROCESS {
#        $rc = [MDMLocal.Interface]::UnregisterDeviceWithLocalManagement()
#        Write-Host "Unregisterd, rc = $rc"
#    }
#}

#Import-Module LocalMDM

#Unregister-LocalMDM

### Desinstallation Clef de registre (Keys.ps1) ###
###################################################

##### Suppression des icones ####
#################################

## icône Se deconnecter sur le bureau ##

# Supprimer le raccourci "Se deconnecter" du bureau public
$shortcutPath = "C:\Users\Public\Desktop\Se deconnecter.lnk"
if (Test-Path $shortcutPath) {
    Remove-Item $shortcutPath -Force
    Write-Host "Le raccourci 'Se deconnecter' a ete supprime du bureau public." -ForegroundColor Green
} else {
    Write-Host "Le raccourci 'Se deconnecter' n'existe pas sur le bureau public." -ForegroundColor Yellow
}


## Suppression du raccourci sur le bureau des comptes Admins_Mona permettant le changement des CGU

# Nom du groupe
$groupName = "Admins_Mona"

# Obtenir les membres du groupe
$adminMembers = Get-LocalGroupMember -Group $groupName

# Nom du raccourci à supprimer
$shortcutName = "Accès aux CGU.lnk"

# Parcourir chaque membre du groupe
foreach ($admin in $adminMembers) {
    # Extraire le nom d'utilisateur sans le domaine ou le nom de l'ordinateur
    $username = ($admin.Name -split '\\')[-1]

    # Obtenir le SID de l'utilisateur
    $userSID = $admin.SID

    # Trouver le profil utilisateur basé sur le SID
    $userProfile = (Get-CimInstance -ClassName Win32_UserProfile | Where-Object { $_.SID -eq $userSID }).LocalPath

    if ($userProfile) {
        $desktopPath = Join-Path $userProfile "Desktop"
        $shortcutPath = Join-Path $desktopPath $shortcutName

        if (Test-Path $shortcutPath) {
            Remove-Item $shortcutPath -Force
            Write-Host "Le raccourci '$shortcutName' a été supprimé pour $username." -ForegroundColor Green
        } else {
            Write-Host "Le raccourci '$shortcutName' n'existe pas pour $username." -ForegroundColor Yellow
        }
    } else {
        Write-Host "Profil utilisateur non trouvé pour $username. Raccourci non supprimé." -ForegroundColor Yellow
    }
}

##### Suppression des groupes/Utilisateurs ####
###############################################

## Suppression Gestionnaire / Usagers / Configurer ###

Get-CimInstance -Class Win32_UserProfile | Where-Object { $_.LocalPath.split('\')[-1] -eq 'Usagers' } | Remove-CimInstance
Get-CimInstance -Class Win32_UserProfile | Where-Object { $_.LocalPath.split('\')[-1] -eq 'Configurer' } | Remove-CimInstance

# Suppression des comptes utilisateurs
$usersToRemove = @("Usagers", "Configurer")
foreach ($user in $usersToRemove) {
    if (Get-LocalUser -Name $user -ErrorAction SilentlyContinue) {
        Remove-LocalUser -Name $user
        Write-Host "Le compte utilisateur $user a ete supprime." -ForegroundColor Green
    } else {
        Write-Host "Le compte utilisateur $user n'existe pas." -ForegroundColor Yellow
    }
}

# Reinitialisation du profil par defaut

try {
    # Renommer le dossier C:\Users\Default en Default.old s'il existe
    if (Test-Path "C:\Users\Default") {
        Rename-Item -Path "C:\Users\Default" -NewName "Default.old" -ErrorAction Stop
        Write-Host "Le dossier C:\Users\Default a ete renomme en Default.old avec succes." -ForegroundColor Green
    }

    # Copier le dossier DefaultProfile vers C:\Users\Default
    Copy-Item -Path "C:\ProgramData\BordeauxMetropole\DefaultProfile" -Destination "C:\Users\Default" -Recurse -Force -ErrorAction Stop
    Write-Host "Le profil par defaut a ete restaure avec succes." -ForegroundColor Green

    # Supprimer l'ancien dossier Default.old s'il existe
    if (Test-Path "C:\Users\Default.old") {
        # Prendre possession du dossier et de son contenu
        takeown /F "C:\Users\Default.old" /R /A /D O | Out-Null
        icacls "C:\Users\Default.old" /grant *S-1-5-32-544:F /T | Out-Null

        # Supprimer le dossier en utilisant la commande rd
        cmd /c "rd /s /q C:\Users\Default.old"
        Write-Host "L'ancien dossier Default.old a ete supprime avec succes." -ForegroundColor Green
    }
} catch {
    Write-Host "Une erreur est survenue lors de la reinitialisation du profil par defaut : $_" -ForegroundColor Red
}


# Suppression du groupe Gestionnaire
$groupName = "Gestionnaire"
if (Get-LocalGroup -Name $groupName -ErrorAction SilentlyContinue) {
    Remove-LocalGroup -Name $groupName
    Write-Host "Le groupe $groupName a ete supprime." -ForegroundColor Green
} else {
    Write-Host "Le groupe $groupName n'existe pas." -ForegroundColor Yellow
}

# Suppression du groupe Admins_Mona
$groupName = "Admins_Mona"
if (Get-LocalGroup -Name $groupName -ErrorAction SilentlyContinue) {
    Remove-LocalGroup -Name $groupName
    Write-Host "Le groupe $groupName a ete supprime." -ForegroundColor Green
} else {
    Write-Host "Le groupe $groupName n'existe pas." -ForegroundColor Yellow
}





### Suppression, reset des clefs de registres ###
#################################################

<#

### CLEFFS DE REGISTRE A NE PAS DEFINIR LORS DE L'INSTALLATION ###

La clef de registre HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList a ete creee.
La valeur AdminBMPublic a ete definie dans la clef de registre HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList.

#>



### CLEFS DE REGISRTE A SUPPRIMER ###
#####################################

# Chargement du fichier XML
$configPath = "C:\Program Files\BordeauxMetropole\Mona\Uninstall\reverse_Keys.xml"
if (Test-Path $configPath) {
    $config = [xml](Get-Content $configPath)
    Write-Host "Le fichier de configuration a ete trouve et charge avec succes." -ForegroundColor Green
} else {
    Write-Host "Le fichier de configuration n'a pas ete trouve a l'emplacement specifie." -ForegroundColor Red
    exit
}

# Iteration sur chaque clef de registre a supprimer
foreach ($key in $config.Configuration.SUPPRESSION.RegistryKey) {
    $keyPath = $key.KeyPath
    $valueName = $key.ValueName

    # Verification de l'existence de la clef de registre
    if (Test-Path $keyPath) {
        # Suppression de la valeur specifique
        Remove-ItemProperty -Path $keyPath -Name $valueName -ErrorAction SilentlyContinue
        Write-Host "La valeur $valueName a ete supprimee de la clef de registre $keyPath." -ForegroundColor Green
    } else {
        Write-Host "La clef de registre $keyPath n'existe pas." -ForegroundColor Yellow
    }
}

Write-Host "Suppression des clefs de registre terminee." -ForegroundColor Cyan

### CLEFS DE REGISTRE A SET EN VALEURS PAR DEFAUT L'INSTALLATION ###
####################################################################

# Chargement du fichier XML
$configPath = "C:\Program Files\BordeauxMetropole\Mona\Uninstall\reverse_Keys.xml"
if (Test-Path $configPath) {
    $config = [xml](Get-Content $configPath)
    Write-Host "Le fichier de configuration a ete charge avec succes." -ForegroundColor Green
} else {
    Write-Host "Le fichier de configuration n'a pas ete trouve." -ForegroundColor Red
    exit
}

# Iteration sur chaque clef de registre a modifier
foreach ($key in $config.Configuration.REVERSE.RegistryKey) {
    $keyPath = $key.KeyPath
    $valueName = $key.ValueName
    $data = $key.Data
    $type = $key.Type

    # Verification de l'existence de la clef de registre
    if (Test-Path $keyPath) {
        # Modification de la valeur specifique
        Set-ItemProperty -Path $keyPath -Name $valueName -Value $data -Type $type -ErrorAction SilentlyContinue
        Write-Host "La valeur $valueName a ete modifiee dans la clef de registre $keyPath." -ForegroundColor Green
    } else {
        Write-Host "La clef de registre $keyPath n'existe pas." -ForegroundColor Yellow
    }
}

Write-Host "Modification des clefs de registre terminee." -ForegroundColor Cyan


################## Suppression de l'exclusion de repertoire ######################
#################################################################################

# Suppression de l'exclusion du répertoire d'installation Mona
$cheminExclusion = "C:\Program Files\BordeauxMetropole\Mona"

# Vérifier si le chemin est présent dans les exclusions
$exclusionsExistantes = Get-MpPreference | Select-Object -ExpandProperty ExclusionPath

if ($exclusionsExistantes -contains $cheminExclusion) {
    # Supprimer le chemin des exclusions
    Remove-MpPreference -ExclusionPath $cheminExclusion
    Write-Host "Le chemin '$cheminExclusion' a été supprimé des exclusions de Windows Defender." -ForegroundColor Green
} else {
    Write-Host "Le chemin '$cheminExclusion' n'est pas présent dans les exclusions." -ForegroundColor Yellow
}


################## Suppression des Tâches planifiees ######################
###########################################################################



# Suppression des tâches planifiees

###### Reset Profil Usagers ######
###### DISABLE AUTOLOGON ######
###### FORCE AUTOLOGON ######
###### IMPORTATION PROFIL WIFI ######

$tachesASupprimer = @(
    "Reset Profil Usagers",
    "Disable Autologon",
    "Force Autologon",
    "Importer Profil WiFi a la deconnexion"
)

foreach ($tache in $tachesASupprimer) {
    if (Get-ScheduledTask -TaskName $tache -ErrorAction SilentlyContinue) {
        Unregister-ScheduledTask -TaskName $tache -Confirm:$false
        Write-Host "La tâche planifiee '$tache' a ete supprimee." -ForegroundColor Green
    } else {
        Write-Host "La tâche planifiee '$tache' n'existe pas." -ForegroundColor Yellow
    }
}

Write-Host "Suppression des tâches planifiees terminee." -ForegroundColor Cyan



Stop-Transcript

exit 0

