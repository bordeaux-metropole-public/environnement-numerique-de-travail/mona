﻿Start-Transcript -Path "C:\ProgramData\BordeauxMetropole\Mona\reset_profil_usagers.txt"

$UserName = "Usagers"
$localUser = New-Object System.Security.Principal.NTAccount($UserName)
$userSID = $localUser.Translate([System.Security.Principal.SecurityIdentifier])

# Chemin de la clé "State"
$stateKeyPath = "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\$userSID"

# Récupération de la valeur actuelle de la clé "State"
$stateValue = Get-ItemProperty -Path $stateKeyPath -Name "State"

# Incrémentation de la valeur de 1
$newStateValue = $stateValue.State -bor 0x1

# Mise à jour de la valeur de la clé "State"
Set-ItemProperty -Path $stateKeyPath -Name "State" -Value $newStateValue

Write-Host "La valeur de State a été incrémentée à $newStateValue" -ForegroundColor Green

Stop-Transcript

