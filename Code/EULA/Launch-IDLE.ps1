param(
  [switch]$Configure
)

$Script:ScriptPath = $PSScriptRoot
$Script:ScriptFullName = $PSCommandPath
if([String]::IsNullOrEmpty($Script:ScriptPath)) {
  if([System.IO.Path]::GetExtension($PSCommandPath) -eq '.ps1') {
    $Script:ScriptPath = Split-Path -Parent $PSCommandPath
    $Script:ScriptFullName = $PSCommandPath
  } elseif($hostinvocation -ne $null) {
    $Script:ScriptPath = Split-Path -Parent $hostinvocation.MyCommand.path
    $Script:ScriptFullName = $hostinvocation.MyCommand.path
  }	elseif($script:MyInvocation -ne $null) {
    $Script:ScriptPath = Split-Path -Parent $script:MyInvocation.MyCommand.Path
    $Script:ScriptFullName = $script:MyInvocation.MyCommand.Path
  } else {
    $Script:ScriptPath = Split-Path -Parent ([System.Diagnostics.Process]::GetCurrentProcess().MainModule.FileName)
    $Script:ScriptFullName = [System.Diagnostics.Process]::GetCurrentProcess().MainModule.FileName
  }
}

Function Set-InactivityTime {
  Param(
    [int]$Minutes
  )
  
  $scriptPath = $Script:ScriptFullName
  $content = Get-Content $scriptPath -Raw
  $newContent = $content -replace '(\$MaxInactivityTime)\s?=\s?\d+', ('$1 = {0}' -f $Minutes)
  $newContent | Set-Content $scriptPath
}

function start-configure {
  [xml]$xaml = @'
  <Window
   xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
          xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
          xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
          xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
          Title="InactivityCountdown"
          SizeToContent="WidthAndHeight"
          ShowInTaskbar="False"
          WindowStartupLocation="Manual" WindowStyle="ToolWindow">
    <StackPanel Orientation="Vertical" HorizontalAlignment="Center" VerticalAlignment="Center" Margin="10">
        <Label Content="Temps d'inactivite :" Width="150" Margin="10"/>
        <ComboBox Name="cmbTime" Width="150" HorizontalAlignment="Center">
            <ComboBoxItem>2</ComboBoxItem>
            <ComboBoxItem>3</ComboBoxItem>
            <ComboBoxItem>5</ComboBoxItem>
            <ComboBoxItem>8</ComboBoxItem>
            <ComboBoxItem>10</ComboBoxItem>
        </ComboBox>
        <Button Name="btnValidate" Content="Valider" Margin="10" />
    </StackPanel>
  </Window>
'@

  $libs = @('System.Windows.Forms', 'PresentationFramework', 'System.DirectoryServices.AccountManagement')
  foreach($lib in $libs) { Add-Type -AssemblyName $lib }

  $reader = (New-Object System.Xml.XmlNodeReader $xaml) 
  $Form = [Windows.Markup.XamlReader]::Load($reader)
  $xaml.SelectNodes("//*[@Name]") | %{Set-Variable -Name ($_.Name) -Value $Form.FindName($_.Name)}

  $btnValidate.Add_Click({
    Set-InactivityTime -Minutes $cmbTime.SelectedItem.Content.ToString()
    $Form.Close()
  })

  $Form.ShowDialog()
}

try {
  $valeurreg = Get-ItemPropertyValue -Path "HKLM:\SOFTWARE\MonaAssistant" -Name "IdleTimeout" -ErrorAction Stop
  $valeurreg = [int]$valeurreg  # Conversion en entier
  
} catch {
  $valeurreg = 5 # Valeur par defaut si la lecture echoue
  
}

Function start-idle {
  # Variables de configuration pour la detection d'inactivite
  $Script:MaxInactivityTime = $valeurreg # Temps maximum d'inactivite autorise (en minutes)
  $Script:WarningLevel = 2.5 # Niveau d'avertissement avant fermeture (en minutes)
  $Script:AlerteLevel = 1 # Niveau d'alerte avant fermeture (en minutes)
  $Script:CountDownStep = 5 # Pas de decompte (en secondes)

  $libs = @('System.Windows.Forms', 'PresentationFramework', 'System.Windows.Interop')
  foreach($lib in $libs) { [void][System.Reflection.Assembly]::LoadWithPartialName($lib) }

Add-Type -ReferencedAssemblies 'System', 'System.Runtime.InteropServices' -Language CSharp -TypeDefinition @"
  using System;
  using System.Runtime.InteropServices;
  
  public class Win32Helper {
    public const long WS_EX_TOOLWINDOW = 0x00000080L;
    public const long GWL_EXSTYLE = -20;
    
    [DllImport("user32.dll")]
    public static extern long SetWindowLongPtr(long hWnd, long nIndex, long dwNewLong);
    [DllImport("user32.dll")]
    public static extern int FindWindow(string className, string windowText);
    [DllImport("user32.dll")]
    public static extern int ShowWindow(int hwnd, int command);
    
    private Win32Helper() {
      // hide ctor
    }
  }
  
  public class DPI {
    [DllImport("user32.dll")]
    static extern IntPtr GetDC(IntPtr ptr);
    
    [DllImport("user32.dll", EntryPoint = "ReleaseDC")]
    static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);
    
    [DllImport("gdi32.dll")]
    static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

    public enum DeviceCap {
      VERTRES = 10,
      DESKTOPVERTRES = 117  
    }

    private DPI() {
      // hide ctor
    }

    public static float scaling() {
      IntPtr desktop = GetDC(IntPtr.Zero);
      int LogicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);
      int PhysicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);
      ReleaseDC(IntPtr.Zero, desktop);

      return (float)PhysicalScreenHeight / (float)LogicalScreenHeight;
    }
  }
  
  public class Taskbar {
    private const int SW_HIDE = 0;
    private const int SW_SHOW = 1;
    private static int Handle {
      get {
        return Win32Helper.FindWindow("Shell_TrayWnd", "");
      }
    }
    private Taskbar() {
      // hide ctor
    }
    public static void Show() {
      Win32Helper.ShowWindow(Handle, SW_SHOW);
    }
    public static void Hide() {
      Win32Helper.ShowWindow(Handle, SW_HIDE);
    }
  }
"@
  
# Ajout du type .NET pour interagir avec l'API Windows pour l'authentification
Add-Type -TypeDefinition @"
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Util {
  public class Creds {
  const int MAX_USER_NAME = 100;
  const int MAX_PASSWORD = 100;
  const int MAX_DOMAIN  = 100;
  
  [DllImport("credui", CharSet = CharSet.Unicode)]
  private static extern CredUIReturnCodes CredUIPromptForCredentialsW(
            ref CREDUI_INFO creditUR,
            string targetName,
            IntPtr reserved1,
            int iError,
            StringBuilder userName,
            int maxUserName,
            StringBuilder password,
            int maxPassword,
            [MarshalAs(UnmanagedType.Bool)] ref bool pfSave,
            CREDUI_FLAGS flags);
  
    public enum CredUIReturnCodes
  {
            NO_ERROR = 0,
            ERROR_CANCELLED = 1223,
            ERROR_NO_SUCH_LOGON_SESSION = 1312,
            ERROR_NOT_FOUND = 1168,
            ERROR_INVALID_ACCOUNT_NAME = 1315,
            ERROR_INSUFFICIENT_BUFFER = 122,
            ERROR_INVALID_PARAMETER = 87,
            ERROR_INVALID_FLAGS = 1004,
            ERROR_BAD_ARGUMENTS = 160
  }
  
  [Flags]
        public enum CREDUI_FLAGS
        {
            INCORRECT_PASSWORD = 0x1,
            DO_NOT_PERSIST = 0x2,
            REQUEST_ADMINISTRATOR = 0x4,
            EXCLUDE_CERTIFICATES = 0x8,
            REQUIRE_CERTIFICATE = 0x10,
            SHOW_SAVE_CHECK_BOX = 0x40,
            ALWAYS_SHOW_UI = 0x80,
            REQUIRE_SMARTCARD = 0x100,
            PASSWORD_ONLY_OK = 0x200,
            VALIDATE_USERNAME = 0x400,
            COMPLETE_USERNAME = 0x800,
            PERSIST = 0x1000,
            SERVER_CREDENTIAL = 0x4000,
            EXPECT_CONFIRMATION = 0x20000,
            GENERIC_CREDENTIALS = 0x40000,
            USERNAME_TARGET_CREDENTIALS = 0x80000,
            KEEP_USERNAME = 0x100000,
        }
  
  [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
  public struct CREDUI_INFO
  {
            public int cbSize;
            public IntPtr hwndParent;
            public string pszMessageText;
            public string pszCaptionText;
            public IntPtr hbmBanner;
  }
  
  public static CredUIReturnCodes PromptForCredentials(
            string targetName,
            ref string userName,
            ref string password)
  {
    
    bool savePwd = false;
    
    CREDUI_FLAGS flags = CREDUI_FLAGS.GENERIC_CREDENTIALS |
          CREDUI_FLAGS.DO_NOT_PERSIST |
          CREDUI_FLAGS.ALWAYS_SHOW_UI;
    
    CREDUI_INFO creditUI = new CREDUI_INFO();
    creditUI.pszCaptionText = "Authentification";  
    creditUI.pszMessageText = "Saisissez votre login et mot de passe";
    creditUI.cbSize = Marshal.SizeOf(creditUI);
    
    StringBuilder user = new StringBuilder(MAX_USER_NAME);
    StringBuilder pwd = new StringBuilder(MAX_PASSWORD);
    
    CredUIReturnCodes result = CredUIPromptForCredentialsW(
                  ref creditUI,
                  targetName,
                  IntPtr.Zero,
                  0,
                  user,
                  MAX_USER_NAME,
                  pwd,
                  MAX_PASSWORD,
                  ref savePwd,
                  flags);

    userName = user.ToString();
    password = pwd.ToString();
    
    return result;
  }

  }
}
"@
  
  Add-Type -ReferencedAssemblies System.Drawing -TypeDefinition @'
  using System;
  using System.Runtime.InteropServices;

  namespace Helper {
    public static class IdleTimeDetector {
      internal struct LASTINPUTINFO {
        public uint cbSize;
        public uint dwTime;
      }
      
      [DllImport("user32.dll")]
      static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

      static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
      static readonly IntPtr HWND_TOP = new IntPtr(0);
      const UInt32 SWP_NOSIZE = 0x0001;
      const UInt32 SWP_NOMOVE = 0x0002;
      const UInt32 SWP_NOACTIVATE = 0x0010;
      
      [DllImport("user32.dll")]
      static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

      public static void SendBackWindow(IntPtr Handle) {
        SetWindowPos(Handle, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
      }
      
      public static void SendFrontWindow(IntPtr Handle) {
        SetWindowPos(Handle, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
      }

      public static IdleTimeInfo GetIdleTimeInfo() {
        int systemUptime = Environment.TickCount,
          lastInputTicks = 0,
          idleTicks = 0;

        LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
        lastInputInfo.cbSize = (uint)Marshal.SizeOf(lastInputInfo);
        lastInputInfo.dwTime = 0;

        if (GetLastInputInfo(ref lastInputInfo)) {
          lastInputTicks = (int)lastInputInfo.dwTime;

          idleTicks = systemUptime - lastInputTicks;
        }

        return new IdleTimeInfo {
          LastInputTime = DateTime.Now.AddMilliseconds(-1 * idleTicks),
          IdleTime = new TimeSpan(0, 0, 0, 0, idleTicks),
          SystemUptimeMilliseconds = systemUptime,
        };
      }
    }

    public class IdleTimeInfo {
      public DateTime LastInputTime { get; internal set; }
      public TimeSpan IdleTime { get; internal set; }
      public int SystemUptimeMilliseconds { get; internal set; }
    }
  }
'@

$Global:CurrentDPI = [DPI]::Scaling()

  # Chargement et preparation de l'interface XAML pour l'affichage des CGU et la gestion de l'inactivite
  [xml]$xaml = @'
  <Window
   xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
          xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
          xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
          xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
          Style="{DynamicResource WindowStyleRounded}"
          Title="InactivityCountdown"
          SizeToContent="WidthAndHeight"
          ShowInTaskbar="False"
          WindowStartupLocation="Manual">
    <Window.Resources>
      <Style x:Key="SliderCheckBox" TargetType="{x:Type CheckBox}">
        <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}"/>
        <Setter Property="BorderThickness" Value="1"/>
        <Setter Property="Cursor" Value="Hand"/>
        <Setter Property="Template">
          <Setter.Value>
            <ControlTemplate TargetType="{x:Type CheckBox}">
              <ControlTemplate.Resources>
                <Color A="255" R="0" G="139" B="0" x:Key="IsCheckedBackground"/><!-- #008B00 -->
                <Color A="255" R="176" G="0" B="32" x:Key="IsCheckedOffBackground"/><!-- #B00020 -->
                <Storyboard x:Key="StoryboardIsCheckedOff">
                  <ThicknessAnimationUsingKeyFrames Storyboard.TargetProperty="Margin" Storyboard.TargetName="CheckFlag">
                    <SplineThicknessKeyFrame KeyTime="0" Value="14, 0, 0, 0"/>
                    <SplineThicknessKeyFrame KeyTime="0:0:0.2" Value="0, 0, 14, 0"/>
                  </ThicknessAnimationUsingKeyFrames>
                  <ColorAnimation Storyboard.TargetName="ForegroundPanel" Storyboard.TargetProperty="Background.Color" From="{StaticResource IsCheckedBackground}" To="{StaticResource IsCheckedOffBackground}" Duration="0:0:0.2"/>
                </Storyboard>
                <Storyboard x:Key="StoryboardIsChecked">
                  <ThicknessAnimationUsingKeyFrames Storyboard.TargetProperty="Margin" Storyboard.TargetName="CheckFlag">
                    <SplineThicknessKeyFrame KeyTime="0" Value="0, 0, 14, 0"/>
                    <SplineThicknessKeyFrame KeyTime="0:0:0.2" Value="14, 0, 0, 0"/>
                  </ThicknessAnimationUsingKeyFrames>
                  <ColorAnimation Storyboard.TargetName="ForegroundPanel" Storyboard.TargetProperty="Background.Color" From="{StaticResource IsCheckedOffBackground}" To="{StaticResource IsCheckedBackground}" Duration="0:0:0.2"/>
                </Storyboard>
              </ControlTemplate.Resources>
              <BulletDecorator Background="Transparent" SnapsToDevicePixels="True">
                <BulletDecorator.Bullet>
                  <Border Name="ForegroundPanel" BorderThickness="1" Width="35" Height="20" CornerRadius="10">
                    <Border.Background>
                      <SolidColorBrush/>
                    </Border.Background>
                    <Canvas>
                      <Border Name="CheckFlag" Background="White" CornerRadius="10" VerticalAlignment="Center" BorderThickness="1" Width="18" Height="18" RenderTransformOrigin="0.5,0.5">
                        <Border.Effect>
                          <DropShadowEffect ShadowDepth="1" Direction="180"/>
                        </Border.Effect>
                      </Border>
                    </Canvas>
                  </Border>
                </BulletDecorator.Bullet>
                <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" Margin="{TemplateBinding Padding}" RecognizesAccessKey="True" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}" VerticalAlignment="Center">
                  <ContentPresenter.Effect>
                    <DropShadowEffect ShadowDepth="1.5" Color="LightGray"/>
                  </ContentPresenter.Effect>
                </ContentPresenter>
              </BulletDecorator>
              <ControlTemplate.Triggers>
                <Trigger Property="HasContent" Value="true">
                  <Setter Property="Padding" Value="5,0,0,0"/>
                </Trigger>
                <Trigger Property="IsEnabled" Value="false">
                  <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.GrayTextBrushKey}}"/>
                </Trigger>
                <Trigger Property="IsChecked" Value="True">
                  <Trigger.EnterActions>
                    <BeginStoryboard Name="BeginStoryboardCheckedTrue" Storyboard="{StaticResource StoryboardIsChecked}"/>
                    <RemoveStoryboard BeginStoryboardName="BeginStoryboardCheckedFalse"/>
                  </Trigger.EnterActions>
                </Trigger>
                <Trigger Property="IsChecked" Value="False">
                  <Trigger.EnterActions>
                    <BeginStoryboard Name="BeginStoryboardCheckedFalse" Storyboard="{StaticResource StoryboardIsCheckedOff}"/>
                    <RemoveStoryboard BeginStoryboardName="BeginStoryboardCheckedTrue"/>
                  </Trigger.EnterActions>
                </Trigger>
              </ControlTemplate.Triggers>
            </ControlTemplate>
          </Setter.Value>
        </Setter>
      </Style>
    
      <Style x:Key="WindowStyleRounded" TargetType="{x:Type Window}">
        <Setter Property="WindowStyle" Value="None"/>
        <Setter Property="AllowsTransparency" Value="True"/>
        <Setter Property="ResizeMode" Value="NoResize"/>
        <Setter Property="Background" Value="Transparent"/>
        <Setter Property="BorderThickness" Value="0"/>
        <Setter Property="Topmost" Value="True"/>
        <Setter Property="Template">
          <Setter.Value>
            <ControlTemplate TargetType="{x:Type Window}">
              <Border x:Name="winBorder" BorderBrush="#0096D6" Background="GhostWhite" BorderThickness="1" Margin="5,2,5,5" CornerRadius="8" HorizontalAlignment="Center" VerticalAlignment="Center">
                <Border.Effect>
                  <DropShadowEffect BlurRadius="5" Direction="-45" RenderingBias="Quality" ShadowDepth="1.5"/>
                </Border.Effect>
                <ContentPresenter/>
              </Border>
            </ControlTemplate>
          </Setter.Value>
        </Setter>
      </Style>
      
      <Style TargetType="{x:Type Label}">
        <Setter Property="Foreground" Value="Black"/>
        <Setter Property="Template">
          <Setter.Value>
            <ControlTemplate TargetType="{x:Type Label}">
              <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" VerticalAlignment="{TemplateBinding VerticalContentAlignment}" SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}" RecognizesAccessKey="True">
                <ContentPresenter.Effect>
                  <DropShadowEffect Direction="-60" RenderingBias="Quality" ShadowDepth="1" Color="Gray"/>
                </ContentPresenter.Effect>
              </ContentPresenter>
            </ControlTemplate>
          </Setter.Value>
        </Setter>
      </Style>
      
      <Style TargetType="{x:Type Label}" BasedOn="{StaticResource {x:Type Label}}" x:Key="Warning">
        <Setter Property="Foreground" Value="Orange"/>
      </Style>
      
      <Style TargetType="{x:Type Label}" BasedOn="{StaticResource Warning}" x:Key="AnimatedLabel">
        <Setter Property="RenderTransformOrigin" Value="0.5,0.5"/>
        <Setter Property="RenderTransform">
          <Setter.Value>
            <ScaleTransform x:Name="transform"/>
          </Setter.Value>
        </Setter>
      </Style>
      
      <Storyboard x:Key="Animated" RepeatBehavior="Forever">
        <ColorAnimation To="Red" BeginTime="00:00:00.00" Storyboard.TargetProperty="(Foreground).(SolidColorBrush.Color)" Duration="00:00:01.00"/>
        <ColorAnimation From="Red" BeginTime="00:00:01.00" Storyboard.TargetProperty="(Foreground).(SolidColorBrush.Color)" Duration="00:00:01.00"/>
        <DoubleAnimation BeginTime="00:00:00.00" Storyboard.TargetProperty="RenderTransform.ScaleX" To="10" Duration="00:00:01.00"/>
        <DoubleAnimation BeginTime="00:00:00.00" Storyboard.TargetProperty="RenderTransform.ScaleY" To="10" Duration="00:00:01.00"/>
        <DoubleAnimation BeginTime="00:00:01.00" Storyboard.TargetProperty="RenderTransform.ScaleX" To="1" Duration="00:00:01.00"/>
        <DoubleAnimation BeginTime="00:00:01.00" Storyboard.TargetProperty="RenderTransform.ScaleY" To="1" Duration="00:00:01.00"/>
      </Storyboard>
    </Window.Resources>
    
    <StackPanel Margin="0">
      <StackPanel Name="Eula" Margin="0" Width="300" Visibility="Collapsed">
        <Label Margin="0" Padding="0" FontSize="16" HorizontalAlignment="Center" VerticalAlignment="Center">
        <Label.Content>
            <AccessText TextAlignment="Center" TextWrapping="Wrap" Text="Pour des raisons de securite et de confidentialite, la session sera automatiquement fermee apres 5 minutes d'inactivitee (aucun mouvement de souris ou de clavier).&#10;L'ensemble des donnees seront supprimees.&#10;"/>
        </Label.Content>
        </Label>
        <Button Name="btnAccept" Content="J'ai compris" HorizontalAlignment="Center" VerticalAlignment="Center" Width="75" />
      </StackPanel>
      <StackPanel Name="Content" Margin="0" Width="150">
        <Label Margin="0" Padding="0" Panel.ZIndex="1" Name="lblInactivityCountdown" Content="Je suis un label" FontSize="38" FontFamily="Calibri" HorizontalAlignment="Center" VerticalAlignment="Center"/>
        <CheckBox Name="chkTopMost" Margin="10,0,0,8" Content="Toujours visible" HorizontalAlignment="Left" VerticalAlignment="Center" Style="{StaticResource SliderCheckBox}"/>
        <CheckBox Name="chkSuspend" Visibility="Collapsed" Margin="10,0,0,4" Content="Suspendre" HorizontalAlignment="Left" VerticalAlignment="Center" Style="{StaticResource SliderCheckBox}"/>
        <CheckBox Name="chkAutologonExit" Visibility="Collapsed" Margin="10,0,0,4" Content="Se Deconnecter" HorizontalAlignment="Left" VerticalAlignment="Center" Style="{StaticResource SliderCheckBox}"/>

      </StackPanel>
    </StackPanel>
    
  </Window>
'@

  $Global:UnlockTimeout = $false
  $Global:Animated = $false

  $libs = @('System.Windows.Forms', 'PresentationFramework', 'System.DirectoryServices.AccountManagement')
  foreach($lib in $libs) { Add-Type -AssemblyName $lib }

  $reader=(New-Object System.Xml.XmlNodeReader $xaml) 
  $Form=[Windows.Markup.XamlReader]::Load($reader)
  $xaml.SelectNodes("//*[@Name]") | %{Set-Variable -Name ($_.Name) -Value $Form.FindName($_.Name)}

  function Move-Windows {
    $wndHelper = [System.Windows.Interop.WindowInteropHelper]::new($Form).Handle
    $Screen = [System.Windows.Forms.Screen]::FromHandle($wndHelper)
    $border = $Form.Template.FindName('winBorder', $Form)
    
    $Form.Left = ($Screen.WorkingArea.Width / $Global:CurrentDPI) - $Form.ActualWidth
    $Form.Top = ($Screen.WorkingArea.Height / $Global:CurrentDPI) - $Form.ActualHeight
  }

  $Form.Add_Loaded({
    [Win32Helper]::SetWindowLongPtr([System.Windows.Interop.WindowInteropHelper]::new($Form).Handle, [Win32Helper]::GWL_EXSTYLE, [Win32Helper]::WS_EX_TOOLWINDOW)
    $chkTopMost.IsChecked = $Form.TopMost
    Move-Windows
    
    $lblInactivityCountdown.Content = [TimeSpan]::FromMinutes($MaxInactivityTime).ToString('mm\:ss')
    
    $global:InactivityDT = New-Object -TypeName System.Windows.Threading.DispatcherTimer
    $global:InactivityDT.Interval = [TimeSpan]::FromMilliseconds(500)
    $global:InactivityDT.Add_Tick({
      $val = [Helper.IdleTimeDetector]::GetIdleTimeInfo().IdleTime.TotalSeconds
        
      if((($MaxInactivityTime * 60) - $val) -gt ($AlerteLevel * 60)) {
        if($Global:Animated) {

          $Form.TopMost = $chkTopMost.IsChecked
          if(-Not $Form.TopMost) { [Helper.IdleTimeDetector]::SendBackWindow([System.Windows.Interop.WindowInteropHelper]::new($Form).Handle) }

          $Form.SizeToContent = 'WidthAndHeight'
          
          $Form.Left = $Global:SavePosition.X
          $Form.Top = $Global:SavePosition.Y
          
          $Form.TryFindResource("Animated").Stop()
          $Global:Animated = $false
        }
        
        if((($MaxInactivityTime * 60) - $val) -le ($WarningLevel * 60)) { $lblInactivityCountdown.Style = $Form.TryFindResource("Warning") }
        else { $lblInactivityCountdown.Style = $Form.TryFindResource($lblInactivityCountdown.GetType()) }
        
        $valStep = [Timespan]::FromSeconds(($MaxInactivityTime * 60) - ( $CountDownStep * [math]::Floor($val / $CountDownStep)))
        $lblInactivityCountdown.Content = $valStep.ToString('mm\:ss')
      } else {
        $lblInactivityCountdown.Style = $Form.TryFindResource("AnimatedLabel")
        $lblInactivityCountdown.Content = [TimeSpan]::FromSeconds(($MaxInactivityTime * 60) - $val).ToString('mm\:ss')
        
        if(-Not $Global:Animated) {
          $Global:SavePosition = [System.Windows.Point]::new($Form.Left, $Form.Top)

          $wndHelper = [System.Windows.Interop.WindowInteropHelper]::new($Form).Handle
          $Screen = [System.Windows.Forms.Screen]::FromHandle($wndHelper)

          $Form.SizeToContent = 'Manual'
          $Form.Height = 350
          $Form.Width = 850
          $Form.UpdateLayout()
          
          $Form.Left = (($Screen.WorkingArea.Width / $Global:CurrentDPI) / 2) - ( $Form.ActualWidth / 2 )
          $Form.Top = (($Screen.WorkingArea.Height / $Global:CurrentDPI) / 2) - ( $Form.ActualHeight / 2 )
          
          $SB = $Form.TryFindResource("Animated")
          $SB.Stop()
          $SB.Children | %{
            [System.Windows.Media.Animation.Storyboard]::SetTarget($_, $lblInactivityCountdown)
          }
          $SB.Begin()
          $Global:Animated = $true
          
          [Helper.IdleTimeDetector]::SendFrontWindow([System.Windows.Interop.WindowInteropHelper]::new($Form).Handle)
          $Form.TopMost = $True
        }
      }
      
      if( $val -ge ($MaxInactivityTime * 60)) { Start-Process -FilePath 'C:\Windows\System32\logoff.exe'; exit 0 }
    })
    
    $global:InactivityDT.Start()
  })

  $Form.Add_Closing({
    param([object]$sender, [System.EventArgs]$e)
    if($Global:AllowClosing) { return }
    $e.Cancel = $True
  })

  $Form.Add_MouseLeftButtonDown({
    param([object]$sender, [System.EventArgs]$e)
      
    $wndHelper = [System.Windows.Interop.WindowInteropHelper]::new($Form).Handle
    $Screen = [System.Windows.Forms.Screen]::FromHandle($wndHelper)
    $border = $Form.Template.FindName('winBorder', $Form)

    $this.DragMove()
    
    $p1 = $border.TransformToVisual($Form).Transform(([System.Windows.Point]::new(0, 0)))
    
    if(($Form.Left + $p1.X) -lt 0) { $Form.Left -= ($Form.Left + $p1.X) }
    if(($Form.Left + $Form.ActualWidth - $p1.X) -gt ($Screen.WorkingArea.Width / $Global:CurrentDPI)) { $Form.Left += (($Screen.WorkingArea.Width / $Global:CurrentDPI) - ($Form.Left + $Form.ActualWidth - $p1.X)) }

    if(($Form.Top + $Form.ActualHeight - $p1.Y) -gt ($Screen.WorkingArea.Height / $Global:CurrentDPI)) { $Form.Top += ($Screen.WorkingArea.Height / $Global:CurrentDPI) - ($Form.Top + $Form.ActualHeight - $p1.Y) }
  })

  $Form.Add_MouseDoubleClick({
    # Cette partie permet la fermeture si Ctrl et Shift gauches sont enfonces
    if ([System.Windows.Input.Keyboard]::IsKeyDown([System.Windows.Input.Key]::LeftShift) -and 
        [System.Windows.Input.Keyboard]::IsKeyDown([System.Windows.Input.Key]::LeftCtrl)) { 
        $Global:AllowClosing = $True 
    }

    # Cette partie gere la visibilite des boutons independamment de la fermeture
    if ([System.Windows.Input.Keyboard]::IsKeyDown([System.Windows.Input.Key]::LeftShift)) {
        $chkSuspend.Visibility = if ($chkSuspend.Visibility -eq 'Collapsed') { 'Visible' } else { 'Collapsed' }
        $chkAutologonExit.Visibility = if ($chkAutologonExit.Visibility -eq 'Collapsed') { 'Visible' } else { 'Collapsed' }
    }
})


  $chkSuspend.Add_Click({
    $Global:Animated = $false
    
    if(-Not $chkSuspend.IsChecked) {
      $global:InactivityDT.Start()
    } else {
      $Username = ''
      $Password = ''
      [Util.Creds]::PromptForCredentials('EULA', [ref]$Username, [ref]$Password)
      
      $ValidCreds = Validate-LocalCredentials -Username $Username -Password $Password
      
      $GestionnaireMembers = (Get-LocalGroupMember 'Gestionnaire').Name
      
      if($ValidCreds -and ($GestionnaireMembers -contains $UserName -or $GestionnaireMembers -contains "$($Env:ComputerName)\$UserName" )) {
        $lblInactivityCountdown.Content = 'Stop'
        $global:InactivityDT.Stop()
      }
      else {
        $chkSuspend.IsChecked = $false
        $chkSuspend.Visibility = 'Collapsed'
		
		$chkAutologonExit.IsChecked = $false
        $chkAutologonExit.Visibility = 'Collapsed'
      }
    }
  })

# Recuperation de la tache planifiee 'Disable Autologon' pour desactiver l'autologon #
$chkAutologonExit.Add_Click({
  $Global:Animated = $false

  if (-Not $chkAutologonExit.IsChecked) {
      $global:InactivityDT.Start()
  } else {
      $Username = ''
      $Password = ''
      [Util.Creds]::PromptForCredentials('EULA', [ref]$Username, [ref]$Password)

      # Valider les credentials (Supposons que Validate-LocalCredentials retourne $true si les credentials sont valides)
      $ValidCreds = $true

      $GestionnaireMembers = (Get-LocalGroupMember 'Gestionnaire').Name

      if ($ValidCreds -and ($GestionnaireMembers -contains $Username -or $GestionnaireMembers -contains "$($Env:ComputerName)\$Username")) {
        $lblInactivityCountdown.Content = 'Stop'

          # Utiliser les credentials saisis pour demarrer la tache planifiee
          $securePassword = ConvertTo-SecureString $Password -AsPlainText -Force
          $pi = New-Object System.Diagnostics.ProcessStartInfo
          $pi.FileName = 'cmd.exe'
          $pi.Arguments = '/c schtasks /run /tn "Disable Autologon"' # Commande pour demarrer la tache planifiee
          $pi.UserName = $Username
          $pi.Password = $securePassword
          $pi.UseShellExecute = $false
          $pi.Domain = $Env:ComputerName # Optionnel, specifier le domaine si necessaire
          $process = [System.Diagnostics.Process]::Start($pi)

          # Attendre que le processus se termine
          $process.WaitForExit()

          # Verifier si la tache a ete lancee avec succes (code de sortie 0)
          if ($process.ExitCode -eq 0) {
              # Effectuer un log off de la session
              logoff
          } else {
              Write-Warning "La tache 'Disable Autologon' n'a pas pu etre lancee. Code de sortie : $($process.ExitCode)"
          }
      } else {
          $chkAutologonExit.IsChecked = $false
          $chkAutologonExit.Visibility = 'Collapsed'
		  
		  $chkSuspend.IsChecked = $false
		  $chkSuspend.Visibility = 'Collapsed'
      }
  }
})

  
   $chkTopMost.Add_Click({
    $Form.TopMost = $chkTopMost.IsChecked
  })

  $btnAccept.Add_Click({
    $Eula.Visibility = 'Collapsed'
    $Content.Visibility = 'Visible'
    $Form.UpdateLayout()
    
    $wndHelper = [System.Windows.Interop.WindowInteropHelper]::new($Form).Handle
    $Screen = [System.Windows.Forms.Screen]::FromHandle($wndHelper)
    $Form.Left = (($Screen.WorkingArea.Width / $Global:CurrentDPI) / 2) - ( $Form.ActualWidth / 2 )
    $Form.Top = (($Screen.WorkingArea.Height / $Global:CurrentDPI) / 2) - ( $Form.ActualHeight / 2 )
  })

  function Validate-LocalCredentials {
    param($Username, $Password)
    
    try {
      Add-Type -AssemblyName System.DirectoryServices.AccountManagement
      $PC = [System.DirectoryServices.AccountManagement.PrincipalContext]::new('Machine')
      return $PC.ValidateCredentials($Username, $Password)
    } catch { return $False }

  }
  
  $Form.ShowDialog()
}

if($Configure) { start-configure }
else { start-idle }




