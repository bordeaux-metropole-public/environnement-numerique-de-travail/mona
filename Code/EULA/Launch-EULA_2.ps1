####################################################################################
# Script d'affichage des EULA pour poste libre service  USAGERS                    #
#                                                                                  #
# Bordeaux Metropole                                                               #
#                                                                                  #
# ECONOCOM                                                                         #
# Richard Durand                                                                   #
# V1.4                                                                             #
# 20/02/2023                                                                       #
#                                                                                  #
#                                                                                  #
# changeLog                                                                        #
# V1.0 04/01/2023 initial                                                          #
# V1.1 18/01/2023 Ajout de la fonction idle (detection inactivite)                 #
# V1.2 25/01/2023 Modification interface                                           #
# V1.3 14/02/2023 Modification interface                                           #
# V1.4 20/02/2023 Modification interface, passage Webview2 (Edge)                  #
# V1.5 26/04/2023 Ajout countdown                                                  #
# V1.6 18/03/2024 Ajout parametres "Disable Autologon" + Comments                  #
#                                                                                  #
####################################################################################

# Variables
$Debug = $False
$showEULAUsers = @('USAGERS')
$MyDir = [System.IO.Path]::GetDirectoryName($myInvocation.MyCommand.Definition)

$UserProfileLetter = 'Z:'

$KeyPath = 'HKLM:\SOFTWARE\NEO'

$UserDataFolder = "$UserProfileLetter\AppData\Local\Temp"

$Flag_Action = $False

$Script:ScriptPath = $PSScriptRoot
if([String]::IsNullOrEmpty($Script:ScriptPath)) {
  if([System.IO.Path]::GetExtension($PSCommandPath) -eq '.ps1') {
    $Script:ScriptPath = Split-Path -Parent $PSCommandPath
  } elseif($hostinvocation -ne $null) {
    $Script:ScriptPath = Split-Path -Parent $hostinvocation.MyCommand.path
  } elseif($script:MyInvocation -ne $null) {
    $Script:ScriptPath = Split-Path -Parent $script:MyInvocation.MyCommand.Path
  } else {
    $Script:ScriptPath = Split-Path -Parent ([System.Diagnostics.Process]::GetCurrentProcess().MainModule.FileName)
  }
}

$idleScript = "$($Script:ScriptPath)\Launch-IDLE.ps1"

# ---------------------------------------------------------------------------------
# Verification de l'appartenance au groupe 'Gestionnaire' et lancement du script 
# WiFiMonitor.ps1 si necessaire
# ---------------------------------------------------------------------------------
$UserPrincipal = New-Object System.Security.Principal.WindowsPrincipal([System.Security.Principal.WindowsIdentity]::GetCurrent())

if ($UserPrincipal.IsInRole("Gestionnaire")) {
    Start-Process -FilePath powershell.exe -ArgumentList '-ExecutionPolicy Bypass -File "C:\Program Files\BordeauxMetropole\Mona\Wifi\WiFiMonitor.ps1"' -WindowStyle Hidden
}

#==============================================================================================
# Debut Fonction
#==============================================================================================
#------------------------------------------
# Fonction idle (detection d'inactivite)
#------------------------------------------

function Start-EdgeSizedWindow {
  try { [Win32] | Out-Null } catch {
    Add-Type @"
    using System;
    using System.Runtime.InteropServices;
  
    public class Win32 {
      [DllImport("user32.dll")]
      [return: MarshalAs(UnmanagedType.Bool)]
      public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
    }
"@
  }
  [void][System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')
  
  [void][System.Diagnostics.Process]::Start('msedge.exe')

  [IntPtr]$mWin = [IntPtr]::Zero
  do {
    try {
      [IntPtr]$mWin = (Get-Process msedge | ?{ $_.MainWindowHandle -ne 0 }).MainWindowHandle
    } catch { [IntPtr]$mWin = [IntPtr]::Zero }
  } until($mWin -ne [IntPtr]::Zero)

  $Screen = [System.Windows.Forms.Screen]::FromHandle($mWin)
  [void][Win32]::MoveWindow($mWin, 300, 5, ($Screen.WorkingArea.Width - 300), ($Screen.WorkingArea.Height - 5), $true)
}

#==============================================================================================
# Fin Fonction
#==============================================================================================

# Demarrage du script en fonction de l'utilisateur et du mode debogage
if($env:USERNAME -notin $showEULAUsers -and -not $Debug) {
  if($env:USERNAME -eq 'USAGERS') { exit 0 }
  Start-Process -FilePath powershell.exe -ArgumentList "-ExecutionPolicy ByPass -File ""$idleScript""" -WindowStyle Hidden
  exit 0
}

if(-Not $Debug) {
  Start-Process -FilePath 'C:\Windows\System32\subst.exe' -ArgumentList "$UserProfileLetter $($Env:USERPROFILE)" -NoNewWindow -Wait
  $Signature = @'
    [DllImport("shell32.dll")]
    public extern static int SHSetKnownFolderPath(ref Guid folderId, uint flags, IntPtr token, [MarshalAs(UnmanagedType.LPWStr)] string path);
'@
  $Type = Add-Type -MemberDefinition $Signature -Name 'KnownFolders' -Namespace 'SHSetKnownFolderPath' -PassThru

  # Modification des chemins connus pour rediriger vers le lecteur virtuel
  $FGuig = [GUID]'FDD39AD0-238F-46AF-ADB4-6C85480369C7'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Desktop")

  $FGuig = [GUID]'B4BFCC3A-DB2C-424C-B029-7FE99A87C641'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Desktop")

  $FGuig = [GUID]'374DE290-123F-4565-9164-39C4925E467B'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Downloads")

  $FGuig = [GUID]'7D83EE9B-2244-4E70-B1F5-5393042AF1E4'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Downloads")
  
  # Images
  $FGuig = [GUID]'33E28130-4E1E-4676-835A-98395C3BC3BB'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Downloads")

  $FGuig = [GUID]'0DDD015D-B06C-45D5-8C4C-F59713854639'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Downloads")
  
  # Musique
  $FGuig = [GUID]'4BD8D571-6D19-48D3-BE97-422220080E43'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Downloads")
  
  # Videos
  $FGuig = [GUID]'18989B1D-99B5-455B-841C-AB7C74E4DDFC'
  $Type::SHSetKnownFolderPath([ref]$FGuig, 0, 0, "$UserProfileLetter\Downloads")

  do{ Start-Sleep -m 500 } until((Test-Path -Path $UserDataFolder))
}

# Preparation du dossier contenant la charte et les ressources associees
$CharteFolder = (Join-Path "$UserDataFolder" 'Charte')
[void][System.IO.Directory]::CreateDirectory("$CharteFolder")

Copy-Item -Path (Join-Path $MyDir 'index.html') -Destination (Join-Path $CharteFolder 'index.html') -Force
Copy-Item -Path (Join-Path $MyDir 'assets') -Destination (Join-Path $CharteFolder 'assets') -Recurse -Force

# Gestion de la charte specifique en fonction du code commune
[string]$CodeCommune = (Get-ItemProperty -Path $KeyPath -ErrorAction SilentlyContinue).CodeCommune
if([String]::IsNullOrEmpty($CodeCommune)) {
  Copy-Item -Path 'C:\ProgramData\BordeauxMetropole\Mona\Admin\EULA\Chartes\Model__CGU__ Defaut.html' -Destination (Join-Path $CharteFolder 'Charte.html') -Force
} else {
  [string]$CharteFile = Get-ChildItem -Path 'C:\ProgramData\BordeauxMetropole\Mona\Admin\EULA\Chartes' -File -Filter *.html | ?{ $_.Name -match "$CodeCommune\.html$" } | Select -First 1 -ExpandProperty FullName
  
  if([String]::IsNullOrEmpty($CharteFile)) {
    Copy-Item -Path 'C:\ProgramData\BordeauxMetropole\Mona\Admin\EULA\Chartes\Model__CGU__ Defaut.html' -Destination (Join-Path $CharteFolder 'Charte.html') -Force
  } else {
    Copy-Item -Path "$CharteFile" -Destination (Join-Path $CharteFolder 'Charte.html') -Force
  }
}

# Configuration de la source web pour l'affichage dans le navigateur integre
$WebSource = ('file:///{0}' -f (Join-Path $CharteFolder 'index.html'))

# Ajout des fonctions Win32 pour la gestion avancee des fenêtres et parametres systeme
$Global:Win32Functions = Add-Type -PassThru -Name "Win32Functions" -MemberDefinition "[DllImport(""kernel32.dll"", SetLastError = true)]
  public static extern bool SetDllDirectory(string lpPathName);
  [DllImport(""user32.dll"", SetLastError = true)]
  public static extern bool SetSysColors(int cElements, int [] lpaElements, int [] lpaRgbValues);
  [DllImport(""user32.dll"", SetLastError = true)]
  public static extern int SystemParametersInfo(int uiAction, int uiParam, string pvParam, int fWinIni);"

# Chargement des bibliotheques necessaires pour le navigateur WebView2  
$libs = @('System.Windows.Forms', 'PresentationFramework', 'System.Windows.Interop')
foreach($lib in $libs) { [void][System.Reflection.Assembly]::LoadWithPartialName($lib) }

[void]$Win32Functions::SetDllDirectory((Join-Path $MyDir '\libs'))
$libsFile = @((Join-Path $MyDir '\libs\Microsoft.Web.WebView2.Wpf.dll'), (Join-Path $MyDir '\libs\Microsoft.Web.WebView2.Core.dll'))
foreach($lib in $libsFile) { [void][reflection.assembly]::LoadFile($lib) }

# Ajout de types supplementaires pour la gestion des DPI et la barre des tâches
Add-Type -ReferencedAssemblies 'System', 'System.Runtime.InteropServices' -Language CSharp -TypeDefinition @"
  using System;
  using System.Runtime.InteropServices;
  
  public class Win32Helper {
    public const long WS_EX_TOOLWINDOW = 0x00000080L;
    public const long GWL_EXSTYLE = -20;
    
    [DllImport("user32.dll")]
    public static extern long SetWindowLongPtr(long hWnd, long nIndex, long dwNewLong);
    [DllImport("user32.dll")]
    public static extern int FindWindow(string className, string windowText);
    [DllImport("user32.dll")]
    public static extern int ShowWindow(int hwnd, int command);
    
    private Win32Helper() {
      // hide ctor
    }
  }
  
  public class DPI {
    [DllImport("user32.dll")]
    static extern IntPtr GetDC(IntPtr ptr);
    
    [DllImport("user32.dll", EntryPoint = "ReleaseDC")]
    static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);
    
    [DllImport("gdi32.dll")]
    static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

    public enum DeviceCap {
      VERTRES = 10,
      DESKTOPVERTRES = 117  
    }

    private DPI() {
      // hide ctor
    }

    public static float scaling() {
      IntPtr desktop = GetDC(IntPtr.Zero);
      int LogicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);
      int PhysicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);
      ReleaseDC(IntPtr.Zero, desktop);

      return (float)PhysicalScreenHeight / (float)LogicalScreenHeight;
    }
  }
  
  public class Taskbar {
    private const int SW_HIDE = 0;
    private const int SW_SHOW = 1;
    private static int Handle {
      get {
        return Win32Helper.FindWindow("Shell_TrayWnd", "");
      }
    }
    private Taskbar() {
      // hide ctor
    }
    public static void Show() {
      Win32Helper.ShowWindow(Handle, SW_SHOW);
    }
    public static void Hide() {
      Win32Helper.ShowWindow(Handle, SW_HIDE);
    }
  }
"@

#==============================================================================================
# XAML Code (customisation graphique)
#==============================================================================================
[xml]$XAML = @"
<Window
  xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
  xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
  xmlns:wv2="clr-namespace:Microsoft.Web.WebView2.Wpf;assembly=Microsoft.Web.WebView2.Wpf"
  Title="Validation des conditions d'acces au poste libre service USAGERS"
  WindowStartupLocation="CenterScreen"
  WindowState="Maximized"
  Topmost="True"
  WindowStyle="None"
  ResizeMode="NoResize"
  ShowInTaskbar="False"
  Background="White">
  <Window.Resources>
    <Style TargetType="Control" x:Key="commonStyle.Rounded">
      <Style.Resources>
        <Style TargetType="Border">
          <Setter Property="CornerRadius" Value="5"/>
        </Style>
      </Style.Resources>
    </Style>
    <Style BasedOn="{StaticResource commonStyle.Rounded}" TargetType="Button"/>
    <Style BasedOn="{StaticResource commonStyle.Rounded}" TargetType="CheckBox"/>
  </Window.Resources>
  <Grid>
    <Grid.RowDefinitions>
      <RowDefinition Height="*"/>
      <RowDefinition Height="130"/>
    </Grid.RowDefinitions>
    <wv2:WebView2 Name="webBrowser" Grid.Row="0" />
    <StackPanel Grid.Row="1" Name="cguPopupContent" Margin="0,15,0,15" Background="Transparent">
      <StackPanel VerticalAlignment="Stretch" HorizontalAlignment="Center" Orientation="Horizontal">
        <CheckBox Name="chkaccept" HorizontalAlignment="Center" VerticalAlignment="center" VerticalContentAlignment="center" Margin="0">
          <CheckBox.LayoutTransform>
            <ScaleTransform ScaleX="1.8" ScaleY="1.8" />
          </CheckBox.LayoutTransform>
          <Label Name="Lblchk" Content="J'ai lu et j'accepte les " Margin="0" Padding="0" Background="White" FontSize="11" FontWeight="Bold" FontFamily="Bahnschrift" />
        </CheckBox>
        <Label Name="Lblchk2" HorizontalAlignment="center" VerticalAlignment="center" VerticalContentAlignment="center" Margin="0" Padding="0" Background="White">
          <TextBlock TextDecorations="Underline" LineHeight="22" LineStackingStrategy="BlockLineHeight" FontSize="22" FontWeight="Bold" FontFamily="Bahnschrift">conditions generales d'utilisation</TextBlock>
        </Label>
      </StackPanel>
      <StackPanel VerticalAlignment="Stretch" HorizontalAlignment="Center" Orientation="Horizontal" Margin="0,15,0,0">
        <Button Name="btnValidation" Content="Continuer" FontSize="16" FontWeight="Bold" FontFamily="Bahnschrift" HorizontalAlignment="Center" VerticalAlignment="Top" Width="135" Height="30" Margin="40,0" IsEnabled="{Binding ElementName=chkaccept, Path=IsChecked}" />
        <Button Name="btnRefuser" Content="Refuser" FontSize="16" FontWeight="Bold" FontFamily="Bahnschrift" HorizontalAlignment="center" VerticalAlignment="Top" Width="135" Height="30" Margin="40,0" />
      </StackPanel>
    </StackPanel>
  </Grid>
</Window>
"@

$Global:CurrentDPI = [DPI]::Scaling()

# Lecture du XAML et preparation de l'interface
$reader=(New-Object System.Xml.XmlNodeReader $xaml) 
try{$Form=[Windows.Markup.XamlReader]::Load( $reader )}
catch{
  Write-Host "Unable to load Windows.Markup.XamlReader. Some possible causes for this problem include: .NET Framework is missing PowerShell must be launched with PowerShell -sta, invalid XAML code was encountered."
  exit
}

# ===========================================================================
# Definition des variables Forms
# ===========================================================================
$xaml.SelectNodes("//*[@Name]") | %{Set-Variable -Name ($_.Name) -Value $Form.FindName($_.Name)}

# ===========================================================================
# Ajout du code et des operations en fonction du chargement de l'interface
# ===========================================================================

$webBrowser.CreationProperties = New-Object 'Microsoft.Web.WebView2.Wpf.CoreWebView2CreationProperties'
$webBrowser.CreationProperties.UserDataFolder = (Join-Path "$UserDataFolder" 'WebData')
$webBrowser.Source = $WebSource

# Gestion du clic sur le bouton Valider
$btnValidation.Add_Click({
    Get-EventSubscriber -SourceIdentifier "ProcessStart" | Unregister-Event
  
    Start-EdgeSizedWindow
    $Flag_Action = $true
    $Form.Close()
    
    # Gestion des raccourcis et nettoyage post-validation
    if (-Not $Debug) {
      $QuickAccess = New-Object -ComObject shell.application
      $QuickAccessItems = $QuickAccess.Namespace("shell:::{679F85CB-0220-4080-B29B-5540CC05AAB6}").Items()
  
      # GUID du dossier "Téléchargements"
      $downloadsGUID = "{374DE290-123F-4565-9164-39C4925E467B}"
  
      # Obtenir le chemin du dossier "Téléchargements" via son GUID
      $downloadsFolder = $QuickAccess.Namespace("shell:::$downloadsGUID").Self
      $downloadsPath = $downloadsFolder.Path
  
      # Désépingler tous les dossiers sauf "Téléchargements"
      $QuickAccessItems | Where-Object {
          $_.Path -ne $downloadsPath
      } | ForEach-Object {
          $_.InvokeVerb("unpinfromhome")
          $_.InvokeVerb("removefromhome")
      }
    }
    ###
    
    Start-Process -FilePath powershell.exe -ArgumentList "-ExecutionPolicy ByPass -File ""$idleScript""" -WindowStyle Hidden
  })

# Interaction avec les liens et elements de l'interface pour les conditions d'utilisation
$Lblchk2.Add_PreviewMouseDown({
    $webBrowser.ExecuteScriptAsync('toggleCharte();')
})

# Gestion du clic sur le bouton Refuser
$btnRefuser.Add_Click({
  $Flag_Action = $true
  if(-Not $Debug) { Start-Process -FilePath 'C:\Windows\System32\logoff.exe' }
})

# Fermeture par sortie non prevue
$Form.Add_Closing({
  if ($Flag_Action -eq $False) {
    if(-Not $Debug) { Start-Process -FilePath 'C:\Windows\System32\logoff.exe' }
  }
})

# Initialisation du navigateur et configuration des parametres
$Global:FirstLoaded = $False
$webBrowser.Add_Loaded({
  $webBrowser.Add_NavigationCompleted({param($sender, $e)
    If ($FirstLoaded){ Return }
    $Global:FirstLoaded = $True
  })

  $webBrowser.Add_CoreWebView2InitializationCompleted({param($sender, $e)
    [Microsoft.Web.WebView2.Core.CoreWebView2Settings]$Settings = $webBrowser.CoreWebView2.Settings
    $Settings.AreDefaultContextMenusEnabled  = $false
    $Settings.AreDefaultScriptDialogsEnabled = $true
    $Settings.AreDevToolsEnabled             = $false
    $Settings.AreHostObjectsAllowed          = $true
    $Settings.IsBuiltInErrorPageEnabled      = $false
    $Settings.IsScriptEnabled                = $true
    $Settings.IsStatusBarEnabled             = $false
    $Settings.IsWebMessageEnabled            = $true
    $Settings.IsZoomControlEnabled           = $false
    $Settings.IsGeneralAutofillEnabled       = $false
    $Settings.IsPasswordAutosaveEnabled      = $false
  })
})

# Gestion de l'affichage et de la fermeture de l'interface
$Form.Add_Loaded({
  [TaskBar]::Hide()
  $This.Activate()
  $wndHelper = [System.Windows.Interop.WindowInteropHelper]::new($This)
  
  [Win32Helper]::SetWindowLongPtr($wndHelper.Handle, [Win32Helper]::GWL_EXSTYLE, [Win32Helper]::WS_EX_TOOLWINDOW)
  
  $global:EscapeDT = New-Object -TypeName System.Windows.Threading.DispatcherTimer
  $global:EscapeDT.Interval = [TimeSpan]::FromMilliseconds(80)
  $global:EscapeDT.Add_Tick({
    if([System.Windows.Input.Keyboard]::IsKeyDown('LeftCtrl') -or [System.Windows.Input.Keyboard]::IsKeyDown('RightCtrl')) { return }
    (New-Object -ComObject wscript.shell).SendKeys("{ESC}")
  })
  
  $global:EscapeDT.Start()
  
  $global:ResizeDT = New-Object -TypeName System.Windows.Threading.DispatcherTimer
  $global:ResizeDT.Interval = [TimeSpan]::FromMilliseconds(5000)
  $global:ResizeDT.Add_Tick({
    [TaskBar]::Hide()
    $wndHelper = [System.Windows.Interop.WindowInteropHelper]::new($Form)
    $Screen = [System.Windows.Forms.Screen]::FromHandle($wndHelper.Handle)

    if($Form.Height -ne ($Screen.Bounds.Height / $Global:CurrentDPI) -or $Form.Width -ne ($Screen.Bounds.Width / $Global:CurrentDPI)) {
      $Form.WindowState = 'Normal'
      $Form.WindowState = 'Maximized'
    }
  })
  
  $global:ResizeDT.Start()
  
  $action = {
    $ExcludeProcess = @(
        'msedgewebview2.exe',
        'explorer.exe',
        'RuntimeBroker.exe',
        'backgroundTaskHost.exe',
        'StartMenuExperienceHost.exe',
        'SearchApp.exe',
        'SettingSyncHost.exe',
        'SearchProtocolHost.exe',
        'Microsoft.Photos.exe',
        'taskhostw.exe',
        'SearchProtocolHost.exe',
        'HxTsr.exe'
    )
    
    Write-Host ($Event.SourceEventArgs.NewEvent.TargetInstance.Name)
    Write-Host ($Event.SourceEventArgs.NewEvent.TargetInstance.ProcessId)
    
    if ($Event.SourceEventArgs.NewEvent.TargetInstance.Name -in $ExcludeProcess) {
        Write-Host 'Exclude'
    } else {
        Write-Host 'Kill'
        $ppid = $Event.SourceEventArgs.NewEvent.TargetInstance.ProcessId
        Stop-Process -Id $ppid
    }
}

  $Query = "select * from __instanceCreationEvent within 1 where targetInstance isa 'win32_Process'"
  $KillEvent = Register-CimIndicationEvent -Query $query -Action $action -SourceIdentifier "ProcessStart"
})

$Form.Add_Closed({
  $global:EscapeDT.Stop()
  $global:EscapeDT = $null
  
  $global:ResizeDT.Stop()
  $global:ResizeDT = $null
  
  [TaskBar]::Show()
  $webBrowser.Dispose()
  $webBrowser = $null
})

# Affichage du formulaire
$Form.Showdialog() | Out-Null