MD C:\ProgramData\BMetro\EULA
START "" /WAIT "%~dp0MicrosoftEdgeWebview2Setup.exe" /silent /install
del "%~dp0MicrosoftEdgeWebview2Setup.exe" /F /Q
robocopy /e . "C:\ProgramData\BMetro\EULA"
PUSHD C:\ProgramData\BMetro\EULA
REG ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\WinLogon" /v "Userinit" /t REG_SZ /d "powershell.exe -executionpolicy bypass -windowstyle hidden -file C:\ProgramData\BMetro\EULA\Launch-EULA.ps1,C:\Windows\system32\userinit.exe," /reg:64 /F

SET "GROUP=Gestionnaire"
NET LOCALGROUP "%GROUP%" 1>NUL 2>&1 || NET LOCALGROUP "%GROUP%" /ADD
net localgroup "%GROUP%" | findstr /b "Configurer" 1>NUL 2>&1 || net localgroup "%GROUP%" Configurer /ADD

@echo 1.0>C:\ProgramData\BMetro\EULA\Ver1.0.txt
del C:\ProgramData\BMetro\EULA\Set-EULA.cmd /F /Q