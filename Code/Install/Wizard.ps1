﻿
param(

     $InstallDIR = 'C:\ProgramData\BMETRO\ORONNACEUR'

    ,$DataDIR = 'C:\ProgramData\BordeauxMetropole\Mona'

)
 

$InstallDIR = $InstallDIR -Replace '[\\"]+$',''
$DataDIR = $DataDIR -Replace '[\\"]+$',''

Set-ExecutionPolicy Bypass -Force

# Demarrez la transcription (assurez-vous que le chemin d'acces est accessible en ecriture)


Start-Transcript -Path "$InstallDIR\script_log.txt" -Append

Set-ExecutionPolicy Bypass -Force

# Verification et installation automatique du fournisseur NuGet si necessaire
# Définir le chemin vers vos modules hors ligne et fournisseurs de packages
$SavePath = "$InstallDIR\Gallery"

# Installer le fournisseur de packages NuGet
$nugetProviderSourcePath = Join-Path $SavePath "NuGet\2.8.5.208"
$nugetProviderDestinationPath = "C:\Program Files\PackageManagement\ProviderAssemblies\NuGet\2.8.5.208"

# Vérification détaillée des chemins NuGet
Write-Host "Vérification des chemins NuGet..."
Write-Host "Source Path: $nugetProviderSourcePath"
Write-Host "Destination Path: $nugetProviderDestinationPath"

# Vérifier si le chemin source existe
if (!(Test-Path -Path $nugetProviderSourcePath)) {
    Write-Error "Le dossier source NuGet n'existe pas: $nugetProviderSourcePath"
    Write-Host "Contenu du dossier Gallery:"
    Get-ChildItem -Path $SavePath | Format-Table Name, LastWriteTime, Length
    exit 1
}

# Afficher le contenu du dossier source NuGet
Write-Host "Contenu du dossier source NuGet"
Get-ChildItem -Path $nugetProviderSourcePath -Recurse | Format-Table FullName, Length

# S'assurer que le répertoire de destination existe
if (!(Test-Path -Path $nugetProviderDestinationPath)) {
    try {
        New-Item -Path $nugetProviderDestinationPath -ItemType Directory -Force -ErrorAction Stop
        Write-Host "Répertoire de destination créé avec succès"
    }
    catch {
        Write-Error "Impossible de créer le répertoire de destination: $_"
        exit 1
    }
}

# Copier les fichiers du fournisseur NuGet avec plus de détails
try {
    Copy-Item -Path "$nugetProviderSourcePath\*" -Destination $nugetProviderDestinationPath -Recurse -Force -Verbose -ErrorAction Stop
    Write-Host "Copie des fichiers NuGet effectuée"
}
catch {
    Write-Error "Erreur lors de la copie des fichiers NuGet: $_"
    exit 1
}

# Vérification plus détaillée après la copie
$nugetDll = Join-Path $nugetProviderDestinationPath "Microsoft.PackageManagement.NuGetProvider.dll"
if (Test-Path -Path $nugetDll) {
    Write-Host "Fournisseur de packages NuGet copié avec succès."
    Write-Host "Taille du fichier dll: $((Get-Item $nugetDll).Length) bytes"
} else {
    Write-Error "Échec de la copie du fournisseur de packages NuGet. Le fichier DLL n'existe pas."
    Write-Host "Contenu du dossier de destination"
    Get-ChildItem -Path $nugetProviderDestinationPath -Recurse | Format-Table FullName, Length
    exit 1
}

# Définir le chemin de destination pour les modules
$moduleDestPath = Join-Path $env:USERPROFILE "Documents\WindowsPowerShell\Modules"

# S'assurer que le chemin de destination des modules existe
if (!(Test-Path -Path $moduleDestPath)) {
    New-Item -Path $moduleDestPath -ItemType Directory -Force
}

# Fonction pour installer un module avec plus de détails
function Install-LocalModule {
    param(
        [string]$ModuleName,
        [string]$SourcePath,
        [string]$DestinationPath
    )
    
    Write-Host "`nInstallation du module ${ModuleName}"
    Write-Host "Source: ${SourcePath}"
    Write-Host "Destination: ${DestinationPath}"
    
    # Vérifier si le module source existe
    if (!(Test-Path -Path $SourcePath)) {
        Write-Error "Le chemin source pour ${ModuleName} n'existe pas: ${SourcePath}"
        return $false
    }
    
    # Afficher le contenu du dossier source
    Write-Host "Contenu du dossier source pour ${ModuleName}"
    Get-ChildItem -Path $SourcePath -Recurse | Format-Table FullName, Length
    
    # Créer le dossier de destination s'il n'existe pas
    if (!(Test-Path -Path $DestinationPath)) {
        New-Item -Path $DestinationPath -ItemType Directory -Force | Out-Null
    }
    
    # Copier les fichiers
    try {
        Copy-Item -Path "$SourcePath\*" -Destination $DestinationPath -Recurse -Force -Verbose
        Write-Host "Fichiers copiés avec succès pour ${ModuleName}"
        
        # Vérifier le contenu après la copie
        Write-Host "Contenu du dossier destination pour ${ModuleName}"
        Get-ChildItem -Path $DestinationPath -Recurse | Format-Table FullName, Length
        
        Import-Module $ModuleName -Force -Verbose -ErrorAction Stop
        Write-Host "Module ${ModuleName} installé avec succès."
        return $true
    }
    catch {
        Write-Error "Erreur lors de l'installation du module ${ModuleName} : $_"
        return $false
    }
}

# Installer les modules dans l'ordre
$modules = @(
    @{
        Name = "PackageManagement"
        SourcePath = Join-Path $SavePath "PackageManagement"
        DestPath = Join-Path $moduleDestPath "PackageManagement"
    },
    @{
        Name = "PowerShellGet"
        SourcePath = Join-Path $SavePath "PowerShellGet"
        DestPath = Join-Path $moduleDestPath "PowerShellGet"
    },
    @{
        Name = "LocalMDM"
        SourcePath = Join-Path $SavePath "LocalMDM"
        DestPath = Join-Path $moduleDestPath "LocalMDM"
    }
)

foreach ($module in $modules) {
    Install-LocalModule -ModuleName $module.Name -SourcePath $module.SourcePath -DestinationPath $module.DestPath
}


# Importer les bibliotheques necessaires


Add-Type -AssemblyName PresentationFramework, PresentationCore, WindowsBase

 
Import-Module Microsoft.PowerShell.Utility
Import-Module Microsoft.PowerShell.Management



# Definir le XAML pour l'interface utilisateur principal
$XAML = @"
<Window xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' 
        xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'
        Title='Assistant de configuration Mona' Height='500' Width='600'>
    <Grid Name='MainGrid'>
        <Grid.RowDefinitions>
            <RowDefinition Height='Auto'/>
            <RowDefinition Height='*'/>
            <RowDefinition Height='Auto'/>
            <RowDefinition Height='Auto'/>
        </Grid.RowDefinitions>
        <ProgressBar Name='pbStatusFinal' Grid.Row='0' Height='20' Margin='10' Minimum='0' Maximum='100' Value='0' Visibility='Visible'/>
        <ScrollViewer Grid.Row='1' VerticalScrollBarVisibility='Auto'>
            <StackPanel Name='ContentPanel' Margin='20,0,20,0'/>
        </ScrollViewer>
        <StackPanel Grid.Row='2' Orientation='Horizontal' HorizontalAlignment='Center' VerticalAlignment='Bottom' Margin='10'>
            <Button Name='btnPrécédent' Content='Précédent' Width='75' Margin='5'/>
            <Button Name='btnSuivant' Content='Suivant' Width='75' Margin='5'/>
        </StackPanel>
        <StackPanel Grid.Row='3' VerticalAlignment='Bottom' HorizontalAlignment='Center' Margin='10'>
            <TextBlock Name='continueInstruction' Foreground='Black' FontSize='14' TextAlignment='Center' Margin='0,0,0,10'>
                Pour poursuivre l'installation, cliquez sur Continuer.
            </TextBlock>
            <Button Name='btnContinuer' Content='Continuer' Width='150' Height='30' Margin='0,0,0,20'/>
        </StackPanel>
    </Grid>
</Window>
"@

# Charger le XAML dans une fenêtre PowerShell
[xml]$xamlReader = $XAML
$reader = (New-Object System.Xml.XmlNodeReader $xamlReader)
$Window = [Windows.Markup.XamlReader]::Load($reader)


# Obtenir les elements de la fenêtre
$btnPrécédent = $Window.FindName('btnPrécédent')
$btnSuivant = $Window.FindName('btnSuivant')
$MainGrid = $Window.FindName('MainGrid')

# Initialiser l'etape du wizard
$global:step = 1
$global:adminIndex = 0
$global:idleTimeout = 5  # Valeur par defaut de 5 minutes


# Definir l'action pour le bouton 'Suivant'
$btnSuivant.Add_Click({
    if ($global:step -eq 2 -and !($global:passwordSet)) {
        $global:errorText.Text = 'Veuillez définir le mot de passe avant de continuer.'
        $global:errorText.Foreground = 'Red'
    } elseif ($global:step -eq 3 -and !($global:configPasswordSet)) {
        $global:configErrorText.Text = 'Veuillez définir le mot de passe avant de continuer.'
        $global:configErrorText.Foreground = 'Red'
    } elseif ($global:step -eq 4) {
        # etape du délai d'inactivite
        if (SaveIdleTimeout) {
            $global:step++
            UpdateWizard
        }
    } else {
        if ($global:step -lt 5) {  
            $global:step++
            if ($global:step -eq 2) {
                # Code spécifique à l'étape 2
                . {
                    # Utiliser le SID du groupe Administrateurs pour prendre en charge différentes langues
                    $adminGroupSID = New-Object System.Security.Principal.SecurityIdentifier('S-1-5-32-544')
            
                    # Obtenir le nom du groupe en utilisant le SID
                    $adminGroup = Get-LocalGroup -SID $adminGroupSID.Value
                    $groupName = $adminGroup.Name
            
                    # Obtenir les membres du groupe Administrateurs
                    $admins = Get-LocalGroupMember -Group $groupName
            
                    if (-not $admins) {
                        Write-Host "Aucun utilisateur trouvé dans le groupe $groupName ou le groupe n'existe pas."
                        exit
                    }
                    $adminAccounts = @()
                    foreach ($admin in $admins) {
                        if ($admin.Name -like "$env:COMPUTERNAME\*" -and $admin.Name -ne "$env:COMPUTERNAME\Administrateur") {
                            $nameParts = $admin.Name -split '\\', 2
                            $name = $nameParts[1]
                            $user = Get-CimInstance -ClassName Win32_UserAccount -Filter "Name='$name' AND LocalAccount=True"
                            if ($user -and -not $user.Disabled) {
                                $adminAccounts += $name
                            }
                        }
                    }
                    for ($i = 0; $i -lt $adminAccounts.Length; $i++) {
                        Set-Variable -Name ("adminAccount" + ($i + 1)) -Value $adminAccounts[$i]
                    }
                    if ($adminAccounts.Length -gt 0) {
                        $global:adminAccounts = $adminAccounts
                        $global:firstAdminAccount = $adminAccounts[0]
                    } else {
                        $global:firstAdminAccount = 'Aucun compte administrateur trouvé'
                    }
                }
            }
            UpdateWizard
        }
    }
})



# Definir l'action pour le bouton 'Precedent'
$btnPrécédent.Add_Click({
    if ($global:step -gt 1) {
        $global:step--
        UpdateWizard
    }
})

$script:processLaunched = $false

# Declaration des variables globales pour les contrôles
$global:passwordBox1 = $null
$global:passwordBox2 = $null
$global:textBox1 = $null
$global:textBox2 = $null
$global:toggleButton = $null
$global:validateButton = $null
$global:errorText = $null
$global:isTextVisible = $false
$global:firstAdminAccount = 'XX'
$global:adminAccounts = @()
$global:adminIndex = 0
$global:passwordSet = $false

# Declaration des variables globales pour les contrôles Configurer
$global:configUserName = $null
$global:configPasswordBox1 = $null
$global:configPasswordBox2 = $null
$global:configTextBox1 = $null
$global:configTextBox2 = $null
$global:configToggleButton = $null
$global:configValidateButton = $null
$global:configErrorText = $null
$global:configPasswordSet = $false

# Initialiser le timer globalement
$global:timer = [System.Windows.Threading.DispatcherTimer]::new()
$global:timer.Interval = [System.TimeSpan]::FromSeconds(3)
$global:timer.Add_Tick({
    $global:errorText.Dispatcher.Invoke([action]{$global:errorText.Text = ''})
    $global:timer.Stop()
})

# Fonction pour demarrer le timer
function StartTimer {
    if ($global:timer.IsEnabled) {
        $global:timer.Stop()
    }
    $global:timer.Start()
}

# Fonction pour mettre a jour l'interface en fonction de l'etape
function UpdateWizard {
    switch ($global:step) {
        1 {
            $MainGrid.Children.Clear()

            # Créer une grille avec deux lignes : une pour le titre et une pour le contenu centre verticalement
            $grid = New-Object System.Windows.Controls.Grid
            $grid.RowDefinitions.Add((New-Object System.Windows.Controls.RowDefinition))
            $grid.RowDefinitions.Add((New-Object System.Windows.Controls.RowDefinition))

            # Ajouter le titre dans la première ligne de la grille
            $lblTitle = New-Object System.Windows.Controls.TextBlock
            $lblTitle.Text = "`nBienvenue dans l’Assistant de configuration de MONA. `n MONA est maintenant installé. Pour permettre son bon fonctionnement vous devez : "
            $lblTitle.FontSize = 15
            $lblTitle.TextAlignment = 'Center'
            $lblTitle.Margin = '0,10,0,10'  
            $lblTitle.TextWrapping = 'Wrap'
            $lblTitle.VerticalAlignment = 'Top'
            [System.Windows.Controls.Grid]::SetRow($lblTitle, 0)
            $grid.Children.Add($lblTitle)

            # Créer un StackPanel pour le contenu centre verticalement
            $stackPanel = New-Object System.Windows.Controls.StackPanel
            $stackPanel.Orientation = 'Vertical'
            $stackPanel.HorizontalAlignment = 'Center'
            $stackPanel.VerticalAlignment = 'Top'
            $stackPanel.Margin = '0,10,0,0'  
            $stackPanel.MaxHeight = 400  

            $lblMessage = New-Object System.Windows.Controls.TextBlock
            #$lblMessage.Text = "1. Définir les mots de passe du compte Configurer et du compte administrateur`n      (défini lors de l’installation de Windows).`n   `n   Pour information ces comptes permettent :`n   `n   - Compte d’administration : ajouter des logiciels ou du matériel,`n      modifier les CGU, modifier l’aspect graphique de la solution Mona.`n   - Compte Configurer : modifier la page web de démarrage`n      et ajouter des raccourcis ou documents pour les usagers.`n      `n2. Définir le délai d’inactivité avant la fermeture et la`n      suppression des données usagers."
            $lblMessage.Text = "1. Configurer les comptes suivants : `n    - Compte Administrateur : compte admin du poste permettant d'ajouter des`n      logiciels ou du matériel, de modifier les CGU et l'aspect graphique de la solution`n      MONA. L'objectif de cette nouvelle configuration du compte est de garantir la`n      sécurité du poste.`n    - Compte Configurer qui permet de modifier la page Web de démarrage et`n      et d'ajouter des raccourcis ou documents pour les usagers.`n      `n2. Définir le délai d’inactivité avant la fermeture et la suppression des données`n      usagers."
            $lblMessage.FontSize = 15
            $lblMessage.TextAlignment = 'Left'
            $lblMessage.Margin = '0,10,0,0'  
            $lblMessage.TextWrapping = 'Wrap'
            $stackPanel.Children.Add($lblMessage)

            [System.Windows.Controls.Grid]::SetRow($stackPanel, 1)
            $grid.Children.Add($stackPanel)

            $MainGrid.Children.Add($grid)
            $MainGrid.Children.Add($btnPrécédent.Parent)
            $btnPrécédent.IsEnabled = $false
            $btnSuivant.IsEnabled = $true
        }
        2 {
            ShowPasswordStep
            $btnPrécédent.IsEnabled = $true
            $btnSuivant.IsEnabled = $true
        }
        3 {
            ShowConfigStep
            $btnPrécédent.IsEnabled = $true
            $btnSuivant.IsEnabled = $true
        }
        4 {
            ShowIdleTimeoutStep
            $btnPrécédent.IsEnabled = $true
            $btnSuivant.IsEnabled = $true
        }
        5 {
            ShowFinalStep
            $btnPrécédent.IsEnabled = $true
            $btnSuivant.IsEnabled = $true
        }
    }
}

# Fonction pour afficher l'etape de mot de passe
function ShowPasswordStep {
    # XAML pour l'etape de mot de passe
    $passwordXaml = @"
    <Grid xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
          xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'>
        <Grid.RowDefinitions>
            <RowDefinition Height='Auto'/>
            <RowDefinition Height='*'/>
            <RowDefinition Height='Auto'/>
        </Grid.RowDefinitions>
        
        <StackPanel Grid.Row='1' VerticalAlignment='Center' HorizontalAlignment='Center'>
            <TextBlock Name='adminAccountText' Foreground='Black' FontSize='14' Margin='5' TextAlignment='Center'>
                Recherche du ou des comptes d’administration de l’ordinateur afin de redéfinir<LineBreak/>
                le mot de passe existant.
            </TextBlock>
            <TextBlock Name='adminAccountText2' Foreground='Black' FontSize='14' Margin='5' TextAlignment='Center'>
                Le compte : 
                <Run Name='adminAccountRun' Foreground='Green' FontWeight='Bold' Text='$global:firstAdminAccount'/>
            </TextBlock>
            
            <Grid Margin='0,10,0,0'>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width='*'/>
                    <ColumnDefinition Width='Auto'/>
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition Height='Auto'/>
                    <RowDefinition Height='Auto'/>
                </Grid.RowDefinitions>
                
                <Grid Grid.Column='0' Grid.Row='0'>
                    <PasswordBox Name='passwordBox1' Width='350' Height='30' Margin='5,5,0,5'/>
                    <TextBox Name='textBox1' Width='350' Height='30' Margin='5,5,0,5' Visibility='Collapsed'/>
                    <TextBlock Name='passwordHint1' Text='Nouveau mot de passe' Foreground='Gray' Opacity='0.5' IsHitTestVisible='False' VerticalAlignment='Center' HorizontalAlignment='Left' Margin='10,0,0,0'/>
                </Grid>
                
                <Grid Grid.Column='0' Grid.Row='1'>
                    <PasswordBox Name='passwordBox2' Width='350' Height='30' Margin='5,5,0,5'/>
                    <TextBox Name='textBox2' Width='350' Height='30' Margin='5,5,0,5' Visibility='Collapsed'/>
                    <TextBlock Name='passwordHint2' Text='Confirmer le mot de passe' Foreground='Gray' Opacity='0.5' IsHitTestVisible='False' VerticalAlignment='Center' HorizontalAlignment='Left' Margin='10,0,0,0'/>
                </Grid>
                
                <Button Name='toggleButton' Content='Afficher le Mot de Passe' Width='150' Height='30' Margin='5,5,5,5' Grid.Column='1' Grid.Row='0' Grid.RowSpan='2' VerticalAlignment='Center'/>
            </Grid>
            
            <Button Name='validateButton' Content='Définir' Width='150' Height='30' Margin='5,15,5,5'/>
            <TextBlock Name='errorText' Foreground='Red' Margin='5' TextAlignment='Center'/>
        </StackPanel>
    
        <TextBlock Name='completionMessage' Grid.Row='2' Foreground='Green' FontSize='14' TextAlignment='Center' Margin='0,130,0,0' Visibility='Collapsed'>
            Veuillez cliquer sur Suivant
        </TextBlock>
    </Grid>
"@

    # Charger le XAML
    $stringReader = New-Object System.IO.StringReader $passwordXaml
    $xmlReader = [System.Xml.XmlReader]::Create($stringReader)
    $passwordGrid = [Windows.Markup.XamlReader]::Load($xmlReader)

    # Attribuer les references globales
    $global:passwordBox1 = $passwordGrid.FindName('passwordBox1')
    $global:passwordBox2 = $passwordGrid.FindName('passwordBox2')
    $global:textBox1 = $passwordGrid.FindName('textBox1')
    $global:textBox2 = $passwordGrid.FindName('textBox2')
    $global:toggleButton = $passwordGrid.FindName('toggleButton')
    $global:validateButton = $passwordGrid.FindName('validateButton')
    $global:errorText = $passwordGrid.FindName('errorText')
    $global:adminAccountText = $passwordGrid.FindName('adminAccountText')
    $global:adminAccountText2 = $passwordGrid.FindName('adminAccountText2')
    $global:adminAccountRun = $passwordGrid.FindName('adminAccountRun')
    $global:passwordHint1 = $passwordGrid.FindName('passwordHint1')
    $global:passwordHint2 = $passwordGrid.FindName('passwordHint2')
    $global:completionMessage = $passwordGrid.FindName('completionMessage')

    # Definir l'evenement PasswordChanged pour passwordBox1
    $global:passwordBox1.Add_PasswordChanged({
        if ($global:passwordBox1.Password.Length -eq 0) {
            $global:passwordHint1.Opacity = 0.5
        } else {
            $global:passwordHint1.Opacity = 0
        }
        $global:errorText.Text = ''  # Reinitialiser le message d'erreur
    })
    
    # Definir l'evenement PasswordChanged pour passwordBox2
    $global:passwordBox2.Add_PasswordChanged({
        if ($global:passwordBox2.Password.Length -eq 0) {
            $global:passwordHint2.Opacity = 0.5
        } else {
            $global:passwordHint2.Opacity = 0
        }
        $global:errorText.Text = ''  # Reinitialiser le message d'erreur
    })

    # Definir l'evenement Click pour le bouton toggleButton
    $global:toggleButton.Add_Click({
        TogglePasswordVisibility
    })

    # Definir l'evenement Click pour le bouton validateButton
    $global:validateButton.Add_Click({
        ValidatePasswords
    })

    # Remplacer le contenu de la grille principale
    $MainGrid.Children.Clear()
    $MainGrid.Children.Add($passwordGrid)
    $MainGrid.Children.Add($btnPrécédent.Parent)

    # Mettre a jour le texte du compte administrateur
    $global:adminAccountRun.Text = $global:firstAdminAccount

    # Fonction pour basculer la visibilite du mot de passe
    function TogglePasswordVisibility {
        if ($global:isTextVisible) {
            # Masquer le Mot de Passe
            $global:passwordBox1.Password = $global:textBox1.Text
            $global:passwordBox2.Password = $global:textBox2.Text
            $global:passwordBox1.Visibility = [System.Windows.Visibility]::Visible
            $global:passwordBox2.Visibility = [System.Windows.Visibility]::Visible
            $global:textBox1.Visibility = [System.Windows.Visibility]::Collapsed
            $global:textBox2.Visibility = [System.Windows.Visibility]::Collapsed
            $global:passwordHint1.Visibility = [System.Windows.Visibility]::Visible
            $global:passwordHint2.Visibility = [System.Windows.Visibility]::Visible
            $global:toggleButton.Content = 'Afficher le Mot de Passe'
            $global:isTextVisible = $false
        } else {
            # Afficher le Mot de Passe
            $global:textBox1.Text = $global:passwordBox1.Password
            $global:textBox2.Text = $global:passwordBox2.Password
            $global:passwordBox1.Visibility = [System.Windows.Visibility]::Collapsed
            $global:passwordBox2.Visibility = [System.Windows.Visibility]::Collapsed
            $global:textBox1.Visibility = [System.Windows.Visibility]::Visible
            $global:textBox2.Visibility = [System.Windows.Visibility]::Visible
            $global:passwordHint1.Visibility = [System.Windows.Visibility]::Collapsed
            $global:passwordHint2.Visibility = [System.Windows.Visibility]::Collapsed
            $global:toggleButton.Content = 'Masquer le Mot de Passe'
            $global:isTextVisible = $true
        }
    }

    # Fonction pour valider les mots de passe
    function ValidatePasswords {
        $password1 = if ($global:isTextVisible) { $global:textBox1.Text } else { $global:passwordBox1.Password }
        $password2 = if ($global:isTextVisible) { $global:textBox2.Text } else { $global:passwordBox2.Password }
        
        if ($password1 -eq $password2 -and $password1.Length -ge 8) {
            $global:errorText.Text = 'Les mots de passe ont ete valides.'
            $global:errorText.Foreground = 'Green'
            $global:passwordSet = $true  # Marquer le mot de passe comme defini

            # Definir le mot de passe pour le compte administrateur actuel
            try {
                Set-LocalUser -Name $global:adminAccounts[$global:adminIndex] -Password (ConvertTo-SecureString $password1 -AsPlainText -Force)
                $global:errorText.Text = "Le mot de passe pour $($global:adminAccounts[$global:adminIndex]) à été défini avec succes."
                $global:errorText.Foreground = 'Green'
            } catch {
                $global:errorText.Text = "Erreur lors de la definition du mot de passe : $_"
                $global:errorText.Foreground = 'Red'
            }

            # Afficher le compte administrateur suivant et effacer les champs de mot de passe
            if ($global:adminIndex -lt $global:adminAccounts.Length - 1) {
                $global:adminIndex++
                $global:adminAccountRun.Text = $global:adminAccounts[$global:adminIndex]
                $global:passwordBox1.Password = ''
                $global:passwordBox2.Password = ''
                $global:textBox1.Text = ''
                $global:textBox2.Text = ''
                $global:passwordHint1.Opacity = 0.5
                $global:passwordHint2.Opacity = 0.5
            } else {
                $global:adminAccountText2.Text = 'Tous les comptes administrateurs ont ete traites, veuillez cliquer sur suivant'
                $global:adminAccountText2.Foreground = 'Green'
                $global:passwordBox1.Password = ''
                $global:passwordBox2.Password = ''
                $global:textBox1.Text = ''
                $global:textBox2.Text = ''
                $global:passwordHint1.Opacity = 0.5
                $global:passwordHint2.Opacity = 0.5
            }

            # Demarrer le timer
            StartTimer
        } else {
            $global:errorText.Text = 'Les mots de passe ne correspondent pas ou sont trop courts (minimum 8 caracteres).'
            $global:errorText.Foreground = 'Red'
        }
    }
}

# Fonction pour basculer la visibilite du mot de passe
function TogglePasswordVisibility {
    if ($global:isTextVisible) {
        # Masquer le Mot de Passe
        $global:passwordBox1.Password = $global:textBox1.Text
        $global:passwordBox2.Password = $global:textBox2.Text
        $global:passwordBox1.Visibility = 'Visible'
        $global:passwordBox2.Visibility = 'Visible'
        $global:textBox1.Visibility = 'Collapsed'
        $global:textBox2.Visibility = 'Collapsed'
        $global:passwordHint1.Visibility = 'Visible'
        $global:passwordHint2.Visibility = 'Visible'
        $global:toggleButton.Content = 'Afficher le Mot de Passe'
        $global:isTextVisible = $false
    } else {
        # Afficher le Mot de Passe
        $global:textBox1.Text = $global:passwordBox1.Password
        $global:textBox2.Text = $global:passwordBox2.Password
        $global:passwordBox1.Visibility = 'Collapsed'
        $global:passwordBox2.Visibility = 'Collapsed'
        $global:textBox1.Visibility = 'Visible'
        $global:textBox2.Visibility = 'Visible'
        $global:passwordHint1.Visibility = 'Collapsed'
        $global:passwordHint2.Visibility = 'Collapsed'
        $global:toggleButton.Content = 'Masquer le Mot de Passe'
        $global:isTextVisible = $true
    }
}

# Fonction pour Definir les mots de passe
function ValidatePasswords {
    $password1 = if ($global:isTextVisible) { $global:textBox1.Text } else { $global:passwordBox1.Password }
    $password2 = if ($global:isTextVisible) { $global:textBox2.Text } else { $global:passwordBox2.Password }
    
    if ($password1 -eq $password2 -and $password1.Length -ge 8) {
        $global:errorText.Text = 'Les mots de passe ont été validés.'
        $global:errorText.Foreground = 'Green'
        $global:passwordSet = $true

        try {
            Set-LocalUser -Name $global:adminAccounts[$global:adminIndex] -Password (ConvertTo-SecureString $password1 -AsPlainText -Force)
            $global:errorText.Text = "Le mot de passe pour $($global:adminAccounts[$global:adminIndex]) a été défini avec succès."
            $global:errorText.Foreground = 'Green'
        } catch {
            $global:errorText.Text = "Erreur lors de la definition du mot de passe : $_"
            $global:errorText.Foreground = 'Red'
            return
        }

        if ($global:adminIndex -lt $global:adminAccounts.Length - 1) {
            $global:adminIndex++
            $global:adminAccountRun.Text = $global:adminAccounts[$global:adminIndex]
            ClearPasswordFields
        } else {
            CompleteAdminAccountSetup
        }

        StartTimer
    } else {
        $global:errorText.Text = 'Les mots de passe ne correspondent pas ou sont trop courts (minimum 8 caracteres).'
        $global:errorText.Foreground = 'Red'
    }
}

function ClearPasswordFields {
    $global:passwordBox1.Password = ''
    $global:passwordBox2.Password = ''
    $global:textBox1.Text = ''
    $global:textBox2.Text = ''
    $global:passwordHint1.Opacity = 0.5
    $global:passwordHint2.Opacity = 0.5
}

function CompleteAdminAccountSetup {
    $global:completionMessage.Visibility = 'Visible'
    $global:passwordBox1.IsEnabled = $false
    $global:passwordBox2.IsEnabled = $false
    $global:textBox1.IsEnabled = $false
    $global:textBox2.IsEnabled = $false
    $global:toggleButton.IsEnabled = $false
    $global:validateButton.IsEnabled = $false

    $global:adminAccountText.Visibility = 'Visible'
    $global:adminAccountText2.Visibility = 'Visible'

    ClearPasswordFields
}

function StartTimer {
    if ($global:timer.IsEnabled) {
        $global:timer.Stop()
    }
    $global:timer.Start()
}

# Fonction pour afficher l'etape de creation du compte Configurer
function ShowConfigStep {
    # XAML pour l'etape de creation de compte Configurer
    $configXaml = @"
<Grid xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
      xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'>
    <Grid.RowDefinitions>
        <RowDefinition Height='*'/>
        <RowDefinition Height='Auto'/>
        <RowDefinition Height='Auto'/>
    </Grid.RowDefinitions>
    
    <StackPanel Grid.Row='1' VerticalAlignment='Center' HorizontalAlignment='Center' Margin='0,0,0,10'>
        <TextBlock Name='configAccountText' Foreground='Black' FontSize='14' Margin='5' TextAlignment='Center'>
            Création du compte <Run Name='configAccountRun' Foreground='Green' FontWeight='Bold' Text='Configurer'/>,
            veuillez saisir le mot de passe
        </TextBlock>
        
        <Grid Margin='0,10,0,0'>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width='*'/>
                <ColumnDefinition Width='Auto'/>
            </Grid.ColumnDefinitions>
            <Grid.RowDefinitions>
                <RowDefinition Height='Auto'/>
                <RowDefinition Height='Auto'/>
            </Grid.RowDefinitions>
            
            <Grid Grid.Column='0' Grid.Row='0'>
                <PasswordBox Name='configPasswordBox1' Width='350' Height='30' Margin='5,5,0,5'/>
                <TextBox Name='configTextBox1' Width='350' Height='30' Margin='5,5,0,5' Visibility='Collapsed'/>
                <TextBlock Name='configPasswordHint1' Text='Nouveau mot de passe' Foreground='Gray' Opacity='0.5' IsHitTestVisible='False' VerticalAlignment='Center' HorizontalAlignment='Left' Margin='10,0,0,0'/>
            </Grid>
            
            <Grid Grid.Column='0' Grid.Row='1'>
                <PasswordBox Name='configPasswordBox2' Width='350' Height='30' Margin='5,5,0,5'/>
                <TextBox Name='configTextBox2' Width='350' Height='30' Margin='5,5,0,5' Visibility='Collapsed'/>
                <TextBlock Name='configPasswordHint2' Text='Confirmer le mot de passe' Foreground='Gray' Opacity='0.5' IsHitTestVisible='False' VerticalAlignment='Center' HorizontalAlignment='Left' Margin='10,0,0,0'/>
            </Grid>
            
            <Button Name='configToggleButton' Content='Afficher le Mot de Passe' Width='150' Height='30' Margin='5,5,5,5' Grid.Column='1' Grid.Row='0' Grid.RowSpan='2' VerticalAlignment='Center'/>
        </Grid>
        
        <Button Name='configValidateButton' Content='Définir' Width='150' Height='30' Margin='5,15,5,5'/>
        <TextBlock Name='configErrorText' Foreground='Red' Margin='5' TextAlignment='Center'/>
    </StackPanel>
    <TextBlock Name='configCompletionMessage' Grid.Row='2' Foreground='Green' FontSize='14' TextAlignment='Center' Margin='0,130,0,0' Visibility='Collapsed'>
    Le compte Configurer a été créé avec succès, veuillez cliquer sur suivant
    </TextBlock>
</Grid>
"@

    # Charger le XAML
    $stringReader = New-Object System.IO.StringReader $configXaml
    $xmlReader = [System.Xml.XmlReader]::Create($stringReader)
    $configGrid = [Windows.Markup.XamlReader]::Load($xmlReader)

    # Attribuer les references globales pour les contrôles de l'etape Configurer
    $global:configPasswordBox1 = $configGrid.FindName('configPasswordBox1')
    $global:configPasswordBox2 = $configGrid.FindName('configPasswordBox2')
    $global:configTextBox1 = $configGrid.FindName('configTextBox1')
    $global:configTextBox2 = $configGrid.FindName('configTextBox2')
    $global:configToggleButton = $configGrid.FindName('configToggleButton')
    $global:configValidateButton = $configGrid.FindName('configValidateButton')
    $global:configErrorText = $configGrid.FindName('configErrorText')
    $global:configPasswordHint1 = $configGrid.FindName('configPasswordHint1')
    $global:configPasswordHint2 = $configGrid.FindName('configPasswordHint2')
    $global:configAccountText = $configGrid.FindName('configAccountText')
    $global:configCompletionMessage = $configGrid.FindName('configCompletionMessage')

    # Ajouter l'evenement de mot de passe change
    $global:configPasswordBox1.Add_PasswordChanged({
        if ($global:configPasswordBox1.Password.Length -eq 0) {
            $global:configPasswordHint1.Opacity = 0.5
        } else {
            $global:configPasswordHint1.Opacity = 0
        }
        $global:configErrorText.Text = ''  # Reinitialiser le message d'erreur
    })
    
    $global:configPasswordBox2.Add_PasswordChanged({
        if ($global:configPasswordBox2.Password.Length -eq 0) {
            $global:configPasswordHint2.Opacity = 0.5
        } else {
            $global:configPasswordHint2.Opacity = 0
        }
        $global:configErrorText.Text = ''  # Reinitialiser le message d'erreur
    })

    # Ajouter l'evenement de clic pour le bouton 'Afficher le Mot de Passe'
    $global:configToggleButton.Add_Click({
        ToggleConfigPasswordVisibility
    })

    # Ajouter l'evenement de clic pour le bouton 'Definir'
    $global:configValidateButton.Add_Click({
        ValidateConfigPasswords
    })

    # Remplacer le contenu de la grille principale par le contenu de l'etape de creation de compte Configurer
    $MainGrid.Children.Clear()
    $MainGrid.Children.Add($configGrid)
    $MainGrid.Children.Add($btnPrécédent.Parent)
}

# Fonction pour basculer la visibilite du mot de passe pour l'etape Configurer
function ToggleConfigPasswordVisibility {
    # Toujours synchroniser le contenu entre PasswordBox et TextBox
    $global:configTextBox1.Text = $global:configPasswordBox1.Password
    $global:configTextBox2.Text = $global:configPasswordBox2.Password

    if ($global:isTextVisible) {
        # Masquer le Mot de Passe
        $global:configPasswordBox1.Visibility = 'Visible'
        $global:configPasswordBox2.Visibility = 'Visible'
        $global:configTextBox1.Visibility = 'Collapsed'
        $global:configTextBox2.Visibility = 'Collapsed'
        $global:configPasswordHint1.Visibility = 'Visible'
        $global:configPasswordHint2.Visibility = 'Visible'
        $global:configToggleButton.Content = 'Afficher le Mot de Passe'
        $global:isTextVisible = $false
    } else {
        # Afficher le Mot de Passe
        $global:configPasswordBox1.Visibility = 'Collapsed'
        $global:configPasswordBox2.Visibility = 'Collapsed'
        $global:configTextBox1.Visibility = 'Visible'
        $global:configTextBox2.Visibility = 'Visible'
        $global:configPasswordHint1.Visibility = 'Collapsed'
        $global:configPasswordHint2.Visibility = 'Collapsed'
        $global:configToggleButton.Content = 'Masquer le Mot de Passe'
        $global:isTextVisible = $true
    }
}

# Fonction pour valider les mots de passe de l'etape Configurer
function ValidateConfigPasswords {
    $password1 = if ($global:isTextVisible) { $global:configTextBox1.Text } else { $global:configPasswordBox1.Password }
    $password2 = if ($global:isTextVisible) { $global:configTextBox2.Text } else { $global:configPasswordBox2.Password }
    
    if ($password1 -eq $password2 -and $password1.Length -ge 8) {
        $global:configErrorText.Text = 'Les mots de passe ont ete valides.'
        $global:configErrorText.Foreground = 'Green'
        $global:configPasswordSet = $true

        try {
            $userName = "Configurer"
            New-LocalUser -Name $userName -Password (ConvertTo-SecureString $password1 -AsPlainText -Force) -FullName 'Configurer' -Description 'Compte local pour configuration'
            Add-LocalGroupMember -Group 'Utilisateurs' -Member $userName
            
            CompleteConfigSetup
        } catch {
            $global:configErrorText.Text = "Erreur lors de la création du compte : $_"
            $global:configErrorText.Foreground = 'Red'
        }

        StartTimer
    } else {
        $global:configErrorText.Text = 'Les mots de passe ne correspondent pas ou sont trop courts (minimum 8 caracteres).'
        $global:configErrorText.Foreground = 'Red'
    }
}

function CompleteConfigSetup {
    $global:configCompletionMessage.Visibility = 'Visible'
    $global:configPasswordBox1.IsEnabled = $false
    $global:configPasswordBox2.IsEnabled = $false
    $global:configTextBox1.IsEnabled = $false
    $global:configTextBox2.IsEnabled = $false
    $global:configToggleButton.IsEnabled = $false
    $global:configValidateButton.IsEnabled = $false

    # Verifier si la propriete Foreground existe avant de la modifier
    if ($global:configAccountText -and $global:configAccountText.PSObject.Properties.Name -contains 'Foreground') {
        $global:configAccountText.Foreground = 'Gray'
    }

    ClearConfigPasswordFields
}

function ClearConfigPasswordFields {
    $global:configPasswordBox1.Password = ''
    $global:configPasswordBox2.Password = ''
    $global:configTextBox1.Text = ''
    $global:configTextBox2.Text = ''
    $global:configPasswordHint1.Opacity = 0.5
    $global:configPasswordHint2.Opacity = 0.5
}

# Fonction pour afficher l'etape de configuration du délai d'inactivite
function ShowIdleTimeoutStep {
    $idleTimeoutXaml = @"
<Grid xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' 
      xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'>
    <!-- Définition des lignes de la grille -->
    <Grid.RowDefinitions>
        <RowDefinition Height='Auto'/>  <!-- Ligne pour le texte principal -->
        <RowDefinition Height='Auto'/>  <!-- Ligne pour le texte d'avertissement -->
        <RowDefinition Height='Auto'/>  <!-- Ligne pour la zone de saisie -->
        <RowDefinition Height='Auto'/>  <!-- Ligne pour les messages d'erreur -->
    </Grid.RowDefinitions>

    <!-- Texte principal -->
    <TextBlock Grid.Row='0' Name='idleTimeoutText' Foreground='Black' FontSize='14' Margin='5,20,5,5' TextAlignment='Center'>
        Définir le délai d'inactivité (en Minutes) avant fermeture et suppression des données usagers :
    </TextBlock>

    <!-- Texte d'avertissement -->
    <TextBlock Grid.Row='1' Name='warningText' Foreground='Red' FontSize='12' Margin='5,5,5,15' TextAlignment='Center'>
        Attention, l'utilisateur perdra alors tout le travail effectué. Aucun retour arrière ne sera possible.
    </TextBlock>

    <!-- Zone de saisie pour le délai d'inactivité -->
    <TextBox Grid.Row='2' Name='idleTimeoutTextBox' Text='5' Width='150' Height='30' Margin='5' HorizontalAlignment='Center'/>

    <!-- Zone pour les messages d'erreur -->
    <TextBlock Grid.Row='3' Name='idleTimeoutErrorText' Foreground='Red' Margin='5' TextAlignment='Center'/>
</Grid>
"@

    # Charger le XAML
    $stringReader = New-Object System.IO.StringReader $idleTimeoutXaml
    $xmlReader = [System.Xml.XmlReader]::Create($stringReader)
    $idleTimeoutGrid = [Windows.Markup.XamlReader]::Load($xmlReader)

    # Récupérer les références aux contrôles
    $global:idleTimeoutTextBox = $idleTimeoutGrid.FindName('idleTimeoutTextBox')
    $global:idleTimeoutErrorText = $idleTimeoutGrid.FindName('idleTimeoutErrorText')

    # Mettre à jour l'interface principale
    $MainGrid.Children.Clear()
    $MainGrid.Children.Add($idleTimeoutGrid)
    $MainGrid.Children.Add($btnPrécédent.Parent)
}


# Fonction pour sauvegarder le temps d'inactivite dans le registre HKLM
function SaveIdleTimeout {
    $idleTimeout = if ($global:idleTimeoutTextBox.Text -eq '') { 5 } else { $global:idleTimeoutTextBox.Text }

    if ([int]::TryParse($idleTimeout, [ref]0) -and [int]$idleTimeout -gt 0) {
        try {
            $registryPath = "HKLM:\Software\MonaAssistant"
            if (-not (Test-Path $registryPath)) {
                New-Item -Path $registryPath -Force | Out-Null
            }
            Set-ItemProperty -Path $registryPath -Name "IdleTimeout" -Value $idleTimeout
            $global:idleTimeout = $idleTimeout
            $global:idleTimeoutErrorText.Text = "Le temps d'inactivité a été enregistré avec succès."
            $global:idleTimeoutErrorText.Foreground = 'Green'
            return $true
        } catch {
            $global:idleTimeoutErrorText.Text = "Erreur lors de l'enregistrement du temps d'inactivité : $_"
            $global:idleTimeoutErrorText.Foreground = 'Red'
            return $false
        }
    } else {
        $global:idleTimeoutErrorText.Text = "Veuillez entrer un nombre valide supérieur à zéro."
        $global:idleTimeoutErrorText.Foreground = 'Red'
        return $false
    }
}



function ShowFinalStep {
    # Définition du XAML pour l'interface utilisateur de l'étape finale
    $finalStepXaml = @"
    <Grid xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' 
          xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'>
        <Grid.RowDefinitions>
            <RowDefinition Height='*'/>
            <RowDefinition Height='Auto'/>
            <RowDefinition Height='Auto'/>
        </Grid.RowDefinitions>

        <!-- StackPanel pour le résumé -->
        <StackPanel Grid.Row='0'>
            <TextBlock Name='finalStepText' Foreground='Black' FontWeight='Bold' FontSize='14' TextAlignment='Center' Margin='0,0,0,20'>
                Résumé de la configuration de Mona :
            </TextBlock>
            <TextBlock Name='adminAccountsSummary' Foreground='Black' FontSize='14' TextAlignment='Center' Margin='0,0,0,10'>
                Le(s) compte(s) administrateur(s) : 
                <Run Name='adminAccountsRun' Foreground='Green' FontWeight='Bold' Text='[AdminAccounts]'/>
            </TextBlock>
            <TextBlock Name='configAccountSummary' Foreground='Black' FontSize='14' TextAlignment='Center' Margin='0,0,0,10'>
                Le compte <Run Foreground='Green' FontWeight='Bold' Text='Configurer'/> a été défini.
            </TextBlock>
            <TextBlock Name='idleTimeoutSummary' Foreground='Black' FontSize='14' TextAlignment='Center' Margin='0,0,0,20'>
                Le délai d'inactivité est de 
                <Run Name='idleTimeoutRun' Foreground='Green' FontWeight='Bold' Text='[IdleTimeout]'/>
            </TextBlock>
        </StackPanel>

        <!-- StackPanel pour la barre de progression -->
        <StackPanel Grid.Row="1" VerticalAlignment="Center" Margin="0,70,0,0">
            <ProgressBar Name='pbStatusFinal' Height='20' Maximum='100' Value='0' Width='200' Margin='0,0,0,10'/>
            <TextBlock Name='tbProgress' Text='0%' HorizontalAlignment='Center'/>
        </StackPanel>

        <!-- StackPanel pour le texte d'instruction et le bouton Continuer -->
        <StackPanel Grid.Row='2' VerticalAlignment='Center' HorizontalAlignment='Center' Margin='0,10,0,0'>
            <TextBlock Name='continueInstruction' Foreground='Black' FontSize='14' TextAlignment='Center' Margin='0,0,0,10'>
                Pour poursuivre l'installation, cliquez sur Continuer.
            </TextBlock>
            <Button Name='btnContinuer' Content='Continuer' Width='150' Height='30' Margin='0,0,0,20'/>
        </StackPanel>
    </Grid>
"@

    # Chargement du XAML
    $reader = (New-Object System.Xml.XmlNodeReader ([xml]$finalStepXaml))
    $finalStepGrid = [Windows.Markup.XamlReader]::Load($reader)

    # Récupération des références aux éléments de l'interface
    $script:pbStatusFinal = $finalStepGrid.FindName('pbStatusFinal')
    $script:tbProgress = $finalStepGrid.FindName('tbProgress')
    $adminAccountsSummary = $finalStepGrid.FindName('adminAccountsSummary')
    $configAccountSummary = $finalStepGrid.FindName('configAccountSummary')
    $idleTimeoutSummary = $finalStepGrid.FindName('idleTimeoutSummary')

    # Récupérer les références au nouveau btnContinuer et continueInstruction
    $script:btnContinuer = $finalStepGrid.FindName('btnContinuer')
    $script:continueInstruction = $finalStepGrid.FindName('continueInstruction')

    # Mise à jour du texte pour les comptes administrateurs
    $adminAccountsRun = $adminAccountsSummary.FindName('adminAccountsRun')
    if ($adminAccountsRun) {
        $adminAccountsRun.Text = $global:adminAccounts -join ", "
    }

    # Mise à jour du texte pour le délai d'inactivité
    $idleTimeoutRun = $idleTimeoutSummary.FindName('idleTimeoutRun')
    if ($idleTimeoutRun) {
        $idleTimeoutRun.Text = "$global:idleTimeout minutes."
    }

    # Définition de l'action du bouton Continuer
    $script:btnContinuer.Add_Click({
        param($sender, $args)

        if ($script:processLaunched) {
            return
        }

        $script:processLaunched = $true

        # Masquer le bouton Continuer
        $sender.Visibility = 'Hidden'

        # Masquer la phrase d'instruction
        $script:continueInstruction.Visibility = 'Hidden'

        # --- Code pour lancer les actions en arrière-plan ---

        # Création d'une table de hachage synchronisée pour partager des données entre les threads
        $syncHash = [hashtable]::Synchronized(@{})
        $syncHash.Window = $Window
        $syncHash.pbStatusFinal = $script:pbStatusFinal
        $syncHash.tbProgress = $script:tbProgress
        $syncHash.btnContinuer = $script:btnContinuer
        $syncHash.continueInstruction = $script:continueInstruction
        $syncHash.Jobs = @()

        # Création d'un état de session initial pour le runspace
        $InitialSessionState = [System.Management.Automation.Runspaces.InitialSessionState]::CreateDefault()
        $InitialSessionState.Variables.Add([System.Management.Automation.Runspaces.SessionStateVariableEntry]::new("syncHash", $syncHash, $null))
        $InitialSessionState.Variables.Add([System.Management.Automation.Runspaces.SessionStateVariableEntry]::new("InstallDIR", $InstallDIR, $null))
        $InitialSessionState.Variables.Add([System.Management.Automation.Runspaces.SessionStateVariableEntry]::new("datadir", $datadir, $null))

        # Configuration du pool de runspaces
        $RunspacePool = [runspacefactory]::CreateRunspacePool(1, 1, $InitialSessionState, $Host)
        $RunspacePool.ApartmentState = 'STA'
        $RunspacePool.ThreadOptions = 'ReuseThread'
        $RunspacePool.Open()

        # Définition des actions à exécuter
        $Action = {
            # Liste des actions à exécuter
            $actions = @(        
                @{ Name = "Vérification de la présence du dossier d'installation"; Action = { 
                    if (-not (Test-Path -Path "$InstallDIR")) { throw "Erreur : Le dossier d'installation [$InstallDIR] n'existe pas."}
                }},
                @{ Name = "Installation de WebView2"; Action = { 
                    Start-Process -FilePath "$InstallDIR\EULA\MicrosoftEdgeWebView2RuntimeInstallerX64.exe" -ArgumentList "/silent", "/install" -Wait
                }},
                @{ Name = "Configuration de WinLogon"; Action = { 
                    Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\WinLogon" -Name "Userinit" -Value "powershell.exe -executionpolicy bypass -windowstyle hidden -file `"$InstallDIR\EULA\Launch-EULA_2.ps1`",C:\Windows\system32\userinit.exe," -Type String -Force
                }},
                @{ Name = "Exécution de Keys.ps1"; Action = {
                    $process = Start-Process -FilePath "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -ArgumentList "-ExecutionPolicy Bypass -File `"$InstallDIR\SET_VALEURS_SCRIPTBASE\Keys.ps1`" -installdir `"$InstallDIR`" -datadir `"$datadir`"" -Wait -PassThru -WindowStyle Hidden
                    if ($process.ExitCode -ne 0) { throw "L'exécution de Keys.ps1 a échoué avec le code de sortie $($process.ExitCode)" }
                }},
                @{ Name = "Exécution de WMI_Script.ps1 (SCRIPT_WMI)"; Action = {
                    $process = Start-Process -FilePath "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -ArgumentList "-Noprofile -ExecutionPolicy Bypass -WindowStyle Hidden -File `"$InstallDIR\SCRIPT_WMI\WMI_Script.ps1`" -installdir `"$InstallDIR`"" -Wait -PassThru -WindowStyle Hidden
                    if ($process.ExitCode -ne 0) { throw "L'exécution de WMI_Script.ps1 (SCRIPT_WMI) a échoué avec le code de sortie $($process.ExitCode)" }
                }}
            )

            # Exécution des actions et mise à jour de la progression
            foreach ($i in 0..($actions.Count - 1)) {
                $action = $actions[$i]
                try {
                    & $action.Action
                    $progress = (($i + 1) / $actions.Count) * 100
                    $syncHash.Window.Dispatcher.Invoke([action]{ 
                        $syncHash.pbStatusFinal.Value = $progress
                        $syncHash.tbProgress.Text = "$([Math]::Round($progress))% - $($action.Name)"
                    })
                } catch {
                    $syncHash.Window.Dispatcher.Invoke([action]{ 
                        $syncHash.tbProgress.Text = "Erreur lors de $($action.Name): $_"
                    })
                    break
                }
            }

            # Mise à jour du message final
            $syncHash.Window.Dispatcher.Invoke([action]{ 
                $syncHash.tbProgress.Text = "100% - Fermeture dans 5 secondes..."
            })

            # Attente de 5 secondes
            Start-Sleep -Seconds 5

            # Fermeture de la fenêtre
            $syncHash.Window.Dispatcher.Invoke([action]{ 
                $syncHash.Window.Close()
           })
        }

        # Création et démarrage du job
        $job = [powershell]::Create().AddScript($Action)
        $job.RunspacePool = $RunspacePool
        $syncHash.Jobs += $job
        $jobHandle = $job.BeginInvoke()
    })

    # Ajout de la grille finale à l'interface principale
    $MainGrid.Children.Clear()
    $MainGrid.Children.Add($finalStepGrid)
}



# Initialiser la fenêtre
UpdateWizard
$null = $Window.ShowDialog()

Stop-Transcript

