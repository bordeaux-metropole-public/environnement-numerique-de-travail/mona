function Set-MyCustomAcl {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath)

    foreach ($aclEntry in $config.Configuration.Acl.AclEntry) {
        $path = $aclEntry.Path
        
        # Vérification de l'existence du chemin
        if (Test-Path -Path $path) {
            Write-Host "Traitement du chemin : $path" -ForegroundColor Green

            $acl = Get-Acl -Path $path

            if ($aclEntry.SetAccessRuleProtection -eq "True") {
                $acl.SetAccessRuleProtection($true,$true)
                Write-Host "Protection des règles d'accès configurée pour $path" -ForegroundColor Green
            }

            if ($aclEntry.RemoveAccessRules) {
                foreach ($rule in $aclEntry.RemoveAccessRules.Rule) {
                    $identity = $rule.IdentityReference
                    $rights = if ($rule.FileSystemRights) { [System.Security.AccessControl.FileSystemRights]$rule.FileSystemRights } else { 0 }
                    $inheritance = if ($rule.InheritanceFlags) { [System.Security.AccessControl.InheritanceFlags]$rule.InheritanceFlags } else { 0 } 
                    $propagation = if ($rule.PropagationFlags) { [System.Security.AccessControl.PropagationFlags]$rule.PropagationFlags } else { 0 }
                    $type = if ($rule.AccessControlType) { [System.Security.AccessControl.AccessControlType]$rule.AccessControlType } else { 0 }

                    $acesToRemove = $acl.Access | Where-Object {$_.IdentityReference -eq $identity -and $_.FileSystemRights -eq $rights -and $_.InheritanceFlags -eq $inheritance -and $_.PropagationFlags -eq $propagation -and $_.AccessControlType -eq $type}
                    
                    foreach ($ace in $acesToRemove) {
                        $acl.RemoveAccessRule($ace)
                    }

                    if ($acesToRemove.Count -gt 0) {
                        Write-Host "Des règles d'accès ont été supprimées pour $identity sur $path" -ForegroundColor Green
                    }
                }
            }

            Set-Acl -Path $path -AclObject $acl
            Write-Host "Les ACL ont été mises à jour pour le chemin : $path" -ForegroundColor Green
        } else {
            Write-Host "Le chemin spécifié n'existe pas : $path" -ForegroundColor Red
        }
    }
}

$configPath = "C:\Users\FABIEN11\OneDrive - SCNBDX\Documents\BORNE_ASSOS\ASSO\WKS\SET_VALEURS_SCRIPTBASE\ACL.xml"
Set-MyCustomAcl -configPath $configPath
