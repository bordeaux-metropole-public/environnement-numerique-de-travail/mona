﻿# Poste public - Point Accès OS Windows 10
# Script Deconnexion Portail Captif
# x.durieux, R.lestelle
$password = Get-Content C:\Windows\Scripts\password.txt | ConvertTo-SecureString -Key (Get-Content C:\Windows\Scripts\WEBLOGOFF.key)
$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($password))))
$ID = Get-Content C:\Windows\Scripts\ID.txt | ConvertTo-SecureString -Key (Get-Content C:\Windows\Scripts\WEBLOGOFF.key)
$ID = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((($ID))))

# Url de déconnexion pour déconnecter l'utilisateur précedant
$IPLast = Resolve-DNSName -Name $env:computername -type A | Select Name,IPAddress
$IPLast = $IPLast.IPAddress

$LogoffUri = 'https://wifi.bordeaux-metropole.fr/deleg/api_admin_deleg.php?deleg_id={0}&deleg_pwd={1}&action=kickuser&user_ip_address={2}' -f $id, $password, $ipLast
$Result = Invoke-WebRequest -Uri $LogoffUri -UseBasicParsing

# Restauration des clés de registre Autologon
$RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"

Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String
Set-ItemProperty $RegPath "DefaultUserName" -Value "PAOS" -type String
Set-ItemProperty $RegPath "DefaultPassword" -Value "" -type String
