﻿
param(
    $InstallDIR = 'C:\ProgramData\BMETRO\ORONNACEUR'
)


param (
    $DataDIR = 'C:\ProgramData\BordeauxMetropole\Mona'
    
)


# Start logging
Start-Transcript -Path "$InstallDIR\registre_log.txt"

$ScriptLocation = Split-path $script:MyInvocation.MyCommand.path
Write-Host  "$Scriptlocation"


############# Fonctions pour la gestion des Administrateurs\Utilisateurs ################
#########################################################################################

New-LocalGroup -Name "Gestionnaire"
New-LocalGroup -Name "Admins_Mona"

# lister les membres du groupe administraeurs
# Get-LocalGroupMember -Group "Administrateurs"

# Lister les membres du groupe Administrateurs en utilisant le SID pour prendre en charge différentes langues
$adminGroupSID = New-Object System.Security.Principal.SecurityIdentifier('S-1-5-32-544')
$adminGroup = Get-LocalGroup -SID $adminGroupSID.Value
$groupName = $adminGroup.Name

# Obtenir les membres du groupe Administrateurs
$adminMembers = Get-LocalGroupMember -Group $groupName

# Ajouter chaque membre du groupe Administrateurs au groupe "Admins_Mona"
foreach ($member in $adminMembers) {
    Add-LocalGroupMember -Group "Admins_Mona" -Member $member.Name
    Add-LocalGroupMember -Group "Gestionnaire" -Member $member.Name
}

# Vérifier les membres du groupe "Admins_Mona"
Get-LocalGroupMember -Group "Admins_Mona"
Get-LocalGroupMember -Group "Gestionnaire"

#ajouter chaque membres du groupe administrateurs dans le gropue gestionnaire
# Get-LocalGroupMember -Group "Administrateurs" | Add-LocalGroupMember -Group "Gestionnaire"


Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

# Constante pour le bit ADS_UF_ACCOUNTDISABLE
$ADS_UF_ACCOUNTDISABLE = 2

# Lister les comptes dans le groupe Administrateurs


# Fonction pour la creation des comptes "utilisateurs"

$ComputerName = $env:COMPUTERNAME
$NameUSAGERS = 'USAGERS'
$Description = ''
$fullNameUsagers = "Acceder au PC"
net user $NameUSAGERS /fullname:$fullnameUsagers /passwordreq:no /passwordchg:no /expires:never /add
Set-LocalUser -Name $NameUSAGERS -PasswordNeverExpires 1
# & net user USAGERS /profilepath:"C:\Users\Mandatory"

Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

function Create-UserAccounts {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content "$configPath")

    # Ajouter le compte "Configurer" au groupe "Gestionnaire"
    try {
        Add-LocalGroupMember -Group "Gestionnaire" -Member "Configurer"
        Write-Host "L'utilisateur Configurer a été ajouté au groupe Gestionnaire." -ForegroundColor Green
    } catch {
        Write-Host "Erreur lors de l'ajout de l'utilisateur Configurer au groupe Gestionnaire : $_" -ForegroundColor Red
    }

    # Traitement des autres comptes utilisateurs définis dans le XML
    foreach ($user in $config.Configuration.UserAccounts.UserAccount) {
        $name = $user.Name
        $fullName = $user.FullName
        $groups = $user.Groups.Group
        
        # Le reste du code pour créer les autres utilisateurs...
        # (Garder la logique existante pour les autres comptes)
    }
}

$configPath = "$InstallDIR\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Write-Host "$configPath"
Create-UserAccounts -configPath $configPath

Add-Type @"
        using System;
        using System.Text;
        using System.Runtime.InteropServices;
        public static class UserEnvCP {
          [DllImport("userenv.dll")]
          public static extern int CreateProfile([MarshalAs(UnmanagedType.LPWStr)] string pszUserSid,
            [MarshalAs(UnmanagedType.LPWStr)] string pszUserName,
            [Out][MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszProfilePath, uint cchProfilePath);
        }
"@

$UserName = "Configurer"
$localUser = New-Object System.Security.Principal.NTAccount($UserName)
$userSID = $localUser.Translate([System.Security.Principal.SecurityIdentifier])
$sb = New-Object System.Text.StringBuilder(260)
$pathLen = $sb.Capacity

[UserEnvCP]::CreateProfile($userSID.Value, $UserName, $sb, $pathLen) | Out-Null;
$profilePath = $sb.ToString()
if(-not (Test-Path (Join-Path $profilePath "NTUSER.DAT"))) {
    Copy-Item "C:\Users\Default\NTUSER.DAT" $profilePath
}

#Copy-Item -Recurse -path "C:\Users\Default" -Destination C:\ProgramData\BordeauxMetropole\DefaultProfile -force
robocopy "C:\Users\Default" "C:\ProgramData\BordeauxMetropole\DefaultProfile" /E /COPYALL /XJ


<#
### 3 - Copie profil obligatoire / Modification sur le bureau Mandatory pour le compte configurer

Copy-Item -Recurse -path "$ScriptLocation\Mandatory.v6" -Destination c:\users\ -force
#>
# mettre le groupe administrateurs en propriétaire du dossier default
$acl = Get-Acl -Path C:\Users\Default
$AdminGroup = New-Object System.Security.Principal.NTAccount('Administrateurs')
$acl.SetOwner($AdminGroup)
Set-Acl -Path C:\Users\Default -AclObject $acl

# ajout de l'autorisation modifier pour le compte Configurer sur le bureau du profil default
#listing des acl en place
$acl = Get-Acl C:\Users\Default\Desktop

#Parametres dans l'ordre : Le compte, FileSystemRights (Read, ReadAndExecute, Write, Modify, ListDirectory, FullControl etc), InheritanceFlags (ContainerInherit None ou ObjectInherit), PropagationFlags (InheritOnly, None ou NoPropagateInherit) , AccessControlType (Allow, Deny)
$rule = New-Object System.Security.AccessControl.FileSystemAccessRule("Configurer","Modify", "ContainerInherit, ObjectInherit", "None", "Allow")
$Acl.addAccessRule($rule) #ajout de la regle
$acl.SetAccessRuleProtection($true,$true) #desactiver l'heritage
$acl | Set-Acl #Appliquer les droits


############# Fonction pour la creation des repertoires ################
########################################################################

# Fonction Create-Directories
function Create-Directories {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath)

    foreach ($directory in $config.Configuration.Directories.Directory) {
        $path = $directory.Path
        $name = $directory.Name

        $fullPath = Join-Path -Path $path -ChildPath $name

        if (-not (Test-Path $fullPath)) {
            New-Item -Path $path -Name $name -ItemType Directory -Force
            Write-Host "Le repertoire $fullPath a ete cree." -ForegroundColor Green
        } else {
            Write-Host "Le repertoire $fullPath existe deja." -ForegroundColor Magenta
        }
    }
}

$configPath = "$InstallDIR\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Create-Directories -configPath $configPath

############# Fonction pour la copie des Fichiers/Repertoires  ################
###############################################################################

# Fonction Copy-Item
function Copy-Files {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath)

    foreach ($file in $config.Configuration.FilesToCopy.File) {
        $sourcePath = $ExecutionContext.InvokeCommand.ExpandString($file.SourcePath)
        $destinationPath = $file.DestinationPath
        $recurse = [System.Convert]::ToBoolean($file.Recurse)
            
        if (Test-Path $sourcePath) {
            Copy-Item -Path $sourcePath -Destination $destinationPath -Recurse:$recurse -Force
            Write-Host "Le fichier/repertoire $sourcePath a ete copie vers $destinationPath." -ForegroundColor Green
        } else {
            Write-Host "Le fichier/repertoire $sourcePath n'existe pas." -ForegroundColor Magenta
        }
    }
}

$configPath = "$InstallDIR\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Copy-Files -configPath $configPath

### Creation icône Se deconnecter sur le bureau

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\Users\Public\Desktop\Se deconnecter.lnk")
$Shortcut.TargetPath = "C:\Windows\System32\logoff.exe"
$Shortcut.IconLocation = "C:\windows\System32\SHELL32.dll,27"
$Shortcut.Save()

### Création d'un raccourci sur le bureau du compte Configurer pour l'ajout de documents sur le profil mandatory

# Création de l'objet WshShell
$WshShell = New-Object -comObject WScript.Shell

# Définition des chemins
$DefaultDesktopPath = "C:\Users\Default\Desktop"
$ProgramDataPath = "C:\ProgramData\BordeauxMetropole\Mona\configurer\HTML"
$PersonalizationFolderPath = "C:\Users\Configurer\Desktop\Personnalisation Mona"

# Création du répertoire "Personnalisation Mona" sur le bureau de C:\Users\Default
if (-not (Test-Path -Path $PersonalizationFolderPath)) {
    New-Item -ItemType Directory -Path $PersonalizationFolderPath
    Write-Output "Répertoire 'Personnalisation Mona' créé."
} else {
    Write-Output "Le répertoire 'Personnalisation Mona' existe déjà."
}

# Création du raccourci vers "C:\Users\Default\Desktop"
$Shortcut1Path = Join-Path -Path $PersonalizationFolderPath -ChildPath "Bureau AccesLibre.lnk"
$Shortcut1 = $WshShell.CreateShortcut($Shortcut1Path)
$Shortcut1.TargetPath = $DefaultDesktopPath
$Shortcut1.IconLocation = "C:\windows\System32\SHELL32.dll,266"
$Shortcut1.Save()
Write-Output "Raccourci 'Bureau AccesLibre' créé."

# Création du raccourci vers "C:\ProgramData\BordeauxMetropole\Mona\configurer\HTML"
$Shortcut2Path = Join-Path -Path $PersonalizationFolderPath -ChildPath "Personnalisation page d'accueil.lnk"
$Shortcut2 = $WshShell.CreateShortcut($Shortcut2Path)
$Shortcut2.TargetPath = $ProgramDataPath
$Shortcut2.IconLocation = "C:\windows\System32\SHELL32.dll,266"
$Shortcut2.Save()
Write-Output "Raccourci 'Personnalisation page d'accueil' créé."


### Création d'un raccourci sur le bureau des comptes Admins_Mona vers le dossier permettant le changement des CGU

# Obtenir les membres du groupe Admins_Mona
$groupName = "Admins_Mona"
$adminMembers = Get-LocalGroupMember -Group $groupName

# Chemin cible du raccourci
$targetPath = "C:\ProgramData\BordeauxMetropole\Mona\Admin\EULA\Chartes"

# Icône du raccourci (icône de dossier)
$iconLocation = "C:\Windows\System32\SHELL32.dll,3"

# Parcourir chaque membre du groupe Admins_Mona
foreach ($admin in $adminMembers) {
    # Extraire uniquement le nom d'utilisateur sans le domaine ou le nom de l'ordinateur
    $username = ($admin.Name -split '\\')[-1]

    # Obtenir le SID de l'utilisateur
    $userSID = $admin.SID

    # Trouver le profil utilisateur basé sur le SID
    $userProfile = (Get-CimInstance -ClassName Win32_UserProfile | Where-Object { $_.SID -eq $userSID }).LocalPath

    if ($userProfile) {
        $desktopPath = Join-Path $userProfile "Desktop"
        if (-Not (Test-Path $desktopPath)) {
            Write-Host "Le dossier Desktop n'existe pas pour $username. Raccourci non créé." -ForegroundColor Yellow
            continue
        }
        $shortcutPath = Join-Path $desktopPath "Accès aux CGU.lnk"

        $WshShell = New-Object -ComObject WScript.Shell
        $Shortcut = $WshShell.CreateShortcut($shortcutPath)
        $Shortcut.TargetPath = $targetPath
        $Shortcut.IconLocation = $iconLocation
        $Shortcut.Save()

        Write-Host "Raccourci créé pour $username" -ForegroundColor Green
    } else {
        Write-Host "Profil utilisateur non trouvé pour $username. Raccourci non créé." -ForegroundColor Yellow
        # Ne pas créer le raccourci sur le bureau public
    }
}


############# Gestion de l'alimentation ################
#########################################################################


# Activation du mode "Performances élevées"
Write-Host "Activation du mode 'Performances élevées'..."
powercfg /setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

# Désactivation de l'extinction de l'affichage (mise sur 'Jamais')
Write-Host "Configuration de l'extinction de l'affichage sur 'Jamais'..."
powercfg /setacvalueindex 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c SUB_VIDEO VIDEOIDLE 0

# Configuration des paramètres avancés de l'alimentation
Write-Host "Modification des délais de mise en veille et de gestion de l'alimentation..."

# Désactivation du délai de mise en veille prolongée sur secteur et batterie
powercfg /x -hibernate-timeout-ac 0
powercfg /x -hibernate-timeout-dc 0

# Désactivation de l'arrêt du disque dur sur secteur et batterie
powercfg /x -disk-timeout-ac 0
powercfg /x -disk-timeout-dc 0

# Désactivation de l'extinction de l'écran sur secteur et batterie
powercfg /x -monitor-timeout-ac 0
powercfg /x -monitor-timeout-dc 0

# Désactivation de la mise en veille sur secteur et batterie
powercfg /x -standby-timeout-ac 0
powercfg /x -standby-timeout-dc 0

Write-Host "Configuration de l'alimentation terminée."

# Définir l'action du bouton Marche/Arrêt sur "Arrêter"
Write-Host "Configuration de l'action du bouton Marche/Arrêt sur 'Arrêter'..."
powercfg /setacvalueindex SCHEME_CURRENT SUB_BUTTONS PBUTTONACTION 3
powercfg /setdcvalueindex SCHEME_CURRENT SUB_BUTTONS PBUTTONACTION 3

# Définir l'action du bouton de Mise en veille sur "Arrêter"
Write-Host "Configuration de l'action du bouton de Mise en veille sur 'Arrêter'..."
powercfg /setacvalueindex SCHEME_CURRENT SUB_BUTTONS SLEEPBUTTONACTION 3
powercfg /setdcvalueindex SCHEME_CURRENT SUB_BUTTONS SLEEPBUTTONACTION 3

# Appliquer les modifications en réactivant le plan d'alimentation actuel
Write-Host "Application des modifications..."
powercfg /setactive SCHEME_CURRENT

Write-Host "Configuration des actions des boutons d'alimentation terminée."

# Définir l'action de fermeture du capot sur "Ne rien faire" pour le mode secteur
Write-Host "Configuration de l'action de fermeture du capot sur 'Ne rien faire' en mode secteur..."
powercfg /setacvalueindex SCHEME_CURRENT SUB_BUTTONS LIDACTION 0

# Définir l'action de fermeture du capot sur "Ne rien faire" pour le mode batterie
Write-Host "Configuration de l'action de fermeture du capot sur 'Ne rien faire' en mode batterie..."
powercfg /setdcvalueindex SCHEME_CURRENT SUB_BUTTONS LIDACTION 0

# Appliquer les modifications en réactivant le plan d'alimentation actuel
Write-Host "Application des modifications..."
powercfg /setactive SCHEME_CURRENT

Write-Host "Configuration de l'action de fermeture du capot terminée."


############# Fonction pour l'execution des clefs de registres ################
###############################################################################


# Chemin vers le fichier XML de configuration
$configPath = "$InstallDIR\SET_VALEURS_SCRIPTBASE\Keys2.xml"


# Chargement du fichier XML
$config = [xml](Get-Content $configPath)

# Iteration sur chaque clef de registre configuree dans le fichier XML
foreach ($key in $config.Configuration.RegistryKeys.RegistryKey) {
    $keyPath = $key.KeyPath
    $valueName = $key.ValueName
    $data = $key.Data
    $type = $key.Type

    # Verification de l'existence de la clef de registre et creation si necessaire
    if (-not (Test-Path $keyPath)) {
        New-Item -Path $keyPath -Force
        Write-Host "La clef de registre $keyPath a ete creee." -ForegroundColor Green
    }

    # Definition de la valeur de la clef de registre
    Set-ItemProperty -Path $keyPath -Name $valueName -Value $data -Type $type
    Write-Host "La valeur $valueName a ete definie dans la clef de registre $keyPath." -ForegroundColor Green
}


###############     Exclusions du repertoire d'installation Mona      ###############
#####################################################################################       

# Chemin à exclure
$cheminExclusion = "C:\Program Files\BordeauxMetropole\Mona"

# Vérifier si le chemin est déjà dans la liste des exclusions
$exclusionsExistantes = Get-MpPreference | Select-Object -ExpandProperty ExclusionPath

if ($exclusionsExistantes -contains $cheminExclusion) {
    Write-Output "Le chemin '$cheminExclusion' est déjà exclu."
} else {
    # Ajout le chemin à la liste des exclusions
    Add-MpPreference -ExclusionPath $cheminExclusion
    Write-Output "Le chemin '$cheminExclusion' a été ajouté aux exclusions de Windows Defender."
}

##################          Création des Tâches planifiées         ######################
#########################################################################################


###### Reset Profil Usagers ######

# Chemin vers le script de reset des profils
$ScriptPath = "$InstallDIR\reset_profil_usagers\reset_profil_usagers.ps1"

# Nom de la tâche
$NomTache = "Reset Profil Usagers"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe" -Argument "-ExecutionPolicy Bypass -File `"$ScriptPath`""

# Déclencheur : à l'ouverture de session d'un utilisateur
$Trigger = New-ScheduledTaskTrigger -AtLogon -User "usagers"

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "reset_profil_usagers" -Settings $Settings


###### DISABLE AUTOLOGON ######

# Nom de la tâche
$NomTache = "Disable Autologon"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "reg" -Argument 'add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v ForceAutoLogon /d 0 /t REG_DWORD /f'

# Créer la tâche sans déclencheur
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -RunLevel Highest -User "SYSTEM" -Description "Desactive l'Autologon" -Settings $Settings


###### FORCE AUTOLOGON ######

# Nom de la tâche
$NomTache = "Force Autologon"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "reg" -Argument 'add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v ForceAutoLogon /d 1 /t REG_DWORD /f'

# Déclencheur : à l'ouverture de session d'un utilisateur
$Trigger = New-ScheduledTaskTrigger -AtLogon

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "Force l'Autologon" -Settings $Settings


###### IMPORTATION PROFIL WIFI ######

$NomTache = "Importer Profil WiFi a la deconnexion"

# Chemin vers le script d'importation Wi-Fi
$ScriptPath = "$InstallDIR\Wifi\script2.ps1"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe" -Argument "-ExecutionPolicy Bypass -File `"$ScriptPath`""

# Déclencheur : à la connexion de tous les utilisateurs
$Trigger = New-ScheduledTaskTrigger -AtLogOn
$Trigger.Enabled = $true

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "Importe le profil Wi-Fi" -Settings $Settings



##################     Apply Authenticated User Permissions to the Task      ##################
###############################################################################################

        

# Récupération des SIDs des groupes 'Administrateurs' et 'Gestionnaire'
$AdminGroup = New-Object System.Security.Principal.NTAccount('Administrateurs')
$AdminSID = $AdminGroup.Translate([System.Security.Principal.SecurityIdentifier])
$GestionnaireGroup = New-Object System.Security.Principal.NTAccount('Gestionnaire')
$GestionnaireSID = $GestionnaireGroup.Translate([System.Security.Principal.SecurityIdentifier])

$Scheduler = New-Object -ComObject "Schedule.Service"
$Scheduler.Connect()

$TaskPath = "\" # Le chemin de la tâche
$TaskName = "Disable Autologon" # Le nom de la tâche

# Récupération de la tâche
$GetTask = $Scheduler.GetFolder($TaskPath).GetTask($TaskName)

# Récupération du descripteur de sécurité actuel
$GetSecurityDescriptor = $GetTask.GetSecurityDescriptor(0xF)

# Vérification de la présence du SID du groupe 'Administrateurs' dans le descripteur de sécurité
if ($GetSecurityDescriptor -notmatch "($AdminSID)") {
    # Ajout de l'ACE avec les permissions pour le groupe 'Administrateurs' en utilisant son SID
    $Permission = "(A;;GRGX;;;$AdminSID)"
    $GetSecurityDescriptor += $Permission
}

# Vérification de la présence du SID du groupe 'Gestionnaire' dans le descripteur de sécurité
if ($GetSecurityDescriptor -notmatch "($GestionnaireSID)") {
    # Ajout de l'ACE avec les permissions pour le groupe 'Gestionnaire' en utilisant son SID
    $Permission = "(A;;GRGX;;;$GestionnaireSID)"
    $GetSecurityDescriptor += $Permission
}

# Application du nouveau descripteur de sécurité à la tâche
$GetTask.SetSecurityDescriptor($GetSecurityDescriptor, 0)


  

################## Fonction pour appliquer les ACL sur les repertoires ##################
#########################################################################################

### 8 - Mise en place du durcissement  et aux dossier profil autres que Telechargement, Désactivation de CMD, Regedit et TaskMGR

# Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'load hku\man C:\Users\Mandatory.v6\ntuser.man' -Wait
Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'load hku\def C:\Users\Default\ntuser.dat' -Wait
Write-Host "Chargement du profil Mandatory" -ForegroundColor Green

Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'import', """$InstallDIR\SET_VALEURS_SCRIPTBASE\Hardening.reg""" -Wait
Write-Host "Importation du fichier Hardening.reg" -ForegroundColor Green

Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'unload hku\def' -Wait
Write-Host "Déchargement du profil Mandatory" -ForegroundColor Green    

### 8 - ACL pour restreindre l'accès au C:\

function Set-MyCustomAcl {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath -Encoding UTF8)

    foreach ($aclEntry in $config.Configuration.Acl.AclEntry) {
        $path = $aclEntry.Path
        
        # Vérification de l'existence du chemin et protection contre la modification accidentelle de C:\ ou C:\Utilisateurs
        if ((Test-Path -Path $path) -and ($path -ne "C:\") -and ($path -ne "C:\Utilisateurs") -and ($path -ne "C:\Users")) {
            Write-Host "Traitement du chemin : $path" -ForegroundColor Green

            $acl = Get-Acl -Path $path

            if ($aclEntry.SetAccessRuleProtection -eq "True") {
                $acl.SetAccessRuleProtection($true, $false)
                Write-Host "Protection des règles d'accès configurée pour $path" -ForegroundColor Green
            }

            # Supprimer les règles spécifiées dans RemoveAccessRules
            if ($aclEntry.RemoveAccessRules) {
                foreach ($rule in $aclEntry.RemoveAccessRules.Rule) {
                    $identity = $rule.IdentityReference
                    $rights = [System.Security.AccessControl.FileSystemRights]$rule.FileSystemRights
                    $inheritance = [System.Security.AccessControl.InheritanceFlags]$rule.InheritanceFlags
                    $propagation = [System.Security.AccessControl.PropagationFlags]$rule.PropagationFlags
                    $type = [System.Security.AccessControl.AccessControlType]$rule.AccessControlType

                    $rulesToRemove = $acl.Access | Where-Object { 
                        $_.IdentityReference.Value -eq $identity -and 
                        $_.FileSystemRights -eq $rights -and 
                        $_.InheritanceFlags -eq $inheritance -and 
                        $_.PropagationFlags -eq $propagation -and 
                        $_.AccessControlType -eq $type 
                    }
                    
                    foreach ($ruleToRemove in $rulesToRemove) {
                        $acl.RemoveAccessRule($ruleToRemove)
                    }
                    Write-Host "Règle d'accès supprimée pour $identity sur $path" -ForegroundColor Yellow
                }
            }

            # Ajouter les nouvelles règles d'accès
            if ($aclEntry.AddAccessRules) {
                foreach ($rule in $aclEntry.AddAccessRules.Rule) {
                    $identity = $rule.IdentityReference
                    $rights = [System.Security.AccessControl.FileSystemRights]$rule.FileSystemRights
                    $inheritance = [System.Security.AccessControl.InheritanceFlags]$rule.InheritanceFlags
                    $propagation = [System.Security.AccessControl.PropagationFlags]$rule.PropagationFlags
                    $type = [System.Security.AccessControl.AccessControlType]$rule.AccessControlType

                    try {
                        if ($identity -eq "CREATEUR PROPRIETAIRE") {
                            $creatorOwnerSid = New-Object System.Security.Principal.SecurityIdentifier("S-1-3-0")
                            $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($creatorOwnerSid, $rights, $inheritance, $propagation, $type)
                        } else {
                            $ntAccount = New-Object System.Security.Principal.NTAccount($identity)
                            $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($ntAccount, $rights, $inheritance, $propagation, $type)
                        }
                        $acl.AddAccessRule($accessRule)
                        Write-Host "Nouvelle règle d'accès ajoutée pour $identity sur $path" -ForegroundColor Green
                    }
                    catch {
                        Write-Host "Erreur lors de l'ajout de la règle pour $identity : $_" -ForegroundColor Red
                    }
                }
            }

            # Appliquer les modifications
            try {
                Set-Acl -Path $path -AclObject $acl
                Write-Host "Les ACL ont été mises à jour pour le chemin : $path" -ForegroundColor Green
            }
            catch {
                Write-Host "Erreur lors de l'application des ACL pour $path : $_" -ForegroundColor Red
            }

            # Définir le propriétaire si spécifié dans le XML
            if ($aclEntry.Owner) {
                try {
                    $ownerIdentity = $aclEntry.Owner
                    $acl = Get-Acl -Path $path
                    $ownerSid = (New-Object System.Security.Principal.NTAccount($ownerIdentity)).Translate([System.Security.Principal.SecurityIdentifier])
                    $acl.SetOwner($ownerSid)
                    Set-Acl -Path $path -AclObject $acl
                    Write-Host "Propriétaire défini sur $ownerIdentity pour $path" -ForegroundColor Green
                }
                catch {
                    Write-Host "Erreur lors de la définition du propriétaire pour $path : $_" -ForegroundColor Red
                }
            }

            # Vérifier les ACLs après modification
            $finalAcl = Get-Acl -Path $path
            Write-Host "ACLs finales pour $path :" -ForegroundColor Cyan
            $finalAcl.Access | Format-Table IdentityReference, FileSystemRights, AccessControlType -AutoSize
        } else {
            Write-Host "Le chemin spécifié n'existe pas ou est protégé : $path" -ForegroundColor Red
        }
    }
}

#$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\ACL.xml"
$configPath = "$InstallDIR\SET_VALEURS_SCRIPTBASE\ACL.xml"
Set-MyCustomAcl -configPath $configPath


################ Application du WindowsLock aus users d'installation + Compte configurer' ####################
##############################################################################################################

# Variables
$UserProfiles = @(
    'C:\Users\Configurer',  # compte à configurer
    $env:USERPROFILE        # Compte depuis lequel vous exécutez le script 
)

foreach ($UserProfile in $UserProfiles) {
    if ($UserProfile -eq $env:USERPROFILE) {
        # Traitement du compte actuel
        Write-Host "Traitement du compte actuel : $UserProfile"

        # Chemin de la clé de registre à modifier dans HKCU
        $RegistryPathHKCU = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\System'

        # Créer la clé si elle n'existe pas
        if (-not (Test-Path $RegistryPathHKCU)) {
            New-Item -Path $RegistryPathHKCU -Force | Out-Null
        }

        # Définir la valeur DisableLockWorkstation à 1
        New-ItemProperty -Path $RegistryPathHKCU -Name 'DisableLockWorkstation' -Value 1 -PropertyType DWORD -Force | Out-Null

        Write-Host "La clé DisableLockWorkstation a été définie pour le compte actuel."
    } else {
        # Traitement des autres comptes utilisateurs
        # Chemin vers le fichier NTUSER.DAT du profil
        $NtUserDat = Join-Path -Path $UserProfile -ChildPath 'NTUSER.DAT'

        # Vérifier si le fichier NTUSER.DAT existe
        if (Test-Path $NtUserDat) {
            Write-Host "Traitement du profil : $UserProfile"

            # Nom temporaire pour la ruche
            $TempHiveName = 'TempHive'

            # Charger la ruche du profil utilisateur
            reg.exe load "HKU\$TempHiveName" "$NtUserDat" | Out-Null

            try {
                # Chemin de la clé de registre à modifier
                $RegistryPath = "Registry::HKU\$TempHiveName\Software\Microsoft\Windows\CurrentVersion\Policies\System"

                # Créer la clé si elle n'existe pas
                if (-not (Test-Path $RegistryPath)) {
                    New-Item -Path $RegistryPath -Force | Out-Null
                }

                # Définir la valeur DisableLockWorkstation à 1
                New-ItemProperty -Path $RegistryPath -Name 'DisableLockWorkstation' -Value 1 -PropertyType DWORD -Force | Out-Null

                Write-Host "La clé DisableLockWorkstation a été définie pour $UserProfile"
            } catch {
                Write-Error "Une erreur s'est produite lors de la modification du registre pour $UserProfile : $_"
            } finally {
                # Décharger la ruche du profil utilisateur
                reg.exe unload "HKU\$TempHiveName" | Out-Null
            }
        } else {
            Write-Warning "Le fichier NTUSER.DAT n'a pas été trouvé pour le profil : $UserProfile"
        }
    }
}

Write-Host "Opération terminée."


################## Fonction pour supprimer Mise a jour de Edge À l'ouverture de session' #####################
##############################################################################################################

# Nom complet de la tâche planifiée
$TaskFullName = "\MicrosoftEdgeUpdateTaskMachineCore"

# Définir le chemin temporaire pour le fichier XML
$xmlPath = "$env:TEMP\MicrosoftEdgeUpdateTaskMachineCore.xml"


# Vérifier si la tâche existe
$TaskExists = schtasks /Query /TN "$TaskFullName" /FO LIST /V 2>&1 | Out-String

if ($TaskExists -match "MicrosoftEdgeUpdateTaskMachineCore") {
    Write-Host "La tâche '$TaskFullName' existe."

    # Exporter les détails de la tâche pour modification
    schtasks /Query /TN "$TaskFullName" /XML > $xmlPath

    # Vérifier si le fichier XML a été créé avec succès
    if (Test-Path $xmlPath) {
        $xml = [xml](Get-Content $xmlPath)

        # Obtenir tous les déclencheurs
        $triggers = $xml.Task.Triggers.ChildNodes

        # Variable pour suivre si un déclencheur a été supprimé
        $triggerRemoved = $false

        # Parcourir tous les déclencheurs et supprimer ceux de type 'LogonTrigger'
        foreach ($trigger in @($triggers)) {
            if ($trigger.Name -eq "LogonTrigger") {
                $trigger.ParentNode.RemoveChild($trigger) | Out-Null
                $triggerRemoved = $true
            }
        }

        if ($triggerRemoved) {
            # Sauvegarder les modifications dans le fichier XML
            $xml.Save($xmlPath)

            # Supprimer et recréer la tâche avec les modifications
            schtasks /Delete /TN "$TaskFullName" /F
            schtasks /Create /TN "$TaskFullName" /XML $xmlPath

            Write-Host "Le déclencheur 'À l'ouverture de session' a été supprimé de la tâche '$TaskFullName'."
        } else {
            Write-Host "Aucun déclencheur 'À l'ouverture de session' n'a été trouvé dans la tâche '$TaskFullName'."
        }

        # Nettoyage
        Remove-Item $xmlPath
    } else {
        Write-Host "Erreur : Le fichier XML n'a pas pu être créé. Vérifiez les permissions et réessayez."
    }
} else {
    Write-Host "La tâche '$TaskFullName' n'existe pas."
}


Stop-Transcript

#$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\ACL.xml"
