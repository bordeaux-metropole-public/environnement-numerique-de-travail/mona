# Start logging
Start-Transcript -Path "C:\Program Files\BordeauxMetropole\registre_log.txt"

$ScriptLocation = Split-path $script:MyInvocation.MyCommand.path


############# Fonctions pour la gestion des Administrateurs\Utilisateurs ################
#########################################################################################

New-LocalGroup -Name "Gestionnaire"

# lister les membres du groupe administraeurs
Get-LocalGroupMember -Group "Administrateurs"

#ajouter chaque membres du groupe administrateurs dans le gropue gestionnaire
Get-LocalGroupMember -Group "Administrateurs" | Add-LocalGroupMember -Group "Gestionnaire"


Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

# Constante pour le bit ADS_UF_ACCOUNTDISABLE
$ADS_UF_ACCOUNTDISABLE = 2

# Lister les comptes dans le groupe Administrateurs


# Fonction pour la creation des comptes "utilisateurs"

$ComputerName = $env:COMPUTERNAME
$NameUSAGERS = 'USAGERS'
$Description = ''
$fullNameUsagers = "Acceder au PC"
net user $NameUSAGERS /fullname:$fullnameUsagers /passwordreq:no /passwordchg:no /expires:never /add
Set-LocalUser -Name $NameUSAGERS -PasswordNeverExpires 1
& net user USAGERS /profilepath:"C:\Users\Mandatory"

Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

function Create-UserAccounts {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath)

    # Ajouter le compte "Configurer" au groupe "Gestionnaire"
    try {
        Add-LocalGroupMember -Group "Gestionnaire" -Member "Configurer"
        Write-Host "L'utilisateur Configurer a été ajouté au groupe Gestionnaire." -ForegroundColor Green
    } catch {
        Write-Host "Erreur lors de l'ajout de l'utilisateur Configurer au groupe Gestionnaire : $_" -ForegroundColor Red
    }

    # Traitement des autres comptes utilisateurs définis dans le XML
    foreach ($user in $config.Configuration.UserAccounts.UserAccount) {
        $name = $user.Name
        $fullName = $user.FullName
        $groups = $user.Groups.Group
        
        # Le reste du code pour créer les autres utilisateurs...
        # (Garder la logique existante pour les autres comptes)
    }
}

$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Create-UserAccounts -configPath $configPath

$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Create-UserAccounts -configPath $configPath

### 3 - Copie profil obligatoire / Modification sur le bureau Mandatory pour le compte configurer

Copy-Item -Recurse -path "$ScriptLocation\Mandatory.v6" -Destination c:\users\ -force

# mettre le groupe administrateurs en propriétaire du dossier mandatory
$acl = Get-Acl -Path C:\Users\Mandatory.v6
$AdminGroup = New-Object System.Security.Principal.NTAccount('Administrateurs')
$acl.SetOwner($AdminGroup)
Set-Acl -Path C:\Users\Mandatory.v6 -AclObject $acl

# ajout de l'autorisation modifier pour le compte Configurer sur le bureau du profil mandatory
#listing des acl en place
$acl = Get-Acl C:\Users\Mandatory.v6\Desktop

#Parametres dans l'ordre : Le compte, FileSystemRights (Read, ReadAndExecute, Write, Modify, ListDirectory, FullControl etc), InheritanceFlags (ContainerInherit None ou ObjectInherit), PropagationFlags (InheritOnly, None ou NoPropagateInherit) , AccessControlType (Allow, Deny)
$rule = New-Object System.Security.AccessControl.FileSystemAccessRule("Configurer","Modify", "ContainerInherit, ObjectInherit", "None", "Allow")
$Acl.addAccessRule($rule) #ajout de la regle
$acl.SetAccessRuleProtection($true,$true) #desactiver l'heritage
$acl | Set-Acl #Appliquer les droits


############# Fonction pour la creation des repertoires ################
########################################################################

# Fonction Create-Directories
function Create-Directories {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath)

    foreach ($directory in $config.Configuration.Directories.Directory) {
        $path = $directory.Path
        $name = $directory.Name

        $fullPath = Join-Path -Path $path -ChildPath $name

        if (-not (Test-Path $fullPath)) {
            New-Item -Path $path -Name $name -ItemType Directory -Force
            Write-Host "Le repertoire $fullPath a ete cree." -ForegroundColor Green
        } else {
            Write-Host "Le repertoire $fullPath existe deja." -ForegroundColor Magenta
        }
    }
}

$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Create-Directories -configPath $configPath


############# Fonction pour la copie des Fichiers/Repertoires  ################
###############################################################################

# Fonction Copy-Item
function Copy-Files {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath)

    foreach ($file in $config.Configuration.FilesToCopy.File) {
        $sourcePath = $ExecutionContext.InvokeCommand.ExpandString($file.SourcePath)
        $destinationPath = $file.DestinationPath
        $recurse = [System.Convert]::ToBoolean($file.Recurse)

        if (Test-Path $sourcePath) {
            Copy-Item -Path $sourcePath -Destination $destinationPath -Recurse:$recurse -Force
            Write-Host "Le fichier/repertoire $sourcePath a ete copie vers $destinationPath." -ForegroundColor Green
        } else {
            Write-Host "Le fichier/repertoire $sourcePath n'existe pas." -ForegroundColor Magenta
        }
    }
}

$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\Keys2.xml"
Copy-Files -configPath $configPath



### Creation icône Se deconnecter sur le bureau

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\Users\Public\Desktop\Se deconnecter.lnk")
$Shortcut.TargetPath = "C:\Windows\System32\shutdown.exe"
$Shortcut.Arguments = "/L /F"
$Shortcut.IconLocation = "C:\windows\System32\SHELL32.dll,27"
$Shortcut.Save()

### Création d'un raccourci sur le bureau du compte Configurer pour l'ajout de documents sur le profil mandatory

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("C:\Users\Default\Desktop\Bureau AccesLibre.lnk")
$Shortcut.TargetPath = "C:\Users\Mandatory.v6\Desktop"
$Shortcut.IconLocation = "C:\windows\System32\SHELL32.dll,266"
$Shortcut.Save()

############# Fonction pour l'execution des clefs de registres ################
###############################################################################


# Chemin vers le fichier XML de configuration
$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\Keys2.xml"

# Chargement du fichier XML
$config = [xml](Get-Content $configPath)

# Iteration sur chaque clef de registre configuree dans le fichier XML
foreach ($key in $config.Configuration.RegistryKeys.RegistryKey) {
    $keyPath = $key.KeyPath
    $valueName = $key.ValueName
    $data = $key.Data
    $type = $key.Type

    # Verification de l'existence de la clef de registre et creation si necessaire
    if (-not (Test-Path $keyPath)) {
        New-Item -Path $keyPath -Force
        Write-Host "La clef de registre $keyPath a ete creee." -ForegroundColor Green
    }

    # Definition de la valeur de la clef de registre
    Set-ItemProperty -Path $keyPath -Name $valueName -Value $data -Type $type
    Write-Host "La valeur $valueName a ete definie dans la clef de registre $keyPath." -ForegroundColor Green
}

##################          Création des Tâches planifiées         ######################
#########################################################################################

###### DISABLE AUTOLOGON ######

# Nom de la tâche
$NomTache = "Disable Autologon"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "reg" -Argument 'add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v ForceAutoLogon /d 0 /t REG_DWORD /f'

# Créer la tâche sans déclencheur
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -RunLevel Highest -User "SYSTEM" -Description "Desactive l'Autologon" -Settings $Settings




###### FORCE AUTOLOGON ######

# Nom de la tâche
$NomTache = "Force Autologon"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "reg" -Argument 'add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /v ForceAutoLogon /d 1 /t REG_DWORD /f'

# Déclencheur : à l'ouverture de session d'un utilisateur
$Trigger = New-ScheduledTaskTrigger -AtLogon

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "Force l'Autologon" -Settings $Settings



###### EXPORTATION PROFIL WIFI ######

$NomTache = "Exporter Profil WiFi a la deconnexion"

# Chemin vers le script d'exportation Wi-Fi
$ScriptPath = """C:\Program Files\BordeauxMetropole\Mona\Wifi\script1.ps1"""

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe" -Argument "-ExecutionPolicy Bypass -File `"$ScriptPath`""

# Déclencheur : à la déconnexion (événement système)
$CIMTriggerClass = Get-CimClass -ClassName MSFT_TaskEventTrigger -Namespace Root/Microsoft/Windows/TaskScheduler:MSFT_TaskEventTrigger
$Trigger = New-CimInstance -CimClass $CIMTriggerClass -ClientOnly
$Trigger.Subscription = '<QueryList><Query Id="0" Path="System"><Select Path="System">*[System[Provider[@Name="Microsoft-Windows-Winlogon"] and EventID=7002]]</Select></Query></QueryList>'
$Trigger.Enabled = $true

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "Exporte le profil Wi-Fi actuel a la deconnexion" -Settings $Settings


###### IMPORTATION PROFIL WIFI ######


$NomTache = "Importer Profil WiFi a la deconnexion"

# Chemin vers le script d'importation Wi-Fi
$ScriptPath = "C:\Program Files\BordeauxMetropole\Mona\Wifi\script2.ps1"

# Action à effectuer
$Action = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe" -Argument "-ExecutionPolicy Bypass -File `"$ScriptPath`""

# Déclencheur : à la connexion de tous les utilisateurs
$Trigger = New-ScheduledTaskTrigger -AtLogOn
$Trigger.Enabled = $true

# Créer la tâche avec le déclencheur spécifié
$Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries
Register-ScheduledTask -TaskName $NomTache -Action $Action -Trigger $Trigger -RunLevel Highest -User "SYSTEM" -Description "Importe le profil Wi-Fi" -Settings $Settings



##################     Apply Authenticated User Permissions to the Task      ##################
###############################################################################################

        

# Récupération des SIDs des groupes 'Administrateurs' et 'Gestionnaire'
$AdminGroup = New-Object System.Security.Principal.NTAccount('Administrateurs')
$AdminSID = $AdminGroup.Translate([System.Security.Principal.SecurityIdentifier])
$GestionnaireGroup = New-Object System.Security.Principal.NTAccount('Gestionnaire')
$GestionnaireSID = $GestionnaireGroup.Translate([System.Security.Principal.SecurityIdentifier])

$Scheduler = New-Object -ComObject "Schedule.Service"
$Scheduler.Connect()

$TaskPath = "\" # Le chemin de la tâche
$TaskName = "Disable Autologon" # Le nom de la tâche

# Récupération de la tâche
$GetTask = $Scheduler.GetFolder($TaskPath).GetTask($TaskName)

# Récupération du descripteur de sécurité actuel
$GetSecurityDescriptor = $GetTask.GetSecurityDescriptor(0xF)

# Vérification de la présence du SID du groupe 'Administrateurs' dans le descripteur de sécurité
if ($GetSecurityDescriptor -notmatch "($AdminSID)") {
    # Ajout de l'ACE avec les permissions pour le groupe 'Administrateurs' en utilisant son SID
    $Permission = "(A;;GRGX;;;$AdminSID)"
    $GetSecurityDescriptor += $Permission
}

# Vérification de la présence du SID du groupe 'Gestionnaire' dans le descripteur de sécurité
if ($GetSecurityDescriptor -notmatch "($GestionnaireSID)") {
    # Ajout de l'ACE avec les permissions pour le groupe 'Gestionnaire' en utilisant son SID
    $Permission = "(A;;GRGX;;;$GestionnaireSID)"
    $GetSecurityDescriptor += $Permission
}

# Application du nouveau descripteur de sécurité à la tâche
$GetTask.SetSecurityDescriptor($GetSecurityDescriptor, 0)


  

################## Fonction pour appliquer les ACL sur les repertoires ##################
#########################################################################################

### 8 - Mise en place du durcissement  et aux dossier profil autres que Telechargement, Désactivation de CMD, Regedit et TaskMGR

Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'load hku\man C:\Users\Mandatory.v6\ntuser.man' -Wait
Write-Host "Chargement du profil Mandatory" -ForegroundColor Green


Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'import', "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\Hardening.reg" -Wait
Write-Host "Importation du fichier Hardening.reg" -ForegroundColor Green


Start-Process -FilePath 'C:\Windows\System32\reg.exe' -ArgumentList 'unload hku\man' -Wait
Write-Host "Déchargement du profil Mandatory" -ForegroundColor Green    

### 8 - ACL pour restreindre l'accès au C:\

function Set-MyCustomAcl {
    param (
        [string]$configPath
    )

    # Chargement du fichier XML
    $config = [xml](Get-Content $configPath -Encoding UTF8)

    foreach ($aclEntry in $config.Configuration.Acl.AclEntry) {
        $path = $aclEntry.Path
        
        # Vérification de l'existence du chemin et protection contre la modification accidentelle de C:\ ou C:\Utilisateurs
        if ((Test-Path -Path $path) -and ($path -ne "C:\") -and ($path -ne "C:\Utilisateurs") -and ($path -ne "C:\Users")) {
            Write-Host "Traitement du chemin : $path" -ForegroundColor Green

            $acl = Get-Acl -Path $path

            if ($aclEntry.SetAccessRuleProtection -eq "True") {
                $acl.SetAccessRuleProtection($true, $false)
                Write-Host "Protection des règles d'accès configurée pour $path" -ForegroundColor Green
            }

            # Supprimer les règles spécifiées dans RemoveAccessRules
            if ($aclEntry.RemoveAccessRules) {
                foreach ($rule in $aclEntry.RemoveAccessRules.Rule) {
                    $identity = $rule.IdentityReference
                    $rights = [System.Security.AccessControl.FileSystemRights]$rule.FileSystemRights
                    $inheritance = [System.Security.AccessControl.InheritanceFlags]$rule.InheritanceFlags
                    $propagation = [System.Security.AccessControl.PropagationFlags]$rule.PropagationFlags
                    $type = [System.Security.AccessControl.AccessControlType]$rule.AccessControlType

                    $rulesToRemove = $acl.Access | Where-Object { 
                        $_.IdentityReference.Value -eq $identity -and 
                        $_.FileSystemRights -eq $rights -and 
                        $_.InheritanceFlags -eq $inheritance -and 
                        $_.PropagationFlags -eq $propagation -and 
                        $_.AccessControlType -eq $type 
                    }
                    
                    foreach ($ruleToRemove in $rulesToRemove) {
                        $acl.RemoveAccessRule($ruleToRemove)
                    }
                    Write-Host "Règle d'accès supprimée pour $identity sur $path" -ForegroundColor Yellow
                }
            }

            # Ajouter les nouvelles règles d'accès
            if ($aclEntry.AddAccessRules) {
                foreach ($rule in $aclEntry.AddAccessRules.Rule) {
                    $identity = $rule.IdentityReference
                    $rights = [System.Security.AccessControl.FileSystemRights]$rule.FileSystemRights
                    $inheritance = [System.Security.AccessControl.InheritanceFlags]$rule.InheritanceFlags
                    $propagation = [System.Security.AccessControl.PropagationFlags]$rule.PropagationFlags
                    $type = [System.Security.AccessControl.AccessControlType]$rule.AccessControlType

                    try {
                        if ($identity -eq "CREATEUR PROPRIETAIRE") {
                            $creatorOwnerSid = New-Object System.Security.Principal.SecurityIdentifier("S-1-3-0")
                            $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($creatorOwnerSid, $rights, $inheritance, $propagation, $type)
                        } else {
                            $ntAccount = New-Object System.Security.Principal.NTAccount($identity)
                            $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($ntAccount, $rights, $inheritance, $propagation, $type)
                        }
                        $acl.AddAccessRule($accessRule)
                        Write-Host "Nouvelle règle d'accès ajoutée pour $identity sur $path" -ForegroundColor Green
                    }
                    catch {
                        Write-Host "Erreur lors de l'ajout de la règle pour $identity : $_" -ForegroundColor Red
                    }
                }
            }

            # Appliquer les modifications
            try {
                Set-Acl -Path $path -AclObject $acl
                Write-Host "Les ACL ont été mises à jour pour le chemin : $path" -ForegroundColor Green
            }
            catch {
                Write-Host "Erreur lors de l'application des ACL pour $path : $_" -ForegroundColor Red
            }

            # Vérifier les ACLs après modification
            $finalAcl = Get-Acl -Path $path
            Write-Host "ACLs finales pour $path :" -ForegroundColor Cyan
            $finalAcl.Access | Format-Table IdentityReference, FileSystemRights, AccessControlType -AutoSize
        } else {
            Write-Host "Le chemin spécifié n'existe pas ou est protégé : $path" -ForegroundColor Red
        }
    }
}

$configPath = "C:\Program Files\BordeauxMetropole\Mona\SET_VALEURS_SCRIPTBASE\ACL.xml"
Set-MyCustomAcl -configPath $configPath




Stop-Transcript
