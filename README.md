# Project Mona

## Description
The Mona project is the successor to the Solibuntu solution developed as part of the Solisol project https://solisol.org/.
This device aims to offer a simple-to-implement solution that can transform a classic Windows installation into an associative terminal for users. If the initial objective is E-Inclusion (digital inclusion) also called “fight against the digital divide”, Mona can adapt to many contexts.
This solution is based on observations which guided the technical choices made in the Mona project:
- Fighting the digital divide
- Propose to associations a solution that is simple to implement
- Respect data confidentiality for users

This device is turnkey, it is simple to install on a Windows installation.
- This system does not generate maintenance, no data is stored. All user data is deleted at each end of use. No additional software is necessary to secure this solution.
- This solution can be personalized with the colors of the association.
The different levels of management: users, guides and association managers make it possible to segment responsibilities.

Once Mona is installed you will be able to: Customize users' desktop by adding read-only files or folders for users.
- Modify the browser start page and personalize it for your association's users. By default a page presenting the most popular teleservices is made available. This page aims above all to address the accessibility of different administrative teleservices.
- Modify the General Conditions of Use for your association
- Install additional software
- Modify the graphic identity of the solution.
- Set the inactivity timeout before erasing data
Once these operations have been completed, the terminal can be made available to users.

See [Presentation du projet Mona](https://gitlab.com/bordeaux-metropole-public/environnement-numerique-de-travail/mona/-/wikis/Accueil) on the Wiki page

## Installation
See the [Documentation](https://gitlab.com/bordeaux-metropole-public/environnement-numerique-de-travail/mona/-/wikis/Documentation-utilisateurs) on the Wiki page

For download  Mona :
https://gitlab.com/bordeaux-metropole-public/desktop-windows/mona/-/blob/main/Version%201.00./Mona.msi?ref_type=heads

This software is only compatible with Windows 10/11 Professionnal or Enterprise.

## Support
See [Tickets](https://gitlab.com/bordeaux-metropole-public/desktop-windows/mona/-/issues)

## Roadmap & Contributing
Mona will be released in **December 2024**

Currently only version 1.00 is planned

## Authors and acknowledgment
This project is developed by Bordeaux Metropole, DGNSI, DCN, France

## License
Mona is released under GNU General Public License v3.0 - see the LICENSE file for details. 

## Project status
This project is in version 1.00